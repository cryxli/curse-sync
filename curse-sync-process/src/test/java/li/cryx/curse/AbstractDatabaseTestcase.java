package li.cryx.curse;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import li.cryx.curse.AbstractDatabaseTestcase.TestCaseConfig;
import li.cryx.curse.config.DatabaseConfig;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestCaseConfig.class)
public abstract class AbstractDatabaseTestcase {

    @Configuration
    @Import(DatabaseConfig.class)
    public static class TestCaseConfig {

        @Bean
        public AppConfig getAppConfig() {
            final String userHome = System.getProperty("user.home");
            try {
                System.setProperty("user.home", "./target");
                return new AppConfig();
            } finally {
                System.setProperty("user.home", userHome);
            }
        }

    }

    @Autowired
    protected DataSource dataSource;

    // @Before
    // public void initDatabase() {
    // final ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
    // rdp.addScript(new ClassPathResource("/sql/ddl.sql"));
    // rdp.addScript(new ClassPathResource("/sql/init.sql"));
    // rdp.execute(dataSource);
    // }

    @Before
    public void clearTables() {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.batchUpdate( //
                "DELETE FROM " + VersionMapper.TABLE, //
                "DELETE FROM " + FileMapper.TABLE, //
                "DELETE FROM " + ProjectMapper.TABLE //
        );
    }

}
