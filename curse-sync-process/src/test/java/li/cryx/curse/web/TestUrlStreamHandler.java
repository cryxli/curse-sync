package li.cryx.curse.web;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.Resource;

/**
 * This stream handler is used in unittests to redirect URLs to classpath resources.
 *
 * @author cryxli
 */
public class TestUrlStreamHandler extends URLStreamHandler {

    private final Map<String, Resource> urlMap = new HashMap<>();

    private class TestUrlConnection extends URLConnection {

        public TestUrlConnection(final URL url) {
            super(url);
        }

        @Override
        public void connect() throws IOException {
            // do nothing
        }

        @Override
        public InputStream getInputStream() throws IOException {
            final Resource res = urlMap.get(url.toString());
            if (res == null) {
                throw new IOException("Unknown URL: " + url);
            } else {
                return res.getInputStream();
            }
        }

    }

    @Override
    protected URLConnection openConnection(final URL url) throws IOException {
        return new TestUrlConnection(url);
    }

    public TestUrlStreamHandler addResource(final String url, final Resource res) {
        urlMap.put(url, res);
        return this;
    }

}
