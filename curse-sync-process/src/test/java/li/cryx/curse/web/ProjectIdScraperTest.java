package li.cryx.curse.web;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectIdScraperTest {

    private ProjectIdScraper scraper;

    @Before
    public void initMocks() {
        scraper = new ProjectIdScraper() {
            @Override
            protected String download(final URL url) throws IOException {
                try (InputStream stream = getClass().getResourceAsStream("/web/project_281849.htm")) {
                    return IOUtils.toString(stream, StandardCharsets.UTF_8);
                }
            }
        };
    }

    @Test
    public void test() throws Exception {
        // test
        final Long projectId = scraper
                .process("https://www.curseforge.com/minecraft/mc-mods/pneumaticcraft-repressurized");

        // verify
        Assert.assertEquals(281849, projectId.longValue());
    }

}
