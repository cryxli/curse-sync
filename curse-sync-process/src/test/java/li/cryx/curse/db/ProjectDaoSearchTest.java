package li.cryx.curse.db;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import li.cryx.curse.AbstractDatabaseTestcase;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.model.filter.ProjectSearchFilter;

public class ProjectDaoSearchTest extends AbstractDatabaseTestcase {

    @Autowired
    private ProjectDao dao;

    private void insertTestset() {
        dao.store(newProject(1, "Project 1"));
        dao.store(newProject(2, "Project 2"));
        dao.store(newProject(3, "Project 3"));
        dao.store(newProject(4, "Project 4"));
        dao.store(newProject(5, "Project 5"));
        dao.store(newProject(6, "Project 6"));
        dao.store(newProject(7, "Project 7"));
    }

    private Project newProject(final long id, final String title) {
        final Project p = new Project();
        p.setId(id);
        p.setTitle(title);
        p.setUrl("some URL");
        p.setFiles("some URL");
        return p;
    }

    @Test
    public void search10Test() {
        // prepare - insert projects
        insertTestset();

        // test - first page of 10 projects
        ProjectSearchFilter filter = new ProjectSearchFilter();
        filter.setPageSize(10);

        List<Project> list = dao.search(filter);

        // verify
        Assert.assertEquals(7, filter.getRowCount());
        Assert.assertEquals(7, list.size());
        Assert.assertEquals(1, list.get(0).getId());
        Assert.assertEquals(2, list.get(1).getId());
        Assert.assertEquals(3, list.get(2).getId());
        Assert.assertEquals(4, list.get(3).getId());
        Assert.assertEquals(5, list.get(4).getId());
        Assert.assertEquals(6, list.get(5).getId());
        Assert.assertEquals(7, list.get(6).getId());

        // test - second page
        filter.setOffset(filter.getOffset() + filter.getPageSize());
        list = dao.search(filter);

        // verify
        Assert.assertEquals(7, filter.getRowCount());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void search5Test() {
        // prepare - insert projects
        insertTestset();

        // test - first page of 5 projects
        ProjectSearchFilter filter = new ProjectSearchFilter();
        filter.setPageSize(5);
        filter.setOrderAsc(false);

        List<Project> list = dao.search(filter);

        // verify
        Assert.assertEquals(7, filter.getRowCount());
        Assert.assertEquals(5, list.size());
        Assert.assertEquals(7, list.get(0).getId());
        Assert.assertEquals(6, list.get(1).getId());
        Assert.assertEquals(5, list.get(2).getId());
        Assert.assertEquals(4, list.get(3).getId());
        Assert.assertEquals(3, list.get(4).getId());

        // test - second page
        filter.setOffset(filter.getOffset() + filter.getPageSize());
        list = dao.search(filter);

        // verify
        Assert.assertEquals(7, filter.getRowCount());
        Assert.assertEquals(2, list.size());
        Assert.assertEquals(2, list.get(0).getId());
        Assert.assertEquals(1, list.get(1).getId());
    }

    @Test
    public void searchAllTest() {
        // prepare - insert projects
        insertTestset();

        // test - first page of 10 projects
        ProjectSearchFilter filter = new ProjectSearchFilter();
        // no paging
        filter.setPageSize(0);
        // is ignored
        filter.setOffset(10);

        List<Project> list = dao.search(filter);

        // verify
        Assert.assertEquals(7, filter.getRowCount());
        Assert.assertEquals(7, list.size());
        Assert.assertEquals(1, list.get(0).getId());
        Assert.assertEquals(2, list.get(1).getId());
        Assert.assertEquals(3, list.get(2).getId());
        Assert.assertEquals(4, list.get(3).getId());
        Assert.assertEquals(5, list.get(4).getId());
        Assert.assertEquals(6, list.get(5).getId());
        Assert.assertEquals(7, list.get(6).getId());
    }

}
