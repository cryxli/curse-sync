package li.cryx.curse.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import li.cryx.curse.AbstractDatabaseTestcase;
import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.model.db.Project;

public class ProjectDaoTest extends AbstractDatabaseTestcase {

    @Autowired
    private ProjectDao dao;

    @Test
    public void findNokTest() {
        // does not exist
        Assert.assertNull(dao.find(250850));
    }

    @Test
    public void findOkTest() {
        // prepare - insert test data
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(ProjectMapper.TABLE).append(" (") //
                .append(ProjectMapper.ATTR_DESC).append(',') //
                .append(ProjectMapper.ATTR_FILES).append(',') //
                .append(ProjectMapper.ATTR_ID).append(',') //
                .append(ProjectMapper.ATTR_KEY).append(',') //
                .append(ProjectMapper.ATTR_LOGO).append(',') //
                .append(ProjectMapper.ATTR_TITLE).append(',') //
                .append(ProjectMapper.ATTR_URL) //
                .append(") VALUES (") //
                .append("'Description',") //
                .append("'URL to first files page',") //
                .append("250850,") //
                .append("'key',") //
                .append("'URL to logo',") //
                .append("'Project Title',") //
                .append("'URL to project page'") //
                .append(")") //
                .toString());

        // test
        final Project p = dao.find(250850);

        // verify
        Assert.assertEquals("Description", p.getDesc());
        Assert.assertEquals("URL to first files page", p.getFiles());
        Assert.assertEquals(250850, p.getId());
        Assert.assertEquals("key", p.getKey());
        Assert.assertEquals("URL to logo", p.getLogo());
        Assert.assertEquals("Project Title", p.getTitle());
        Assert.assertEquals("URL to project page", p.getUrl());
        Assert.assertTrue(p.isUpdated());
    }

    @Test
    public void storeInsertTest() {
        // prepare - object to store
        Project p = new Project();
        p.setDesc("Description");
        p.setFiles("URL to first files page");
        p.setId(250850);
        p.setKey("key");
        p.setLogo("URL to logo");
        p.setTitle("Project Title");
        p.setUrl("URL to project page");
        p.setUpdated(false);

        // test
        dao.store(p);

        // verify
        p = dao.find(250850);
        Assert.assertEquals("Description", p.getDesc());
        Assert.assertEquals("URL to first files page", p.getFiles());
        Assert.assertEquals(250850, p.getId());
        Assert.assertEquals("key", p.getKey());
        Assert.assertEquals("URL to logo", p.getLogo());
        Assert.assertEquals("Project Title", p.getTitle());
        Assert.assertEquals("URL to project page", p.getUrl());
        Assert.assertFalse(p.isUpdated());
    }

    @Test
    public void storeNokTest() {
        try {
            dao.store(new Project());
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Project must provide its curse ID.", e.getMessage());
        }
    }

    @Test
    public void storeUpdateTest() {
        // prepare - insert test data
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(ProjectMapper.TABLE).append(" (") //
                .append(ProjectMapper.ATTR_DESC).append(',') //
                .append(ProjectMapper.ATTR_FILES).append(',') //
                .append(ProjectMapper.ATTR_ID).append(',') //
                .append(ProjectMapper.ATTR_KEY).append(',') //
                .append(ProjectMapper.ATTR_LOGO).append(',') //
                .append(ProjectMapper.ATTR_TITLE).append(',') //
                .append(ProjectMapper.ATTR_URL).append(',') //
                .append(ProjectMapper.ATTR_UPDATE) //
                .append(") VALUES (") //
                .append("'desc',") //
                .append("'files',") //
                .append("250850,") //
                .append("'key',") //
                .append("'logo',") //
                .append("'title',") //
                .append("'url',") //
                .append("1") //
                .append(")") //
                .toString());

        // prepare - object to store
        Project p = new Project();
        p.setDesc("Description");
        p.setFiles("URL to first files page");
        p.setId(250850);
        p.setKey("projectKey");
        p.setLogo("URL to logo");
        p.setTitle("Project Title");
        p.setUrl("URL to project page");
        p.setUpdated(false);

        // test
        dao.store(p);

        // verify
        p = dao.find(250850);
        Assert.assertEquals("Description", p.getDesc());
        Assert.assertEquals("URL to first files page", p.getFiles());
        Assert.assertEquals(250850, p.getId());
        Assert.assertEquals("projectKey", p.getKey());
        Assert.assertEquals("URL to logo", p.getLogo());
        Assert.assertEquals("Project Title", p.getTitle());
        Assert.assertEquals("URL to project page", p.getUrl());
        Assert.assertFalse(p.isUpdated());
    }

}
