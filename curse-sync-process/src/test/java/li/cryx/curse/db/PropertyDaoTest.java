package li.cryx.curse.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import li.cryx.curse.AbstractDatabaseTestcase;

public class PropertyDaoTest extends AbstractDatabaseTestcase {

    @Autowired
    private PropertyDao dao;

    @Test
    public void findTest() {
        Assert.assertNull(dao.find("does.not.exist").toString());

        Assert.assertEquals("https://minecraft.curseforge.com/projects/{0}",
                dao.find("project.page.url.template").toString());
    }

    @Test
    public void findNotNullTest() {
        Assert.assertEquals("default value", dao.find("does.not.exist", "default value").toString());

        Assert.assertEquals("https://minecraft.curseforge.com/projects/{0}",
                dao.find("project.page.url.template").toString());
    }

    @Test
    public void getTest() {
        Assert.assertNull(dao.get("does.not.exist", String.class));

        Assert.assertEquals("https://minecraft.curseforge.com/projects/{0}",
                dao.get("project.page.url.template", String.class));
    }

}
