package li.cryx.curse.db;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import li.cryx.curse.AbstractDatabaseTestcase;
import li.cryx.curse.mng.DataPatch;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.filter.ModFileSearchFilter;

public class FileDaoTest extends AbstractDatabaseTestcase {

    private static final long PRJ = 223852;

    @Autowired
    private FileDao dao;

    @Autowired
    private ProjectDao projectDao;

    private DataPatch dp;

    @Test
    public void findByHashNokTest() {
        // does not exist
        Assert.assertNull(dao.find("MD5 hash"));
    }

    @Test
    public void findByHashOkTest() {
        // prepare - insert test data
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(FileMapper.TABLE).append(" (") //
                .append(FileMapper.ATTR_DETAIL).append(',') //
                .append(FileMapper.ATTR_DOWNLOAD).append(',') //
                .append(FileMapper.ATTR_HASH).append(',') //
                .append(FileMapper.ATTR_ID).append(',') //
                .append(FileMapper.ATTR_NAME).append(',') //
                .append(FileMapper.ATTR_PRJ_ID).append(',') //
                .append(FileMapper.ATTR_RELEASE).append(',') //
                .append(FileMapper.ATTR_UPLOADED) //
                .append(") VALUES (") //
                .append("'URL to detail page',") //
                .append("'URL to download',") //
                .append("'MD5 hash',") //
                .append("2489317,") //
                .append("'filename',") //
                .append(PRJ).append(',') //
                .append("'release type',") //
                .append("1420088400") //
                .append(")") //
                .toString());

        // test
        final File fil = dao.find("MD5 hash");

        // verify
        Assert.assertEquals("URL to detail page", fil.getDetail());
        Assert.assertEquals("URL to download", fil.getDownload());
        Assert.assertEquals("MD5 hash", fil.getHash());
        Assert.assertEquals(2489317, fil.getId());
        Assert.assertEquals("filename", fil.getName());
        Assert.assertEquals(PRJ, fil.getProjectId());
        Assert.assertEquals("release type", fil.getRelease());
        Assert.assertEquals(1420088400, fil.getUploaded());
    }

    @Test
    public void findByIdNokTest() {
        // does not exist
        Assert.assertNull(dao.find(2489317));
    }

    @Test
    public void findByIdOkTest() {
        // prepare - insert test data
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(FileMapper.TABLE).append(" (") //
                .append(FileMapper.ATTR_DETAIL).append(',') //
                .append(FileMapper.ATTR_DOWNLOAD).append(',') //
                .append(FileMapper.ATTR_HASH).append(',') //
                .append(FileMapper.ATTR_ID).append(',') //
                .append(FileMapper.ATTR_NAME).append(',') //
                .append(FileMapper.ATTR_PRJ_ID).append(',') //
                .append(FileMapper.ATTR_RELEASE).append(',') //
                .append(FileMapper.ATTR_UPLOADED) //
                .append(") VALUES (") //
                .append("'URL to detail page',") //
                .append("'URL to download',") //
                .append("'MD5 hash',") //
                .append("2489317,") //
                .append("'filename',") //
                .append(PRJ).append(',') //
                .append("'release type',") //
                .append("1420088400") //
                .append(")") //
                .toString());

        // test
        final File p = dao.find(2489317);

        // verify
        Assert.assertEquals("URL to detail page", p.getDetail());
        Assert.assertEquals("URL to download", p.getDownload());
        Assert.assertEquals("MD5 hash", p.getHash());
        Assert.assertEquals(2489317, p.getId());
        Assert.assertEquals("filename", p.getName());
        Assert.assertEquals(PRJ, p.getProjectId());
        Assert.assertEquals("release type", p.getRelease());
        Assert.assertEquals(1420088400, p.getUploaded());
    }

    @Before
    public void initDataPatch() {
        dp = new DataPatch();
        dp.setFileDao(dao);
        dp.setProjectDao(projectDao);
    }

    @Test
    public void nextUnhashedFileTestDone() {
        // test
        Assert.assertNull(dao.nextUnhashedFile(250850));
    }

    @Test
    public void nextUnhashedFileTestFound() {
        // prepare - create file without hash
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(FileMapper.TABLE).append(" (") //
                .append(FileMapper.ATTR_DETAIL).append(',') //
                .append(FileMapper.ATTR_DOWNLOAD).append(',') //
                .append(FileMapper.ATTR_ID).append(',') //
                .append(FileMapper.ATTR_NAME).append(',') //
                .append(FileMapper.ATTR_PRJ_ID).append(',') //
                .append(FileMapper.ATTR_RELEASE).append(',') //
                .append(FileMapper.ATTR_UPLOADED) //
                .append(") VALUES (") //
                .append("'URL to detail page',") //
                .append("'URL to download',") //
                .append("2489317,") //
                .append("'filename',") //
                .append(PRJ).append(',') //
                .append("'release type',") //
                .append("1510136657") //
                .append(")") //
                .toString());

        // test
        final File fil = dao.nextUnhashedFile(PRJ);

        // verify
        Assert.assertEquals("URL to detail page", fil.getDetail());
        Assert.assertEquals("URL to download", fil.getDownload());
        Assert.assertNull(fil.getHash());
        Assert.assertEquals(2489317, fil.getId());
        Assert.assertEquals("filename", fil.getName());
        Assert.assertEquals(PRJ, fil.getProjectId());
        Assert.assertEquals("release type", fil.getRelease());
        Assert.assertEquals(1510136657, fil.getUploaded());
    }

    @Test
    public void searchAllFiles() {
        // prepare
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_PROJECT.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_FILE.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_VERSION.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_DEPENDENCY.txt"));

        // test
        ModFileSearchFilter filter = new ModFileSearchFilter();
        filter.setProjectId(PRJ);
        final List<File> list = dao.search(filter);

        // verify
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void searchFilesOfVersion() {
        // prepare
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_PROJECT.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_FILE.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_VERSION.txt"));
        dp.process(new ClassPathResource("/datapatches/20190722_CUR_DEPENDENCY.txt"));

        // test
        ModFileSearchFilter filter = new ModFileSearchFilter();
        filter.setProjectId(PRJ);
        filter.setVersion("1.8.9");
        final List<File> list = dao.search(filter);

        // verify
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void storeInsertTest() {
        // prepare - object to store
        File fil = new File();
        fil.setDetail("URL to detail page");
        fil.setDownload("URL to download");
        fil.setHash("MD5 hash");
        fil.setId(2489317);
        fil.setName("filename");
        fil.setProjectId(PRJ);
        fil.setRelease("release type");
        fil.setUploaded(1510136657);

        // test
        dao.store(fil);

        // verify
        fil = dao.find(2489317);
        Assert.assertEquals("URL to detail page", fil.getDetail());
        Assert.assertEquals("URL to download", fil.getDownload());
        Assert.assertEquals("MD5 hash", fil.getHash());
        Assert.assertEquals(2489317, fil.getId());
        Assert.assertEquals("filename", fil.getName());
        Assert.assertEquals(PRJ, fil.getProjectId());
        Assert.assertEquals("release type", fil.getRelease());
        Assert.assertEquals(1510136657, fil.getUploaded());
    }

    @Test
    public void storeNokTest() {
        try {
            dao.store(new File());
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("File must provide its curse ID.", e.getMessage());
        }
    }

    @Test
    public void storeUpdateTest() {
        // prepare - insert test data
        new JdbcTemplate(dataSource).update(new StringBuffer() //
                .append("INSERT INTO ").append(FileMapper.TABLE).append(" (") //
                .append(FileMapper.ATTR_DETAIL).append(',') //
                .append(FileMapper.ATTR_DOWNLOAD).append(',') //
                .append(FileMapper.ATTR_HASH).append(',') //
                .append(FileMapper.ATTR_ID).append(',') //
                .append(FileMapper.ATTR_NAME).append(',') //
                .append(FileMapper.ATTR_PRJ_ID).append(',') //
                .append(FileMapper.ATTR_RELEASE).append(',') //
                .append(FileMapper.ATTR_UPLOADED) //
                .append(") VALUES (") //
                .append("'detail',") //
                .append("'download',") //
                .append("'hash',") //
                .append("2489317,") //
                .append("'name',") //
                .append(PRJ).append(',') //
                .append("'release',") //
                .append("1420088400") //
                .append(")") //
                .toString());

        // prepare - object to store
        File fil = new File();
        fil.setDetail("URL to detail page");
        fil.setDownload("URL to download");
        fil.setHash("MD5 hash");
        fil.setId(2489317);
        fil.setName("filename");
        fil.setProjectId(PRJ);
        fil.setRelease("release type");
        fil.setUploaded(1510136657);

        // test
        dao.store(fil);

        // verify
        fil = dao.find(2489317);
        Assert.assertEquals("URL to detail page", fil.getDetail());
        Assert.assertEquals("URL to download", fil.getDownload());
        Assert.assertEquals("MD5 hash", fil.getHash());
        Assert.assertEquals(2489317, fil.getId());
        Assert.assertEquals("filename", fil.getName());
        Assert.assertEquals(PRJ, fil.getProjectId());
        Assert.assertEquals("release type", fil.getRelease());
        Assert.assertEquals(1510136657, fil.getUploaded());
    }

}
