package li.cryx.curse.json;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.gson.GsonBuilder;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.db.Project;

public class ProjectPageJsonReaderTest {

    private ProjectPageJsonReader reader;

    private ProjectDao projectDao;

    @Before
    public void initReader() {
        reader = new ProjectPageJsonReader() {
            @SuppressWarnings("unchecked")
            @Override
            protected Map<String, Object> download(final URL url) throws IOException {
                try (InputStream stream = getClass().getResourceAsStream("/json/project_228404.json")) {
                    final String s = IOUtils.toString(stream, StandardCharsets.UTF_8);
                    return (Map<String, Object>) new GsonBuilder().create().fromJson(s, Object.class);
                }
            }
        };

        projectDao = Mockito.mock(ProjectDao.class);
        reader.setProjectDao(projectDao);
    }

    @Test
    public void test() throws Exception {
        // prepare
        long projectId = 228404;

        // test
        final Project prj = reader.process(projectId);

        // verify - project
        Assert.assertEquals(projectId, prj.getId());
        Assert.assertEquals("actually-additions", prj.getKey());
        Assert.assertEquals("A bunch of awesome gadgets and things! But it's way more than just that!", prj.getDesc());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/actually-additions/files", prj.getFiles());
        Assert.assertEquals("https://media.forgecdn.net/avatars/thumbnails/43/130/64/64/636017838645578016.png",
                prj.getLogo());
        Assert.assertEquals("Actually Additions", prj.getTitle());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/actually-additions", prj.getUrl());

        // verify - ProjectDao mock
        Mockito.verify(projectDao).find(projectId);
        Mockito.verify(projectDao).store(prj);
    }

}
