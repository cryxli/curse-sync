package li.cryx.curse.json;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import com.google.gson.GsonBuilder;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

public class FilePageJsonReaderTest {

    private FilePageJsonReader reader;

    private ProjectDao projectDao;

    private FileDao fileDao;

    @Before
    public void initReader() {
        reader = new FilePageJsonReader() {
            @SuppressWarnings("unchecked")
            @Override
            protected Map<String, Object> download(final URL url) throws IOException {
                try (InputStream stream = getClass().getResourceAsStream("/json/files_228404_all.json")) {
                    final String s = IOUtils.toString(stream, StandardCharsets.UTF_8);
                    return (Map<String, Object>) new GsonBuilder().create().fromJson(s, Object.class);
                }
            }
        };

        fileDao = Mockito.mock(FileDao.class);
        reader.setFileDao(fileDao);
        projectDao = Mockito.mock(ProjectDao.class);
        reader.setProjectDao(projectDao);

        // ensure dependency loop-up works
        final Project dependency = new Project();
        dependency.setId(230497);
        dependency.setKey("chameleon");
        Mockito.when(projectDao.find("chameleon")).thenReturn(dependency);
    }

    @Test
    public void test() throws Exception {
        // prepare
        long projectId = 228404;

        // test
        reader.process(projectId);

        // verify - files
        final ArgumentCaptor<File> fileCaptor = ArgumentCaptor.forClass(File.class);
        Mockito.verify(fileDao, Mockito.times(201)).store(fileCaptor.capture());

        // verify - latest file
        final String prefix = "https://www.curseforge.com/minecraft/mc-mods/actually-additions/";

        File f = fileCaptor.getAllValues().get(0);
        Assert.assertEquals(2713315, f.getId());
        Assert.assertEquals(projectId, f.getProjectId());
        Assert.assertEquals(1558289656, f.getUploaded());
        Assert.assertEquals(prefix + "files/2713315", f.getDetail());
        Assert.assertEquals(prefix + "download/2713315/file", f.getDownload());
        Assert.assertEquals("9047912d213091364df0229678501b10", f.getHash());
        Assert.assertEquals("ActuallyAdditions-1.12.2-r148.jar", f.getName());
        Assert.assertEquals("Release", f.getRelease());

        Mockito.verify(fileDao).addVersion(2713315, "1.12.2");
        Mockito.verify(fileDao).addVersion(2713315, "Java 8");

        Mockito.verify(fileDao).addDependency(2713315, 230497);

        // verify - oldest file
        f = fileCaptor.getValue();
        Assert.assertEquals(2229705, f.getId());
        Assert.assertEquals(projectId, f.getProjectId());
        Assert.assertEquals(1425751570, f.getUploaded());
        Assert.assertEquals(prefix + "files/2229705", f.getDetail());
        Assert.assertEquals(prefix + "download/2229705/file", f.getDownload());
        Assert.assertEquals("94fd282a1a9e1b119b562985c864d5e2", f.getHash());
        Assert.assertEquals("ActuallyAdditions-1.7.10-0.0.1.1.jar", f.getName());
        Assert.assertEquals("Beta", f.getRelease());

        Mockito.verify(fileDao).addVersion(2229705, "1.7.10");
    }

}
