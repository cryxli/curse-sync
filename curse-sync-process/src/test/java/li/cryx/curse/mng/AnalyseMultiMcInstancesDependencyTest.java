package li.cryx.curse.mng;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import li.cryx.curse.AbstractDatabaseTestcase;
import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.MultiMcInstance;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

/**
 * Test {@link AnalyseMultiMcInstances#analyseDependencies(MultiMcInstance)} method of MultiMC instance analyser.
 *
 * @author cryxli
 */
public class AnalyseMultiMcInstancesDependencyTest extends AbstractDatabaseTestcase {

    private static long PRJ_ID = 250850;

    private static long DEP_ID = 123489;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private FileDao fileDao;

    /** Service under test */
    @Autowired
    private AnalyseMultiMcInstances ana;

    private MultiMcInstance createInstance(final String version, final ModState... mods) {
        final MultiMcInstance instance = new MultiMcInstance();
        instance.setVersion(version);
        for (ModState mod : mods) {
            instance.getMods().add(mod);
        }
        return instance;
    }

    private void insertTestData() {
        // prepare - create project with dependency
        Project p = new Project();
        p.setDesc("Description");
        p.setFiles("URL to first files page");
        p.setId(PRJ_ID);
        p.setLogo("URL to logo");
        p.setTitle("Project Title");
        p.setUrl("URL to project page");
        p.setUpdated(false);
        projectDao.store(p);

        // no dependencies
        File f = new File();
        f.setId(PRJ_ID + 1);
        f.setProjectId(PRJ_ID);
        f.setName("Mod1");
        f.setDownload("Download URL");
        f.setDetail("File page");
        f.setRelease("Release");
        f.setHash("A");
        fileDao.store(f);
        fileDao.addVersion(f.getId(), "1.10.2");

        // depends on DEP_ID
        f = new File();
        f.setId(PRJ_ID + 2);
        f.setProjectId(PRJ_ID);
        f.setName("Mod2");
        f.setDownload("Download URL");
        f.setDetail("File page");
        f.setRelease("Release");
        f.setHash("B");
        fileDao.store(f);
        fileDao.addVersion(f.getId(), "1.12.2");
        fileDao.addDependency(f.getId(), DEP_ID);

        // prepare - create dependency project
        p = new Project();
        p.setDesc("Description");
        p.setFiles("URL to first files page");
        p.setId(DEP_ID);
        p.setLogo("URL to logo");
        p.setTitle("Dependency");
        p.setUrl("URL to project page");
        p.setUpdated(false);
        projectDao.store(p);

        // is a dependency
        f = new File();
        f.setId(DEP_ID + 1);
        f.setProjectId(DEP_ID);
        f.setName("Dependency1");
        f.setDownload("Download URL");
        f.setDetail("File page");
        f.setRelease("Release");
        f.setHash("Z");
        fileDao.store(f);
        fileDao.addVersion(f.getId(), "1.12.2");
    }

    private ModState modFile(final long projectId, final long fileId, final String version) {
        final ModState modState = new ModState();
        modState.setProject(projectDao.find(projectId));
        modState.setFile(fileDao.find(fileId));
        modState.setLatest(fileDao.latest(projectId, version));
        return modState;
    }

    @Test
    public void testMetDependency() {
        // prepare
        final String version = "1.12.2";
        insertTestData();
        MultiMcInstance instance = createInstance(version, //
                modFile(PRJ_ID, PRJ_ID + 2, version), //
                modFile(DEP_ID, DEP_ID + 1, version) //
        );

        // test
        Set<Long> missing = ana.analyseDependencies(instance);

        // verify
        Assert.assertTrue(missing.isEmpty());
    }

    @Test
    public void testNoDependency() {
        // prepare
        final String version = "1.10.2";
        insertTestData();
        MultiMcInstance instance = createInstance(version, //
                modFile(PRJ_ID, PRJ_ID + 1, version) //
        );

        // test
        Set<Long> missing = ana.analyseDependencies(instance);

        // verify
        Assert.assertTrue(missing.isEmpty());
    }

    @Test
    public void testUnmetDependency() {
        // prepare
        final String version = "1.12.2";
        insertTestData();
        MultiMcInstance instance = createInstance(version, //
                modFile(PRJ_ID, PRJ_ID + 2, version) //
        );

        // test
        Set<Long> missing = ana.analyseDependencies(instance);

        // verify
        Assert.assertEquals(1, missing.size());
        Assert.assertEquals(DEP_ID, missing.iterator().next().longValue());
    }

}
