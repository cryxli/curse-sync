package li.cryx.curse.mng;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

/**
 * Verify functionality of {@link DataPatch}.
 *
 * @author cryxli
 */
public class DataPatchTest {

    private ProjectDao projectDao;

    private FileDao fileDao;

    private DataPatch dp;

    @Before
    public void initDataPatch() {
        projectDao = Mockito.mock(ProjectDao.class);
        fileDao = Mockito.mock(FileDao.class);

        dp = new DataPatch();
        dp.setProjectDao(projectDao);
        dp.setFileDao(fileDao);
    }

    @Test
    public void loadDependencyCsv() {
        // test
        final boolean result = dp.process(new ClassPathResource("/datapatches/20190722_CUR_DEPENDENCY.txt"));

        // verify
        Assert.assertTrue(result);
        Mockito.verify(fileDao).addDependency(2285046, 230497);
    }

    @Test
    public void loadFileCsv() {
        // test
        final boolean result = dp.process(new ClassPathResource("/datapatches/20190722_CUR_FILE.txt"));

        // verify
        Assert.assertTrue(result);

        final ArgumentCaptor<File> captor = ArgumentCaptor.forClass(File.class);
        Mockito.verify(fileDao, Mockito.times(3)).store(captor.capture());

        final String prefix = "https://minecraft.curseforge.com/projects/";

        File f = captor.getAllValues().get(0);
        Assert.assertEquals(2323184, f.getId());
        Assert.assertEquals(230497, f.getProjectId());
        Assert.assertEquals("Release", f.getRelease());
        Assert.assertEquals("Chameleon-1.8.9-1.1.5.jar", f.getName());
        Assert.assertEquals("4456b31e2635bb31a78b2cf86d9760c5", f.getHash());
        Assert.assertEquals(1471232759, f.getUploaded());
        Assert.assertEquals(prefix + "chameleon/files/2323184", f.getDetail());
        Assert.assertEquals(prefix + "chameleon/files/2323184/download", f.getDownload());

        f = captor.getAllValues().get(1);
        Assert.assertEquals(2284904, f.getId());
        Assert.assertEquals(223852, f.getProjectId());
        Assert.assertEquals("Release", f.getRelease());
        Assert.assertEquals("StorageDrawers-1.7.10-1.9.4.jar", f.getName());
        Assert.assertEquals("e6290a59f05a95c53296da704734ad7c", f.getHash());
        Assert.assertEquals(1456896988, f.getUploaded());
        Assert.assertEquals(prefix + "storage-drawers/files/2284904", f.getDetail());
        Assert.assertEquals(prefix + "storage-drawers/files/2284904/download", f.getDownload());

        f = captor.getAllValues().get(2);
        Assert.assertEquals(2285046, f.getId());
        Assert.assertEquals(223852, f.getProjectId());
        Assert.assertEquals("Release", f.getRelease());
        Assert.assertEquals("StorageDrawers-1.8.9-2.4.3.jar", f.getName());
        Assert.assertEquals("e7973b391dc458e503a157fbc6893e45", f.getHash());
        Assert.assertEquals(1456984336, f.getUploaded());
        Assert.assertEquals(prefix + "storage-drawers/files/2285046", f.getDetail());
        Assert.assertEquals(prefix + "storage-drawers/files/2285046/download", f.getDownload());
    }

    @Test
    public void loadProjectCsv() {
        // test
        final boolean result = dp.process(new ClassPathResource("/datapatches/20190722_CUR_PROJECT.txt"));

        // verify
        Assert.assertTrue(result);

        final ArgumentCaptor<Project> captor = ArgumentCaptor.forClass(Project.class);
        Mockito.verify(projectDao, Mockito.times(2)).store(captor.capture());
        Project p = captor.getAllValues().get(0);

        Assert.assertEquals(230497, p.getId());
        Assert.assertEquals("A shared code and render library.", p.getDesc());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/chameleon/files", p.getFiles());
        Assert.assertEquals("chameleon", p.getKey());
        Assert.assertEquals("https://media.forgecdn.net/avatars/thumbnails/17/951/64/64/635671587579566644.png",
                p.getLogo());
        Assert.assertEquals("Chameleon", p.getTitle());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/chameleon", p.getUrl());
        Assert.assertTrue(p.isUpdated());

        p = captor.getAllValues().get(1);
        Assert.assertEquals(223852, p.getId());
        Assert.assertEquals("Interactive compartment storage for your workshops.", p.getDesc());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/storage-drawers/files", p.getFiles());
        Assert.assertEquals("storage-drawers", p.getKey());
        Assert.assertEquals("https://media.forgecdn.net/avatars/thumbnails/10/842/64/64/635468327818431130.png",
                p.getLogo());
        Assert.assertEquals("Storage Drawers", p.getTitle());
        Assert.assertEquals("https://www.curseforge.com/minecraft/mc-mods/storage-drawers", p.getUrl());
        Assert.assertTrue(p.isUpdated());
    }

    @Test
    public void loadVersionCsv() {
        // test
        final boolean result = dp.process(new ClassPathResource("/datapatches/20190722_CUR_VERSION.txt"));

        // verify
        Assert.assertTrue(result);
        Mockito.verify(fileDao).addVersion(2284904, "1.7.10");
        Mockito.verify(fileDao).addVersion(2285046, "1.8.9");
        Mockito.verify(fileDao).addVersion(2323184, "1.8.9");
    }

}
