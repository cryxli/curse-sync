package li.cryx.curse;

import java.io.File;
import java.nio.charset.StandardCharsets;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import li.cryx.curse.rc.AppConfig;

public class DataSourceFactory implements FactoryBean<DataSource> {

    private static final Logger LOG = LoggerFactory.getLogger(DataSourceFactory.class);

    private AppConfig config;

    @Override
    public DataSource getObject() throws Exception {
        final File dbFile = new File(config.getConfigFile().getParentFile(), "curse-sync.sqlite3");
        LOG.info("Using database @ " + dbFile.getAbsolutePath());
        final DataSource dataSource = new SingleConnectionDataSource("jdbc:sqlite:" + dbFile.getAbsolutePath(), true);

        // check database
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        boolean isNew = 0 == jdbcTemplate.queryForObject("SELECT COUNT(*) FROM sqlite_master", Integer.class);

        LOG.info("Creating tables in database...");
        ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
        rdp.setSqlScriptEncoding(StandardCharsets.UTF_8.displayName());
        rdp.addScript(new ClassPathResource("/sql/ddl.sql"));
        rdp.execute(dataSource);

        if (isNew) {
            LOG.info("Populating database...");
            rdp = new ResourceDatabasePopulator();
            rdp.setSqlScriptEncoding(StandardCharsets.UTF_8.displayName());
            rdp.addScript(new ClassPathResource("/sql/init.sql"));
            rdp.execute(dataSource);
        }

        return dataSource;
    }

    @Override
    public Class<DataSource> getObjectType() {
        return DataSource.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setAppConfig(final AppConfig config) {
        this.config = config;
    }

}
