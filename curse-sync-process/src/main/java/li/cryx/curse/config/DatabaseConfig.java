package li.cryx.curse.config;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import li.cryx.curse.DataSourceFactory;
import li.cryx.curse.rc.AppConfig;

@ComponentScan(basePackages = { "li.cryx.curse.db" })
@SpringBootConfiguration
@EnableAutoConfiguration
@Import({ ScraperConfig.class, ManagerConfig.class })
public class DatabaseConfig {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseConfig.class);

    @Bean
    public AppConfig getAppConfig(final ApplicationArguments args) {
        final AppConfig conf = new AppConfig();

        // TODO parse command line arguments

        // load existing config -or- use defaults
        try {
            conf.load(conf.getConfigFile());
        } catch (InvalidPropertiesFormatException e) {
            LOG.warn("Corrupted config file.", e);
        } catch (IOException e) {
            LOG.warn("Invalid config file.", e);
        }

        // TODO replace with command line arguments

        // save changes
        try {
            conf.save();
        } catch (IOException e) {
            LOG.error("Cannot save config file.", e);
        }
        return conf;
    }

    @Bean
    public DataSourceFactory getDataSource(final AppConfig config) {
        final DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setAppConfig(config);
        return dataSourceFactory;
    }

}
