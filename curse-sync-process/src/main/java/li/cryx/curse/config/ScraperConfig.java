package li.cryx.curse.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import li.cryx.curse.web.ProjectIdScraper;

@ComponentScan("li.cryx.curse.json")
@Configuration
@Import({ HttpClientConfig.class, RestTemplateConfig.class })
public class ScraperConfig {

    @Bean
    public ProjectIdScraper getProjectIdScraper() {
        return new ProjectIdScraper();
    }

}
