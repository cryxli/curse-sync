package li.cryx.curse.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("li.cryx.curse.mng")
@Configuration
public class ManagerConfig {
}
