package li.cryx.curse.db;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class AbstractDao {

    private JdbcTemplate jdbcTemplate;

    protected JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Get the next free ID value for a table.
     *
     * @param table
     *            Name of the table
     * @param id
     *            Name of the ID attribute of that table
     * @return Next value that is larger than all other ID values.
     */
    protected long nextVal(final String table, final String id) {
        return jdbcTemplate.queryForObject(
                new StringBuffer() //
                        .append("SELECT COALESCE(MAX(").append(id).append("),0)+1") //
                        .append("  FROM ").append(table) //
                        .toString(), //
                Number.class).longValue();

    }

}
