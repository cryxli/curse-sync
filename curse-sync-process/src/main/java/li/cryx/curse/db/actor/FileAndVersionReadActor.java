package li.cryx.curse.db.actor;

import java.util.LinkedList;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import li.cryx.curse.db.AbstractDao;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.McVersionMapper;
import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.FileAndVersion;
import li.cryx.curse.rc.model.filter.ModFileSearchFilter;

/**
 * This class retrieves files (releases) of a mod project AND its versions.
 *
 * @author cryxli
 */
@Repository
public class FileAndVersionReadActor extends AbstractDao {

    protected static final String SELECT_VERSION_FOR_FILE = new StringBuffer() //
            .append("SELECT DISTINCT ") //
            .append(VersionMapper.ATTR_FILE_ID).append(',') //
            .append(VersionMapper.ATTR_VERSION) //
            .append("  FROM ").append(VersionMapper.TABLE) //
            .append("  JOIN ").append(McVersionMapper.TABLE).append(" ON ").append(McVersionMapper.ATTR_VALUE)
            .append(" = ").append(VersionMapper.ATTR_VERSION).append(" AND ").append(McVersionMapper.ATTR_TYPE)
            .append(" = ").append(McVersionMapper.TYPE) //
            .append(" WHERE ").append(VersionMapper.ATTR_FILE_ID).append(" = ?") //
            .append(" ORDER BY ").append(VersionMapper.ATTR_VERSION).append(" DESC") //
            .toString();

    private static final String SELECT_PROJECT_NAME = new StringBuffer() //
            .append("SELECT ").append(ProjectMapper.ATTR_TITLE) //
            .append("  FROM ").append(ProjectMapper.TABLE) //
            .append(" WHERE ").append(ProjectMapper.ATTR_ID).append(" = ?") //
            .toString();

    private List<File> loadFiles(final ModFileSearchFilter filter) {
        final List<Object> values = new LinkedList<>();

        final StringBuffer count = new StringBuffer("SELECT COUNT(*)");
        final StringBuffer sql = new StringBuffer() //
                .append("  FROM ").append(FileMapper.TABLE);

        if (filter.getVersion() != null) {
            sql.append("  JOIN ").append(VersionMapper.TABLE) //
                    .append(" ON ").append(VersionMapper.ATTR_FILE_ID) //
                    .append(" = ").append(FileMapper.ATTR_ID);
        }

        sql.append(" WHERE ").append(FileMapper.ATTR_PRJ_ID).append(" = ?");
        values.add(filter.getProjectId());

        if (filter.getVersion() != null) {
            sql.append("   AND ").append(VersionMapper.ATTR_VERSION).append(" = ?");
            values.add(filter.getVersion());
        }

        count.append(sql);
        sql.insert(0, "SELECT *");
        sql.append(" ORDER BY ").append(filter.getOrderBy()).append(" ").append(filter.isOrderAsc() ? "ASC" : "DESC");
        // return all, or only one page
        if (filter.getPageSize() > 0) {
            sql //
                    .append(" LIMIT ").append(filter.getPageSize()) //
                    .append(" OFFSET ").append(filter.getOffset()) //
            ;
        }

        filter.setRowCount(jdbcTemplate().queryForObject(count.toString(), values.toArray(), Number.class).intValue());
        try {
            return jdbcTemplate().query(sql.toString(), values.toArray(), new FileMapper().asMapper());
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

    public List<FileAndVersion> search(final ModFileSearchFilter filter) {
        String projectName;
        try {
            projectName = jdbcTemplate().queryForObject(SELECT_PROJECT_NAME, new Object[] { filter.getProjectId() },
                    String.class);
        } catch (EmptyResultDataAccessException e) {
            projectName = null;
        }

        final VersionMapper map = new VersionMapper();
        final List<FileAndVersion> result = new LinkedList<>();

        // load all files for the mod
        for (File fil : loadFiles(filter)) {
            final FileAndVersion fv = new FileAndVersion(fil);
            fv.setProjectName(projectName);
            result.add(fv);

            // for each file, load the MC versions it supports
            try {
                fv.setVersions(
                        jdbcTemplate().query(SELECT_VERSION_FOR_FILE, new Object[] { fil.getId() }, map.asMapper()));
            } catch (EmptyResultDataAccessException e) {
                // do nothing
            }
        }

        return result;
    }

}
