package li.cryx.curse.db.actor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import li.cryx.curse.db.AbstractDao;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;

/**
 * Database actor the will remove a project, all its files and corresponding versions from the database.
 *
 * @author cryxli
 */
@Repository
public class DeleteProjectActor extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteProjectActor.class);

    private static final String DELETE_VERSION = new StringBuffer() //
            .append("DELETE FROM ").append(VersionMapper.TABLE) //
            .append(" WHERE ").append(VersionMapper.ATTR_FILE_ID).append(" IN (") //
            //
            .append("SELECT ").append(FileMapper.ATTR_ID) //
            .append("  FROM ").append(FileMapper.TABLE) //
            .append(" WHERE ").append(FileMapper.ATTR_PRJ_ID).append(" = ?") //
            //
            .append(")") //
            .toString();

    private static final String DELETE_FILE = new StringBuffer() //
            .append("DELETE FROM ").append(FileMapper.TABLE) //
            .append(" WHERE ").append(FileMapper.ATTR_PRJ_ID).append(" = ?") //
            .toString();

    private static final String DELETE_PROJECT = new StringBuffer() //
            .append("DELETE FROM ").append(ProjectMapper.TABLE) //
            .append(" WHERE ").append(ProjectMapper.ATTR_ID).append(" = ?") //
            .toString();

    @Transactional
    public void delete(final long projectId) {
        LOG.info("Deleting mod PRJ_ID = {}", projectId);
        jdbcTemplate().update(DELETE_VERSION, projectId);
        jdbcTemplate().update(DELETE_FILE, projectId);
        jdbcTemplate().update(DELETE_PROJECT, projectId);
    }

}
