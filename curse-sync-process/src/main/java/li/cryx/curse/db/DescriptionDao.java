package li.cryx.curse.db;

import java.util.LinkedList;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import li.cryx.curse.rc.jdbc.McVersionMapper;
import li.cryx.curse.rc.model.McVersion;

@Repository
public class DescriptionDao extends AbstractDao {

    private static final String SELECT_DESC = new StringBuffer() //
            .append("SELECT *") //
            .append("  FROM ").append(McVersionMapper.TABLE) //
            .append(" WHERE ").append(McVersionMapper.ATTR_TYPE).append(" = ?") //
            .append(" ORDER BY ").append(McVersionMapper.ATTR_KEY).append(" ASC") //
            .toString();

    public List<McVersion> loadMcVersions() {
        try {
            return jdbcTemplate().query(SELECT_DESC, new Object[] { McVersionMapper.TYPE },
                    new McVersionMapper().asMapper());
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

}
