package li.cryx.curse.db;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.model.filter.ProjectSearchFilter;

@Repository
public class ProjectDao extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectDao.class);

    public Project find(final long id) {
        try {
            return jdbcTemplate().query(new StringBuffer() //
                    .append("SELECT *") //
                    .append("  FROM ").append(ProjectMapper.TABLE) //
                    .append(" WHERE ").append(ProjectMapper.ATTR_ID).append(" = ?") //
                    .toString(), //
                    new Object[] { id }, //
                    new ProjectMapper().asExtractor() //
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            LOG.error("Error loading project id=" + id, e);
            throw e;
        }
    }

    public Project find(final String key) {
        try {
            return jdbcTemplate().query(new StringBuffer() //
                    .append("SELECT *") //
                    .append("  FROM ").append(ProjectMapper.TABLE) //
                    .append(" WHERE ").append(ProjectMapper.ATTR_KEY).append(" = ?") //
                    .toString(), //
                    new Object[] { key }, //
                    new ProjectMapper().asExtractor() //
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            LOG.error("Error loading project key=" + key, e);
            throw e;
        }
    }

    public List<Project> search(final ProjectSearchFilter filter) {
        final StringBuffer count = new StringBuffer("SELECT COUNT(*)");
        final StringBuffer sql = new StringBuffer() //
                .append("  FROM ").append(ProjectMapper.TABLE) //
                .append(" WHERE 1=1") //
        ;
        if (filter.getUpdate() != null) {
            sql.append("   AND ").append(ProjectMapper.ATTR_UPDATE).append(" = ").append(filter.getUpdate() ? 1 : 0);
        }
        count.append(sql);
        sql.insert(0, "SELECT *");
        sql.append(" ORDER BY ").append(filter.getOrderBy()).append(' ').append(filter.isOrderAsc() ? "ASC" : "DESC");
        // return all, or only one page
        if (filter.getPageSize() > 0) {
            sql //
                    .append(" LIMIT ").append(filter.getPageSize()) //
                    .append(" OFFSET ").append(filter.getOffset()) //
            ;
        }

        filter.setRowCount(jdbcTemplate().queryForObject(count.toString(), Number.class).intValue());
        try {
            return jdbcTemplate().query(sql.toString(), //
                    new ProjectMapper().asMapper() //
            );
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        } catch (DataAccessException e) {
            LOG.error("Error searching " + filter, e);
            throw e;
        }
    }

    public void store(final Project prj) {
        if (prj.getId() == 0) {
            throw new IllegalArgumentException("Project must provide its curse ID.");
        }
        try {
            storeInternal(prj);
        } catch (DataAccessException e) {
            LOG.error("Error storing project " + prj, e);
            throw e;
        }
    }

    private void storeInternal(final Project prj) throws DataAccessException {
        final int count = jdbcTemplate().queryForObject(new StringBuffer() //
                .append("SELECT COUNT(*)") //
                .append("  FROM ").append(ProjectMapper.TABLE) //
                .append(" WHERE ").append(ProjectMapper.ATTR_ID).append(" = ?") //
                .toString(), Number.class, //
                new Object[] { prj.getId() } //
        ).intValue();
        if (count == 0) {
            // INSERT
            jdbcTemplate().update(new StringBuffer() //
                    .append("INSERT INTO ").append(ProjectMapper.TABLE).append(" (") //
                    .append(ProjectMapper.ATTR_DESC).append(',') //
                    .append(ProjectMapper.ATTR_FILES).append(',') //
                    .append(ProjectMapper.ATTR_ID).append(',') //
                    .append(ProjectMapper.ATTR_KEY).append(',') //
                    .append(ProjectMapper.ATTR_LOGO).append(',') //
                    .append(ProjectMapper.ATTR_TITLE).append(',') //
                    .append(ProjectMapper.ATTR_URL).append(',') //
                    .append(ProjectMapper.ATTR_UPDATE) //
                    .append(") VALUES (?,?,?,?,?,?,?,?)") //
                    .toString(), //
                    prj.getDesc(), //
                    prj.getFiles(), //
                    prj.getId(), //
                    prj.getKey(), //
                    prj.getLogo(), //
                    prj.getTitle(), //
                    prj.getUrl(), //
                    prj.isUpdated() ? 1 : 0 //
            );
        } else {
            // UPDATE
            jdbcTemplate().update(new StringBuffer() //
                    .append("UPDATE ").append(ProjectMapper.TABLE) //
                    .append("   SET ") //
                    .append(ProjectMapper.ATTR_DESC).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_FILES).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_KEY).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_LOGO).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_TITLE).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_URL).append(" = ?").append(',') //
                    .append(ProjectMapper.ATTR_UPDATE).append(" = ?") //
                    .append(" WHERE ").append(ProjectMapper.ATTR_ID).append(" = ?") //
                    .toString(), //
                    prj.getDesc(), //
                    prj.getFiles(), //
                    prj.getKey(), //
                    prj.getLogo(), //
                    prj.getTitle(), //
                    prj.getUrl(), //
                    prj.isUpdated() ? 1 : 0, //
                    prj.getId() //
            );
        }
    }

}
