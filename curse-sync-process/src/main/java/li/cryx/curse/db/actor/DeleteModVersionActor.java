package li.cryx.curse.db.actor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import li.cryx.curse.db.AbstractDao;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;

/**
 * Database actor the will remove a file from the database together with its version entries.
 *
 * @author cryxli
 */
@Repository
public class DeleteModVersionActor extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteModVersionActor.class);

    private static final String DELETE_VERSION = new StringBuffer() //
            .append("DELETE FROM ").append(VersionMapper.TABLE) //
            .append(" WHERE ").append(VersionMapper.ATTR_FILE_ID).append(" = ?") //
            .toString();

    private static final String DELETE_FILE = new StringBuffer() //
            .append("DELETE FROM ").append(FileMapper.TABLE) //
            .append(" WHERE ").append(FileMapper.ATTR_ID).append(" = ?") //
            .toString();

    @Transactional
    public void delete(final long fileId) {
        LOG.info("Deleting mod version FIL_ID = {}", fileId);
        jdbcTemplate().update(DELETE_VERSION, fileId);
        jdbcTemplate().update(DELETE_FILE, fileId);
    }

}
