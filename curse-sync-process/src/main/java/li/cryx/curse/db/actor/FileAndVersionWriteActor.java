package li.cryx.curse.db.actor;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import li.cryx.curse.db.AbstractDao;
import li.cryx.curse.rc.jdbc.VersionMapper;
import li.cryx.curse.rc.model.db.FileAndVersion;
import li.cryx.curse.rc.model.db.FileVersion;

@Repository
public class FileAndVersionWriteActor extends AbstractDao {

    private static final String DELETE_VERSION = new StringBuffer() //
            .append("DELETE FROM ").append(VersionMapper.TABLE) //
            .append(" WHERE ").append(VersionMapper.ATTR_FILE_ID).append(" = ?") //
            .append("   AND ").append(VersionMapper.ATTR_VERSION).append(" = ?") //
            .toString();

    private static final String INSERT_VERSION = new StringBuffer() //
            .append("INSERT INTO ").append(VersionMapper.TABLE).append(" (") //
            .append(VersionMapper.ATTR_FILE_ID).append(',') //
            .append(VersionMapper.ATTR_VERSION) //
            .append(") VALUES (") //
            .append("?,?") //
            .append(")").toString();

    @Transactional
    public void saveChanges(final FileAndVersion fv) {

        // TODO Allow to change anything else?

        updateVersions(fv.getId(), fv.getVersions());
    }

    private void updateVersions(final long fileId, final List<FileVersion> versions) {
        // get current versions
        final VersionMapper map = new VersionMapper();
        final List<FileVersion> current = jdbcTemplate().query(FileAndVersionReadActor.SELECT_VERSION_FOR_FILE,
                new Object[] { fileId }, map.asMapper());

        // determine necessary actions
        final List<FileVersion> toDelete = new LinkedList<>();
        final List<FileVersion> toInsert = new LinkedList<>();
        if (versions.isEmpty()) {
            toDelete.addAll(current);
        } else if (current.isEmpty()) {
            toInsert.addAll(versions);
        } else {
            for (FileVersion v : current) {
                if (!versions.contains(v)) {
                    toDelete.add(v);
                }
            }
            for (FileVersion v : versions) {
                if (!current.contains(v)) {
                    toInsert.add(v);
                }
            }
        }

        // perform deletes
        for (FileVersion v : toDelete) {
            jdbcTemplate().update(DELETE_VERSION, fileId, v.getVersion());
        }
        // perform update
        for (FileVersion v : toInsert) {
            jdbcTemplate().update(INSERT_VERSION, fileId, v.getVersion());
        }
    }

}
