package li.cryx.curse.db;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import li.cryx.curse.rc.jdbc.DependencyMapper;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.filter.ModFileSearchFilter;

@Repository
public class FileDao extends AbstractDao {

    private static String INSERT = new StringBuffer() //
            .append("INSERT INTO ").append(FileMapper.TABLE).append(" (") //
            .append(FileMapper.ATTR_DETAIL).append(',') //
            .append(FileMapper.ATTR_DOWNLOAD).append(',') //
            .append(FileMapper.ATTR_HASH).append(',') //
            .append(FileMapper.ATTR_ID).append(',') //
            .append(FileMapper.ATTR_NAME).append(',') //
            .append(FileMapper.ATTR_PRJ_ID).append(',') //
            .append(FileMapper.ATTR_RELEASE).append(',') //
            .append(FileMapper.ATTR_UPLOADED) //
            .append(") VALUES (?,?,?,?,?,?,?,?)") //
            .toString();

    private static String INSERT_DEPENDENCY = new StringBuffer() //
            .append("INSERT INTO ").append(DependencyMapper.TABLE).append(" (") //
            .append(DependencyMapper.ATTR_FIL_ID).append(',') //
            .append(DependencyMapper.ATTR_PRJ_ID) //
            .append(") VALUES (?,?)") //
            .toString();

    private static String INSERT_VERSION = new StringBuffer() //
            .append("INSERT INTO ").append(VersionMapper.TABLE).append(" (") //
            .append(VersionMapper.ATTR_FILE_ID).append(',') //
            .append(VersionMapper.ATTR_VERSION) //
            .append(") VALUES (?,?)") //
            .toString();

    private static String QUERY_DEPENDENCY_COUNT = new StringBuffer() //
            .append("SELECT COUNT(*)") //
            .append("  FROM ").append(DependencyMapper.TABLE) //
            .append(" WHERE ").append(DependencyMapper.ATTR_FIL_ID).append(" = ?") //
            .append("   AND ").append(DependencyMapper.ATTR_PRJ_ID).append(" = ?") //
            .toString();

    private static String QUERY_DEPENDENCY_PROJECTS = new StringBuffer() //
            .append("SELECT ").append(DependencyMapper.ATTR_PRJ_ID) //
            .append("  FROM ").append(DependencyMapper.TABLE) //
            .append(" WHERE ").append(DependencyMapper.ATTR_FIL_ID).append(" = ?") //
            .toString();

    private static String QUERY_LATEST = new StringBuffer() //
            .append("SELECT ").append(FileMapper.TABLE).append(".*") //
            .append("  FROM ").append(FileMapper.TABLE) //
            .append("  JOIN ").append(VersionMapper.TABLE) //
            .append(" ON ").append(VersionMapper.ATTR_FILE_ID).append(" = ").append(FileMapper.ATTR_ID) //
            .append(" WHERE ").append(FileMapper.ATTR_PRJ_ID).append(" = ?") //
            .append("   AND ").append(VersionMapper.ATTR_VERSION).append(" = ?") //
            .append(" ORDER BY ").append(FileMapper.ATTR_DOWNLOAD).append(" DESC") //
            .append(" LIMIT 1") //
            .toString();

    private static String QUERY_VERSION = new StringBuffer() //
            .append("SELECT COUNT(*)") //
            .append("  FROM ").append(VersionMapper.TABLE) //
            .append(" WHERE ").append(VersionMapper.ATTR_FILE_ID).append(" = ?") //
            .append("   AND ").append(VersionMapper.ATTR_VERSION).append(" = ?") //
            .toString();

    private static String UPDATE = new StringBuffer() //
            .append("UPDATE ").append(FileMapper.TABLE) //
            .append("   SET ") //
            .append(FileMapper.ATTR_DETAIL).append(" = ?").append(',') //
            .append(FileMapper.ATTR_DOWNLOAD).append(" = ?").append(',') //
            .append(FileMapper.ATTR_HASH).append(" = ?").append(',') //
            .append(FileMapper.ATTR_NAME).append(" = ?").append(',') //
            .append(FileMapper.ATTR_PRJ_ID).append(" = ?").append(',') //
            .append(FileMapper.ATTR_RELEASE).append(" = ?").append(',') //
            .append(FileMapper.ATTR_UPLOADED).append(" = ?") //
            .append(" WHERE ").append(FileMapper.ATTR_ID).append(" = ?") //
            .toString();

    public void addDependency(final long fileId, final long dependencyProjectId) {
        final int count = jdbcTemplate().queryForObject(QUERY_DEPENDENCY_COUNT, //
                new Object[] { fileId, dependencyProjectId }, //
                Number.class).intValue();
        if (count == 0) {
            jdbcTemplate().update(INSERT_DEPENDENCY, //
                    new Object[] { fileId, dependencyProjectId } //
            );
        }
    }

    public void addVersion(final long fileId, final String version) {
        final int count = jdbcTemplate().queryForObject(QUERY_VERSION, //
                new Object[] { fileId, version }, //
                Number.class).intValue();
        if (count == 0) {
            jdbcTemplate().update(INSERT_VERSION, //
                    new Object[] { fileId, version } //
            );
        }
    }

    public File find(final long id) {
        return findById(id);
    }

    public File find(final String hash) {
        return findByHash(hash);
    }

    public File findByHash(final String hash) {
        try {
            return jdbcTemplate().query(new StringBuffer() //
                    .append("SELECT *") //
                    .append(" FROM ").append(FileMapper.TABLE) //
                    .append(" WHERE ").append(FileMapper.ATTR_HASH).append(" = ?") //
                    .toString(), //
                    new Object[] { hash }, //
                    new FileMapper().asExtractor() //
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public File findById(final long id) {
        try {
            return jdbcTemplate().query(new StringBuffer() //
                    .append("SELECT *") //
                    .append(" FROM ").append(FileMapper.TABLE) //
                    .append(" WHERE ").append(FileMapper.ATTR_ID).append(" = ?") //
                    .toString(), //
                    new Object[] { id }, //
                    new FileMapper().asExtractor() //
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * Get all project IDs this the given file depends on.
     *
     * @param fileId
     *            ID of a certain mod file.
     * @return Set of dependencies. Can be empty, but cannot be <code>null</code>.
     */
    public Set<Long> getDependencies(final long fileId) {
        return new HashSet<>(jdbcTemplate().queryForList(QUERY_DEPENDENCY_PROJECTS, Long.class, fileId));
    }

    public File latest(final long projectId, final String version) {
        return jdbcTemplate().query(QUERY_LATEST, //
                new Object[] { projectId, version }, //
                new FileMapper().asExtractor());
    }

    public File nextUnhashedFile(final long projectId) {
        try {
            return jdbcTemplate().query(new StringBuffer() //
                    .append("SELECT *") //
                    .append("  FROM ").append(FileMapper.TABLE) //
                    .append(" WHERE ").append(FileMapper.ATTR_PRJ_ID).append(" = ?") //
                    .append("   AND ").append(FileMapper.ATTR_HASH).append(" IS NULL") //
                    .append("   AND ").append(FileMapper.ATTR_DETAIL).append(" IS NOT NULL") //
                    .toString(), //
                    new Object[] { projectId }, //
                    new FileMapper().asExtractor() //
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<File> search(final ModFileSearchFilter filter) {
        List<Object> values = new LinkedList<>();
        final StringBuffer sql = new StringBuffer() //
                .append("SELECT *") //
                .append("  FROM ").append(FileMapper.TABLE) //
        ;
        if (filter.getVersion() != null) {
            sql.append("  JOIN ").append(VersionMapper.TABLE).append(" ON ").append(VersionMapper.ATTR_FILE_ID)
                    .append(" = ").append(FileMapper.ATTR_ID);
        }
        sql.append(" WHERE 1=1");
        if (filter.getProjectId() != null) {
            sql.append("   AND ").append(FileMapper.ATTR_PRJ_ID).append(" = ?");
            values.add(filter.getProjectId());
        }
        if (filter.getVersion() != null) {
            sql.append("   AND ").append(VersionMapper.ATTR_VERSION).append(" = ?");
            values.add(filter.getVersion());
        }

        // TODO

        sql.append(" ORDER BY ").append(filter.getOrderBy()).append(' ').append(filter.isOrderAsc() ? "ASC" : "DESC") //
        ;
        // return all, or only one page
        if (filter.getPageSize() > 0) {
            sql //
                    .append(" LIMIT ").append(filter.getPageSize()) //
                    .append(" OFFSET ").append(filter.getOffset()) //
            ;
        }

        return jdbcTemplate().query(sql.toString(), values.toArray(), new FileMapper().asMapper());
    }

    public void store(final File fil) {
        if (fil.getId() == 0) {
            throw new IllegalArgumentException("File must provide its curse ID.");
        }

        final int count = jdbcTemplate().queryForObject(new StringBuffer() //
                .append("SELECT COUNT(*)") //
                .append("  FROM ").append(FileMapper.TABLE) //
                .append(" WHERE ").append(FileMapper.ATTR_ID).append(" = ?") //
                .toString(), Number.class, //
                new Object[] { fil.getId() } //
        ).intValue();
        if (count == 0) {
            // INSERT
            jdbcTemplate().update(INSERT, //
                    fil.getDetail(), //
                    fil.getDownload(), //
                    fil.getHash(), //
                    fil.getId(), //
                    fil.getName(), //
                    fil.getProjectId(), //
                    fil.getRelease(), //
                    fil.getUploaded() //
            );
        } else {
            // UPDATE
            jdbcTemplate().update(UPDATE, //
                    fil.getDetail(), //
                    fil.getDownload(), //
                    fil.getHash(), //
                    fil.getName(), //
                    fil.getProjectId(), //
                    fil.getRelease(), //
                    fil.getUploaded(), //
                    fil.getId() //
            );
        }
    }

}
