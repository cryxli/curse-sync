package li.cryx.curse.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import li.cryx.curse.rc.model.db.Property;

@Repository
public class PropertyDao extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(PropertyDao.class);

    public Property find(final String key) {
        String value = null;
        try {
            value = jdbcTemplate().queryForObject(new StringBuffer() //
                    .append("SELECT PROP_VALUE") //
                    .append("  FROM PVD_PROPERTY") //
                    .append(" WHERE PROP_KEY = :key") //
                    .append("   AND FK_USER IS NULL") //
                    .toString(), //
                    new Object[] { key }, //
                    String.class);
        } catch (EmptyResultDataAccessException e) {
            value = null;
        } catch (DataAccessException e) {
            LOG.error("Error loading property " + key, e);
            throw e;
        }
        return new Property(key, value);
    }

    public Property find(final String key, final Object value) {
        final Property p = find(key);
        if (p.isNull() && value != null) {
            p.set(value.toString());
        }
        return p;
    }

    public <V> V get(final String key, final Class<V> clazz) {
        final Property p = find(key);
        if (p.isNull()) {
            return null;
        } else {
            return p.get(clazz);
        }
    }

}
