package li.cryx.curse.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractScraper {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractScraper.class);

    private final ThreadLocal<URL> contextUrl = new ThreadLocal<>();

    @Autowired
    private RestTemplate restTemplate;

    protected String download(final URL url) throws IOException {
        LOG.debug("Downloading " + url);
        try {
            return restTemplate.getForObject(url.toURI(), String.class);
        } catch (URISyntaxException e) {
            throw new IOException(e);
        }
    }

    protected String find(final Pattern pattern, final String document) {
        Matcher matcher = pattern.matcher(document);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    protected String find(final String regex, final String document) {
        return find(Pattern.compile(regex), document);
    }

    protected URL getContextUrl() {
        return contextUrl.get();
    }

    protected String protocolHostPortFileString(final URL url) {
        return protocolHostPortFileUrl(url).toString();
    }

    protected URL protocolHostPortFileUrl(final URL url) {
        try {
            return new URL(url.getProtocol(), url.getHost(), url.getPort(), url.getFile());
        } catch (MalformedURLException e) {
            return url;
        }
    }

    protected String resolve(final URL baseUrl, final String relative) {
        if (relative != null && relative.startsWith("http")) {
            return relative;
        } else {
            try {
                return new URL(baseUrl, relative).toString();
            } catch (MalformedURLException e) {
                return relative;
            }
        }
    }

    protected URL resolveUrl(final URL baseUrl, final String relative) {
        try {
            return new URL(baseUrl, relative);
        } catch (MalformedURLException e) {
            throw new RuntimeException("base URL=" + baseUrl + "\nrelative URL=" + relative, e);
        }
    }

    protected void setContextUrl(final URL url) {
        contextUrl.set(url);
    }

}
