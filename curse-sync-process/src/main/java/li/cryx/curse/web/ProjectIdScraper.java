package li.cryx.curse.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class ProjectIdScraper extends AbstractScraper {

    private final Pattern PROJECT_ID = Pattern.compile("<span>Project ID</span>\\s*<span>(\\d+)</span>");

    public Long process(final String url) throws MalformedURLException, IOException {
        return process(new URL(url));
    }

    public Long process(final URL url) throws IOException {
        setContextUrl(url);
        final String s = download(url);

        final Matcher matcher = PROJECT_ID.matcher(s);
        final String match;
        if (matcher.find()) {
            match = matcher.group(1);
        } else {
            return null;
        }

        return Long.parseLong(match);
    }

}
