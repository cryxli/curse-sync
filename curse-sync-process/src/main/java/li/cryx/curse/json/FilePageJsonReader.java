package li.cryx.curse.json;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

/**
 * Based of https://www.reddit.com/r/feedthebeast/comments/9vj5nm/wip_curseforge_minecraft_mod_indexer_api/
 *
 * @author cryxli
 */
@Component
public class FilePageJsonReader extends AbstractJsonReader {

    private static final Logger LOG = LoggerFactory.getLogger(FilePageJsonReader.class);

    public static final String API_URL = "https://ddph1n5l22.execute-api.eu-central-1.amazonaws.com/dev/mod/{0,number,0}/files";

    public static final String DETAIL_URL = "https://www.curseforge.com/minecraft/mc-mods/{0}/files/{1,number,0}";

    private final ThreadLocal<Integer> index = new ThreadLocal<>();

    private FileDao fileDao;

    private ProjectDao projectDao;

    protected Integer getContextIndex() {
        return index.get();
    }

    protected void handleDependencies(final long filId, final Map<String, Object> json) throws IOException {
        final List<String> dependencies = getList(json, "mod_dependencies");
        for (String projectKey : dependencies) {
            Project p = projectDao.find(projectKey);
            if (p == null) {
                // FIXME
                // How to avoid circular dependencies?

                // add unknown project
                ProjectPageJsonReader subReader = new ProjectPageJsonReader();
                subReader.setProjectDao(projectDao);
                p = subReader.processByKey(projectKey);
            }
            if (p != null) {
                fileDao.addDependency(filId, p.getId());
                LOG.info("file {} depends on project {}", filId, p.getId());
            } else {
                LOG.warn("dependency {} does not exist", projectKey);
            }
        }
    }

    protected void handleFile(final Map<String, Object> json) throws IOException {
        final long id = getLong(json, "id");
        final long prjId = getLong(json, "mod_id");

        File f = fileDao.findById(id);
        if (f == null) {
            LOG.info("New file {} for project {}", id, prjId);
            f = new File();
            f.setProjectId(prjId);
            f.setId(id);
        } else {
            LOG.info("File {} found", id);
            return;
        }

        f.setDetail(MessageFormat.format(DETAIL_URL, getStr(json, "mod_key"), f.getId()));
        f.setDownload(getStr(json, "download_url"));
        f.setHash(getStr(json, "file_md5"));
        f.setName(getStr(json, "file_name"));
        f.setRelease(getStr(json, "release_type"));

        String s = getStr(json, "uploaded");
        long uploaded = LocalDateTime.parse(s).toEpochSecond(ZoneOffset.UTC);
        f.setUploaded(uploaded);

        // update file
        fileDao.store(f);
        handleVersions(id, json);
        handleDependencies(id, json);
    }

    protected void handleVersions(final long filId, final Map<String, Object> json) throws IOException {
        final List<String> versions = getList(json, "minecraft_version");
        for (String version : versions) {
            fileDao.addVersion(filId, version);
            LOG.info("file {} works with {}", filId, version);
        }

        final List<String> javas = getList(json, "java_version");
        for (String java : javas) {
            fileDao.addVersion(filId, java);
            LOG.info("file {} works with {}", filId, java);
        }
    }

    void process(final long projectId) throws MalformedURLException, IOException {
        process(MessageFormat.format(API_URL, projectId));
    }

    public void process(final Project p) throws MalformedURLException, IOException {
        process(p.getId());

    }

    void process(final String url) throws MalformedURLException, IOException {
        process(new URL(url));
    }

    void process(final URL url) throws IOException {
        setContextUrl(url);
        Map<String, Object> json = download(url);
        checkStatus(json);

        int index = 0;
        for (Map<String, Object> fileInfo : getListOfObjects(json, "result")) {
            setContextIndex(index++);
            handleFile(fileInfo);
        }
    }

    protected void setContextIndex(final int index) {
        this.index.set(index);
    }

    @Autowired
    public void setFileDao(final FileDao fileDao) {
        this.fileDao = fileDao;
    }

    @Autowired
    public void setProjectDao(final ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

}
