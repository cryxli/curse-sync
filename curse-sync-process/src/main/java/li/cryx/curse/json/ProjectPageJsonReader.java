package li.cryx.curse.json;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.model.db.Project;

/**
 * Based on https://www.reddit.com/r/feedthebeast/comments/9vj5nm/wip_curseforge_minecraft_mod_indexer_api/
 *
 * @author cryxli
 */
@Component
public class ProjectPageJsonReader extends AbstractJsonReader {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectPageJsonReader.class);

    public static final String API_URL = "https://ddph1n5l22.execute-api.eu-central-1.amazonaws.com/dev/mod/{0}";

    private ProjectDao projectDao;

    public Project process(final long projectId) throws MalformedURLException, IOException {
        return process(MessageFormat.format(API_URL, String.valueOf(projectId)));
    }

    Project process(final String url) throws MalformedURLException, IOException {
        return process(new URL(url));
    }

    Project process(final URL url) throws IOException {
        setContextUrl(url);
        Map<String, Object> json = download(url);
        checkStatus(json);

        json = getMap(json, "result");
        final long id = getLong(json, "id");

        // try to load project info from DB first before creating a new entry.
        Project prj = projectDao.find(id);
        if (prj == null) {
            prj = new Project();
            prj.setId(id);
            prj.setUrl(url.toString());
            LOG.info("New project {}", id);
        } else {
            LOG.info("Project {} found", prj.getTitle());
        }

        prj.setKey(getStr(json, "key"));
        prj.setTitle(getStr(json, "name"));
        prj.setUrl(getStr(json, "url"));
        prj.setFiles(prj.getUrl() + "/files");
        prj.setLogo(getStr(json, "avatar"));
        prj.setDesc(getStr(json, "blurb"));

        projectDao.store(prj);
        return prj;
    }

    public Project processByKey(final String key) throws MalformedURLException, IOException {
        return process(MessageFormat.format(API_URL, key));
    }

    @Autowired
    public void setProjectDao(final ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

}
