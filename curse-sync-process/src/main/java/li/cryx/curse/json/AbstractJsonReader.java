package li.cryx.curse.json;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.google.gson.GsonBuilder;

public abstract class AbstractJsonReader {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractJsonReader.class);

    private final ThreadLocal<URL> contextUrl = new ThreadLocal<>();

    @Autowired
    private RestTemplate restTemplate;

    protected void checkStatus(final Map<String, Object> json) throws IOException {
        if (!json.containsKey("status") || !"ok".equals(json.get("status"))) {
            throw new IOException("API call failed: " + getContextUrl());
        }
    }

    @SuppressWarnings("unchecked")
    protected Map<String, Object> download(final URL url) throws IOException {
        LOG.debug("Downloading " + url);
        try {
            final String s = restTemplate.getForObject(url.toURI(), String.class);
            final Object obj = new GsonBuilder().create().fromJson(s, Object.class);
            if (!(obj instanceof Map)) {
                throw new IOException("Unsupported API answer format: " + obj.getClass());
            }

            return (Map<String, Object>) obj;
        } catch (URISyntaxException e) {
            throw new IOException(e);
        }
    }

    protected URL getContextUrl() {
        return contextUrl.get();
    }

    @SuppressWarnings("unchecked")
    protected <E> List<E> getList(final Map<String, Object> json, final String key) throws IOException {
        if (!json.containsKey(key)) {
            return null;
        }

        final Object obj = json.get(key);
        if (obj == null) {
            return null;
        } else if (!(obj instanceof List)) {
            throw new IOException("Key '" + key + "' is not a list: " + getContextUrl());
        }

        return (List<E>) obj;
    }

    @SuppressWarnings("unchecked")
    protected List<Map<String, Object>> getListOfObjects(final Map<String, Object> json, final String key)
            throws IOException {
        if (!json.containsKey(key)) {
            return null;
        }

        final Object obj = json.get(key);
        if (obj == null) {
            return null;
        } else if (!(obj instanceof List)) {
            throw new IOException("Key '" + key + "' is not a list: " + getContextUrl());
        }

        return (List<Map<String, Object>>) obj;
    }

    protected Long getLong(final Map<String, Object> json, final String key) throws IOException {
        if (!json.containsKey(key)) {
            return null;
        }

        final Object obj = json.get(key);
        if (obj == null) {
            return null;
        } else if (!(obj instanceof Number)) {
            throw new IOException("Key '" + key + "' is not numeric: " + getContextUrl());
        }

        return ((Number) obj).longValue();
    }

    @SuppressWarnings("unchecked")
    protected Map<String, Object> getMap(final Map<String, Object> json, final String key) throws IOException {
        if (!json.containsKey(key)) {
            throw new IOException("API answer incomplete. Missing '" + key + "': " + getContextUrl());
        }

        final Object obj = json.get(key);
        if (obj == null) {
            return null;
        } else if (!(obj instanceof Map)) {
            throw new IOException("API answer incomplete. Unsupported type of '" + key + "': " + getContextUrl());
        }

        return (Map<String, Object>) obj;
    }

    protected String getStr(final Map<String, Object> json, final String key) throws IOException {
        if (!json.containsKey(key)) {
            return null;
        }

        final Object obj = json.get(key);
        if (obj == null) {
            return null;
        } else if (!(obj instanceof String)) {
            throw new IOException("Key '" + key + "' is not a string: " + getContextUrl());
        }

        return (String) obj;
    }

    protected void setContextUrl(final URL url) {
        contextUrl.set(url);
    }

}
