package li.cryx.curse;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import li.cryx.curse.config.DatabaseConfig;
import li.cryx.curse.mng.UpdateAllMods;

/**
 * Special starter that will update all project that are already present in the
 * database.
 *
 * <pre>
 * $ java -cp .:curse-sync-rc.jar -Dloader.main=li.cryx.curse.UpdateAllStarter org.springframework.boot.loader.PropertiesLauncher
 * </pre>
 *
 * @author cryxli
 */
@SpringBootApplication
@Import(DatabaseConfig.class)
public class UpdateAllStarter {

    public static void main(final String[] args) {
	final ApplicationContext ctx = new SpringApplicationBuilder(UpdateAllStarter.class).headless(false).run(args);
	ctx.getBean(UpdateAllMods.class).process();
    }

}
