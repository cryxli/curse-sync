package li.cryx.curse.mng;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.jdbc.DependencyMapper;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.jdbc.VersionMapper;
import li.cryx.curse.rc.model.NamedInputStream;
import li.cryx.curse.rc.model.db.Project;

public class DataPatch {

    public static class CsvReader {

        private BufferedReader reader;

        private final List<String> headers = new LinkedList<>();

        public CsvReader(final InputStream stream) {
            this(new InputStreamReader(stream, StandardCharsets.UTF_8));
        }

        public CsvReader(final Reader reader) {
            if (reader instanceof BufferedReader) {
                this.reader = (BufferedReader) reader;
            } else {
                this.reader = new BufferedReader(reader);
            }
        }

        public void process(final Consumer<Map<String, String>> callback) throws IOException {
            headers.clear();

            // read first uncommented line
            String line = reader.readLine();
            while (line.startsWith("#") && reader.ready()) {
                line = reader.readLine();
            }
            if (line.startsWith("#")) {
                return;
            }
            headers.addAll(Arrays.asList(line.split("\t")));

            // read data rows
            while (reader.ready()) {
                line = reader.readLine();
                // skip commented lines
                while (line.startsWith("#") && reader.ready()) {
                    line = reader.readLine();
                }
                if (line.startsWith("#")) {
                    continue;
                }

                final Map<String, String> row = new HashMap<>();
                int columnIndex = 0;
                for (String s : line.split("\t")) {
                    row.put(headers.get(columnIndex), s);
                    columnIndex++;
                    if (columnIndex >= headers.size()) {
                        break;
                    }
                }
                callback.accept(row);
            }
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(DataPatch.class);

    private ProjectDao projectDao;

    private FileDao fileDao;

    public boolean process(final File file) {
        try (FileInputStream fis = new FileInputStream(file)) {
            return process(file.getName(), fis);
        } catch (IOException e) {
            return false;
        }
    }

    public boolean process(final NamedInputStream namedInputStream) {
        String name = StringUtils.stripToEmpty(namedInputStream.getName()).toUpperCase();
        if (name.indexOf(".") > 0) {
            name = name.substring(0, name.indexOf("."));
        }

        if (name.endsWith(ProjectMapper.TABLE)) {
            return processProject(namedInputStream);
        } else if (name.endsWith(FileMapper.TABLE)) {
            return processFile(namedInputStream);
        } else if (name.endsWith(VersionMapper.TABLE)) {
            return processVersion(namedInputStream);
        } else if (name.endsWith(DependencyMapper.TABLE)) {
            return processDependency(namedInputStream);
        } else {
            return false;
        }
    }

    public boolean process(final Resource res) {
        try (InputStream stream = res.getInputStream()) {
            return process(res.getFilename(), stream);
        } catch (IOException e) {
            LOG.error("Error reading CSV.", e);
            return false;
        }
    }

    public boolean process(final String name, final InputStream stream) {
        return process(new NamedInputStream(name, stream));
    }

    private boolean processDependency(final InputStream stream) {
        LOG.info("Processing dependency CSV");
        try {
            new CsvReader(stream).process((row) -> {
                final Long fileId = toLong(row.get(DependencyMapper.ATTR_FIL_ID));
                final Long projectId = toLong(row.get(DependencyMapper.ATTR_PRJ_ID));
                if (fileId != null && projectId != null) {
                    fileDao.addDependency(fileId, projectId);
                    LOG.info("Added dependency {} to file {}", projectId, fileId);
                } else {
                    LOG.warn("Incomplete dependency row: {}", row);
                }
            });
            return true;
        } catch (IOException e) {
            LOG.error("Error reading CSV.", e);
            return false;
        }
    }

    private boolean processFile(final InputStream stream) {
        LOG.info("Processing file CSV");
        try {
            new CsvReader(stream).process((row) -> {
                final Long fileId = toLong(row.get(FileMapper.ATTR_ID));
                if (fileId == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                li.cryx.curse.rc.model.db.File f = fileDao.find(fileId);
                if (f == null) {
                    f = new li.cryx.curse.rc.model.db.File();
                    f.setId(fileId);

                    final Long projectId = toLong(row.get(FileMapper.ATTR_PRJ_ID));
                    if (projectId == null) {
                        LOG.warn("Incomplete CSV row: {}", row);
                        return;
                    }
                    f.setProjectId(projectId);
                }

                final String detail = StringUtils.stripToNull(row.get(FileMapper.ATTR_DETAIL));
                if (detail == null && f.getDetail() == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                f.setDetail(detail);

                final String download = StringUtils.stripToNull(row.get(FileMapper.ATTR_DOWNLOAD));
                if (download == null && f.getDownload() == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                f.setDownload(download);

                final String hash = StringUtils.stripToNull(row.get(FileMapper.ATTR_HASH));
                if (hash == null && f.getHash() == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                f.setHash(hash);

                final String name = StringUtils.stripToNull(row.get(FileMapper.ATTR_NAME));
                if (name != null) {
                    f.setName(name);
                }

                final String release = StringUtils.stripToNull(row.get(FileMapper.ATTR_RELEASE));
                if (release != null) {
                    f.setRelease(release);
                }

                final Long uploaded = toLong(row.get(FileMapper.ATTR_UPLOADED));
                if (uploaded == null && f.getUploaded() == 0) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                f.setUploaded(uploaded);

                fileDao.store(f);
                LOG.info("File added or updated: {}", fileId);
            });
            return true;
        } catch (IOException e) {
            LOG.error("Error reading CSV.", e);
            return false;
        }
    }

    private boolean processProject(final InputStream stream) {
        LOG.info("Processing project CSV");
        try {
            new CsvReader(stream).process((row) -> {
                final Long projectId = toLong(row.get(ProjectMapper.ATTR_ID));
                if (projectId == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                Project p = projectDao.find(projectId);
                if (p == null) {
                    p = new Project();
                    p.setId(projectId);
                }

                final String key = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_KEY));
                if (key == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                p.setKey(key);

                final String title = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_TITLE));
                if (title == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                p.setTitle(title);

                final String files = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_FILES));
                if (files == null && p.getFiles() == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                p.setFiles(files);

                final String url = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_URL));
                if (url == null && p.getFiles() == null) {
                    LOG.warn("Incomplete CSV row: {}", row);
                    return;
                }
                p.setUrl(url);

                final String desc = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_DESC));
                if (desc != null) {
                    p.setDesc(desc);
                }

                final String logo = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_LOGO));
                if (logo != null) {
                    p.setLogo(logo);
                }

                final String update = StringUtils.stripToNull(row.get(ProjectMapper.ATTR_UPDATE));
                p.setUpdated(update == null || "1".equals(update) || "true".equalsIgnoreCase(update));

                projectDao.store(p);
                LOG.info("Added or updated '{}'", p.getTitle());
            });
            return true;
        } catch (IOException e) {
            LOG.error("Error reading CSV.", e);
            return false;
        }
    }

    private boolean processVersion(final InputStream stream) {
        LOG.info("Processing version CSV");
        try {
            new CsvReader(stream).process((row) -> {
                final Long fileId = toLong(row.get(VersionMapper.ATTR_FILE_ID));
                final String version = row.get(VersionMapper.ATTR_VERSION);
                if (fileId != null && version != null) {
                    fileDao.addVersion(fileId, version);
                    LOG.info("Added version {} to file {}", version, fileId);
                } else {
                    LOG.warn("Incomplete version row: {}", row);
                }
            });
            return true;
        } catch (IOException e) {
            LOG.error("Error reading CSV.", e);
            return false;
        }
    }

    @Autowired
    public void setFileDao(final FileDao fileDao) {
        this.fileDao = fileDao;
    }

    @Autowired
    public void setProjectDao(final ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    private Long toLong(final String value) {
        if (value != null) {
            try {
                return Long.parseLong(StringUtils.stripToEmpty(value));
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

}
