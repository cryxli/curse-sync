package li.cryx.curse.mng;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.ModStateDecorator;
import li.cryx.curse.rc.model.MultiMcInstance;

@Scope("prototype")
@Service
public class AnalyseMultiMcInstances {

    private static final class MapTypeToken extends TypeToken<Map<String, Object>> {
    }

    private static class ModStateNameComparator implements Comparator<ModState> {

        @Override
        public int compare(final ModState ms1, final ModState ms2) {
            if (ms1.getTitle() == ms2.getTitle()) {
                return 0;
            } else if (ms1.getTitle() == null) {
                return 1;
            } else if (ms2.getTitle() == null) {
                return -1;
            } else {
                return ms1.getTitle().compareTo(ms2.getTitle());
            }
        }

    }

    private static final Logger LOG = LoggerFactory.getLogger(AnalyseMultiMcInstances.class);

    private static final TypeToken<?> MAP_TYPE = new MapTypeToken();

    @Autowired
    private AnalyseModFile analyseModFile;

    @Autowired
    private FileDao fileDao;

    @SuppressWarnings("unchecked")
    public List<ModState> analiseModFiles(final MultiMcInstance instance) {
        // list mod files
        final File mods = new File(instance.getMinecraftDir(), "mods");
        final File[] modFiles = mods.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                final String s = name.toLowerCase();
                return s.endsWith(".jar") || s.endsWith(".zip");
            }
        });

        // configure analyser
        instance.setMods(Collections.EMPTY_LIST);
        analyseModFile.setVersion(instance.getVersion());

        // analyse mod files
        for (File modFile : modFiles) {
            final ModState state = analyseModFile.process(modFile);
            instance.getMods().add(state);
        }

        Collections.sort(instance.getMods(), new ModStateNameComparator());
        return instance.getMods();
    }

    public Set<Long> analyseDependencies(final MultiMcInstance instance) {
        // Finally, both lists must contain the same project IDs.
        // When removing all projectIds from requiredProjectIds, only
        // the missing dependencies remain.
        final Set<Long> projectIds = new HashSet<>();
        final Set<Long> requiredProjectIds = new HashSet<>();

        final Queue<ModState> q = new LinkedList<>(instance.getMods());
        while (!q.isEmpty()) {
            // dequeue
            final ModStateDecorator mod = new ModStateDecorator(q.remove());
            final long prjId = mod.getProject().getId();
            projectIds.add(prjId);
            requiredProjectIds.add(prjId);

            // look for required dependencies
            final Set<Long> dependencies = fileDao.getDependencies(mod.getCurrentFile().getId());
            requiredProjectIds.addAll(dependencies);
        }

        // have requiredProjectIds contain missing project IDs
        requiredProjectIds.removeAll(projectIds);
        return requiredProjectIds;
    }

    public List<MultiMcInstance> process(final File multiMcDir) {
        if (multiMcDir == null) {
            return new LinkedList<>();
        }

        final File[] instanceDirs = new File(multiMcDir, "instances").listFiles(new FileFilter() {
            @Override
            public boolean accept(final File pathname) {
                return pathname.isDirectory();
            }
        });

        final List<MultiMcInstance> instances = new LinkedList<>();
        for (File dir : instanceDirs) {
            final File conf = new File(dir, "instance.cfg");
            final File newConf = new File(dir, "mmc-pack.json");
            if (!conf.isFile() && !conf.isFile()) {
                continue;
            }

            final MultiMcInstance instance = new MultiMcInstance();
            final File minecraftDir = new File(dir, "minecraft");
            if (minecraftDir.isDirectory()) {
                instance.setMinecraftDir(minecraftDir);
            } else {
                instance.setMinecraftDir(new File(dir, ".minecraft"));
            }

            if (conf.isFile()) {
                final Properties prop = new Properties();
                try (FileInputStream fis = new FileInputStream(conf)) {
                    prop.load(fis);
                    instance.setName(prop.getProperty("name"));
                    instance.setVersion(prop.getProperty("IntendedVersion"));
                    instance.setDir(dir);
                    instances.add(instance);
                } catch (IOException e) {
                    LOG.error("Error reading instance config: " + conf.getAbsolutePath());
                }
            }
            if (newConf.isFile()) {
                Map<String, Object> map = null;
                try {
                    map = new Gson().fromJson(new FileReader(newConf), MAP_TYPE.getType());
                    // System.out.println(map);

                    if (map.containsKey("components") && map.get("components") instanceof List) {
                        @SuppressWarnings("unchecked")
                        Iterator<Map<String, Object>> iter = ((List<Map<String, Object>>) map.get("components"))
                                .iterator();
                        while (iter.hasNext()) {
                            map = iter.next();
                            if (map.containsKey("uid") && "net.minecraft".equals(map.get("uid"))) {
                                // object with Minecraft version found
                                if (map.containsKey("version")) {
                                    instance.setVersion((String) map.get("version"));
                                }

                                // done
                                break;
                            }
                        }
                    }

                } catch (Exception e) {
                    LOG.error("Error reading instance config: " + newConf.getAbsolutePath());
                }
            }
        }

        Collections.sort(instances, new Comparator<MultiMcInstance>() {
            @Override
            public int compare(final MultiMcInstance o1, final MultiMcInstance o2) {
                if (o1 == o2) {
                    return 0;
                } else if (o1 == null) {
                    return 1;
                } else if (o2 == null) {
                    return -1;
                } else if (o1.getName() == null) {
                    return 1;
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            }
        });
        return instances;
    }

    public List<MultiMcInstance> process(final String multiMcDir) {
        if (multiMcDir != null) {
            return process(new File(multiMcDir));
        } else {
            return new LinkedList<>();
        }
    }

}
