package li.cryx.curse.mng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import li.cryx.curse.db.FileDao;
import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.rc.common.MD5;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.db.File;

@Scope("prototype")
@Service
public class AnalyseModFile {

    private FileDao fileDao;

    private ProjectDao projectDao;

    private String version;

    public ModState process(final java.io.File file) {
        if (file == null) {
            return null;
        }

        final ModState ms = new ModState();
        ms.setJar(file);
        ms.setHash(MD5.hash(file));
        if (ms.getHash() == null) {
            return ms;
        }

        final File fil = fileDao.findByHash(ms.getHash());
        if (fil == null) {
            return ms;
        }
        ms.setFile(fil);
        ms.setProject(projectDao.find(fil.getProjectId()));

        if (version != null) {
            File latest = fileDao.latest(fil.getProjectId(), version);
            ms.setLatest(latest);
        }

        return ms;
    }

    public ModState process(final String file) {
        return process(new java.io.File(file));
    }

    @Autowired
    public void setFileDao(final FileDao fileDao) {
        this.fileDao = fileDao;
    }

    @Autowired
    public void setProjectDao(final ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

}
