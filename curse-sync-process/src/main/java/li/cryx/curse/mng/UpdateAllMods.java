package li.cryx.curse.mng;

import java.io.IOException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import li.cryx.curse.rc.jdbc.ProjectMapper;

@Scope("prototype")
@Service
public class UpdateAllMods {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateAllMods.class);

    private JdbcTemplate jdbcTemplate;

    private ScanJsonMod scan;

    public void process() {
        final List<Long> list = jdbcTemplate.queryForList(new StringBuffer() //
                .append("SELECT ").append(ProjectMapper.ATTR_ID) //
                .append("  FROM ").append(ProjectMapper.TABLE) //
                .append(" WHERE ").append(ProjectMapper.ATTR_UPDATE).append(" = 1") //
                .append(" ORDER BY ").append(ProjectMapper.ATTR_TITLE) //
                .toString(), Long.class);
        LOG.info("{} mod projects to scan", list.size());

        int counter = 1;
        for (Long projectId : list) {
            try {
                LOG.info("Processing {}/{}", counter++, list.size());
                LOG.debug("Scanning " + projectId);
                scan.process(projectId);
            } catch (IOException e) {
                LOG.error("Error while updating project {}", projectId);
            }
        }
    }

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Autowired
    public void setScanJsonMod(final ScanJsonMod scan) {
        this.scan = scan;
    }

}
