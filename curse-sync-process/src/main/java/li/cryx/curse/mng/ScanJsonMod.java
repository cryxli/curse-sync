package li.cryx.curse.mng;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import li.cryx.curse.json.FilePageJsonReader;
import li.cryx.curse.json.ProjectPageJsonReader;
import li.cryx.curse.rc.model.db.Project;

@Scope("prototype")
@Service
public class ScanJsonMod {

    private static final Logger LOG = LoggerFactory.getLogger(ScanJsonMod.class);

    private ProjectPageJsonReader projectReader;

    private FilePageJsonReader fileReader;

    private boolean running;

    public synchronized void interrupt() {
        running = false;
        LOG.info("Interrupted");
    }

    public synchronized void process(final long projectId) throws IOException {
        running = true;

        final Project p;
        try {
            p = projectReader.process(projectId);
        } catch (FileNotFoundException e) {
            LOG.warn("Project {} no longer exists", projectId);
            return;
        }
        if (p == null || p.getId() == 0 || p.getFiles() == null || !running) {
            return;
        }

        try {
            fileReader.process(p);
        } catch (FileNotFoundException e) {
            LOG.warn("No file data found for project {}", projectId);
            return;
        }

        running = false;
    }

    @Autowired
    public void setFilePageJsonReader(final FilePageJsonReader fileReader) {
        this.fileReader = fileReader;
    }

    @Autowired
    public void setProjectPageJsonReader(final ProjectPageJsonReader projectReader) {
        this.projectReader = projectReader;
    }

}
