package li.cryx.curse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import li.cryx.curse.config.DatabaseConfig;
import li.cryx.curse.json.FilePageJsonReader;
import li.cryx.curse.json.ProjectPageJsonReader;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.web.ProjectIdScraper;

/**
 * Special starter that will only add (and scan) a new project to the database.
 *
 * <pre>
 * $ java -cp .:curse-sync-rc.jar -Dloader.main=li.cryx.curse.AddModStarter org.springframework.boot.loader.PropertiesLauncher &lt;URL to mod page&gt;
 * </pre>
 *
 * @author cryxli
 */
@SpringBootApplication
@Import(DatabaseConfig.class)
public class AddModStarter {

    private static final Logger LOG = LoggerFactory.getLogger(AddModStarter.class);

    public static void main(final String[] args) {
        if (args.length < 1) {
            LOG.warn("No project URL provided. Shutting down.");
            return;
        }
        final ApplicationContext ctx = new SpringApplicationBuilder(UpdateAllStarter.class).headless(false).run(args);

        final String urlStr = args[args.length - 1];
        final URL url;
        try {
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            LOG.error("{} is not a valid URL", urlStr);
            return;
        }

        try {
            final Long projectId = ctx.getBean(ProjectIdScraper.class).process(url);
            if (projectId == null) {
                LOG.error("Project not found at {}", urlStr);
                return;
            }

            final Project p = ctx.getBean(ProjectPageJsonReader.class).process(projectId);
            ctx.getBean(FilePageJsonReader.class).process(p);
        } catch (IOException e) {
            LOG.error("Error while reading project from " + urlStr);
        }
    }

}
