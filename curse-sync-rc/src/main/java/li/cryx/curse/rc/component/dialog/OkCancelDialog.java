package li.cryx.curse.rc.component.dialog;

import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;

public class OkCancelDialog extends MessageDialog {

    public OkCancelDialog(final String titleKey, final String msgKey) {
        super(DIALOG_TYPE_QUESTION, titleKey, msgKey);
        final Translation t = Translation.getInstance();
        removeAllButtons();
        setButton(ACTION_CODE_OK, Fa.CHECK.decorateButton(createButton(ACTION_CODE_OK), t.get("OK")));
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
    }

}
