package li.cryx.curse.rc;

import java.awt.Color;

import li.cryx.curse.rc.model.ModStateEnum;

public interface TConstants {

    /** Translated enums */
    public interface ENUM {

        /** Enum {@link ModStateEnum} */
        public interface MOD_STATE {

            String UP_TO_DATE = "enum.mod-state.up-to-date";

            String OUTDATED = "enum.mod-state.outdated";

            String UNKNOWN = "enum.mod-state.unknown";

        }

    }

    /** The main window */
    public interface MAIN_WINDOW {
        /** Application name. */
        String TITLE = "frame.main.title";

        /** Tooltip: Button bar, Settings button */
        String BUT_SETTINGS = "frame.main.but.settings.tooltip";

        /** Tooltip: Button bar, Project list button */
        String BUT_PROJECT_LIST = "frame.main.but.project-list.tooltip";

        /** Tooltip: Button bar, MultiMC instance button */
        String BUT_MULTIMC = "frame.main.but.multimc.tooltip";

    }

    /** View to alter file versions */
    public interface VIEW_FILE_VERSION {

        String LABEL_FILE_NAME = "view.fileversion.label.filename";

        String LABEL_FILE_VERSIONS = "view.fileversion.label.versions";

        String BUT_SAVE = "view.fileversion.but.save";

    }

    /** MultiMC instance */
    public interface VIEW_MULTIMC {

        /** Table columns */
        public interface COLS {

            String PROJECT_TITLE = "view.multimc.table.title";

            String STATE = "view.multimc.table.state";

            String FILENAME = "view.multimc.table.filename";

            String RELEASE = "view.multimc.table.release";

        }

        /** Analyse depencendies dialog related strings */
        public interface DEPENDENCIES {

            String OK_RESULT = "view.multimc.dialog.dependencies.ok-result";

            // TODO

        }

        /** Regarding the select MultiMC instance dialog */
        public interface SELECT_INSTANCE {

            String TITLE = "view.multimc.dialog.select-instance.title";

        }

        /** Regarding the "update all mods" dialog. */
        public interface UPDATE_ALL {

            /** Dialog caption/title */
            String CAPTION = "view.multimc.dialog.update-all.caption";

            /** Main info message. */
            String INFO = "view.multimc.dialog.update-all.info";

        }

        /** Regarding the "update mod" dialog. */
        public interface UPDATE_MOD {

            /** Dialog caption/title */
            String CAPTION = "view.multimc.dialog.update-mod.caption";

            /** Main info message. */
            String INFO = "view.multimc.dialog.update-mod.info";

            /** 1st step: Check download dir for latest mod JAR */
            String STEP_CHECK_EXISTING = "view.multimc.dialog.update-mod.step1";

            /** 2nd step: Download JAR */
            String STEP_DOWNLOAD_JAR = "view.multimc.dialog.update-mod.step2";

            /** 3rd step: Backup old JAR */
            String STEP_BACKUP_MOD = "view.multimc.dialog.update-mod.step3";

            /** 4th step: Copy latest mod to instance */
            String STEP_COPY_MOD = "view.multimc.dialog.update-mod.step4";

        }

        /** View title */
        String CAPTION = "view.multimc.caption";

        /** Warning message to configure MultiMC directory first */
        String WARNING = "view.multimc.warning";

        /** Tooltip: Button bar, select MultiMC instance button */
        String BUT_INSTANCE = "view.multimc.button.select-instance.tooltip";

        /** Tooltip: Button bar, select MultiMC instance button, but there are none */
        String BUT_INSTANCE_NODATA = "view.multimc.button.select-instance.tooltip.nodata";

        /** Tooltip: Button bar, update all mods button */
        String BUT_UPDATE_ALL = "view.multimc.button.update-all.tooltip";

        /** Tooltip: Button bar, open MC instance directory */
        String BUT_INSTANCE_DIR = "view.multimc.button.instance-dir.tooltip";

        /** Popup menu: Open project page in browser. */
        String BUT_OPEN_PROJECT_PAGE = "view.multimc.button.project-page";

        /** Popup menu: Display info about new version. */
        String INFO_NEW_VERSION = "view.multimc.info.new-version";

        /** Popup menu: Open download page in browser. */
        String BUT_OPEN_DOWNLOAD_PAGE = "view.multimc.button.download-page";

        /** Popup menu: Open file page in browser. */
        String BUT_OPEN_FILE_PAGE = "view.multimc.button.file-page";

        /** Popup menu: Download and update mod. */
        String BUT_UPDATE_MOD = "view.multimc.button.update-mod";

        /** Error message: Could not open download page. */
        String ERROR_DOWNLOAD_URL = "view.multimc.download.url.error";

        /** Error message: Could not open file detail page. */
        String ERROR_FILE_URL = "view.multimc.file.url.error";

        /** Tooltip: Button bar, analyse dependencies. */
        String BUT_DEPENDENCIES = "view.multimc.button.dependencies.tooltip";

    }

    /** The project detail view */
    public interface VIEW_PROJECT_DETAIL {

        /** Table column */
        public interface COLS {

            String FILENAME = "view.project-detail.table.filename";

            String VERSIONS = "view.project-detail.table.versions";

            String RELEASE = "view.project-detail.table.release";

            String UPLOADED = "view.project-detail.table.uploaded";

        }

        /** Error messages */
        public interface ERROR {

            /** Error when opening project page in browser */
            String OPEN_PAGE = "view.project-detail.error.open-page";

            /** Error when opening version page in browser. */
            String OPEN_VERSION_PAGE = "view.project-detail.error.open-version-page";

        }

        /** Entries of the popup menu when selecting an entry in the table */
        public interface POPUP {

            /** Open URL of file version page */
            String OPEN_VERSION_PAGE = "view.project-detail.popup.open-version-page";

            /** Delete a file from the database */
            String DELETE_VERSION = "view.project-detail.popup.delete-version";

            /** Change version for a file */
            String EDIT_VERSION = "view.project-detail.popup.edit-version";

        }

        String DELETE_VERSION_TITLE = "view.project-detail.dialog.delete-title";

        String DELETE_VERSION_QUESTION = "view.project-detail.dialog.delete-msg";

    }

    /** THe list of known mod projects */
    public interface VIEW_PROJECT_LIST {

        /** Dialog: Delete project from database */
        interface DIALOG_DELETE {
            /** Dialog caption: Delete project from database */
            String CAPTION = "view.project-list.dialog.delete.project.caption";

            /** Dialog message: Really delete project from database? */
            String MESSAGE = "view.project-list.dialog.delete.project.msg";
        }

        /** View title */
        String CAPTION = "view.project-list.caption";

        /** Column label for project title */
        String COL_TITLE = "view.project-list.col.title";

        /** Column label for update project */
        String COL_UPDATE = "view.project-list.col.update";

        /** Tooltip: Button bar, Add mod button */
        String BUT_ADD = "view.project-list.tooltip.add";

        /** Tooltip: Button bar, Update mods button. */
        String BUT_UPDATE = "view.project-list.tooltip.update";

        /** Popup menu: Visit project page. */
        String BUT_PROJECT_URL = "view.project-list.project.url";

        /** Popup menu: Scan for updates for current mod. */
        String BUT_SCAN_UPDATE = "view.project-list.scan.update";

        /** Popup menu: Delete a project. */
        String BUT_DELETE_PROJECT = "view.project-list.delete.project";

        /** Error message: Cannot open project's main page. */
        String ERROR_PROJECT_URL = "view.project-list.project.url.error";

        /** Info message: Updating all projects. */
        String UPDATE_DIALOG_TEXT = "view.project-list.dialog.project.update";

        /** Dialog title: Adding a project. */
        String ADD_DIALOG_TITLE = "view.project-list.dialog.add.project.title";

        /** Dialog message: Copy-paste URL of new project's main page. */
        String ADD_DIALOG_HINT = "view.project-list.dialog.add.project.hint";

        /** Error message: Error while scanning new project. */
        String ADD_DIALOG_MSG = "view.project-list.dialog.add.project.msg";

    }

    /** The settings view */
    public interface VIEW_SETTINGS {

        /** Regarding the download directory */
        public interface MOD_DIR {
            /** Property label */
            String LABEL = "view.settings.mod-dir.label";

            /** Button text */
            String BUTTON = "view.settings.mod-dir.button";

            /** Dialog title: Directory browser */
            String BROWSE = "view.settings.mod-dir.browse";
        }

        /** Regarding the MultiMC directory */
        public interface MULTIMC_DIR {
            /** Property label */
            String LABEL = "view.settings.multimc-dir.label";

            /** Button text */
            String BUTTON = "view.settings.multimc-dir.button";

            /** Dialog title: Directory browser */
            String BROWSE = "view.settings.multimc-dir.browse";

            String VALIDATION_ERROR = "view.settings.multimc-dir.validation-error";
        }

        /** View title */
        String CAPTION = "view.settings.caption";

        /** Checkbox to enable proxy settings */
        String PROXY_ENABLE = "view.settings.proxy.enable";

        /** Label for proxy host */
        String PROXY_HOST = "view.settings.proxy.host";

        /** Label for proxy port */
        String PROXY_PORT = "view.settings.proxy.port";

        /** Checkbox to enable proxy authentification */
        String PROXY_AUTH_ENABLE = "view.settings.proxy.enable.auth";

        /** Label for proxy user */
        String PROXY_USER = "view.settings.proxy.user";

        /** Label for proxy password */
        String PROXY_PASS = "view.settings.proxy.pass";

    }

    /** The Watches Overview */
    public interface VIEW_WATCHES_OVERVIEW {

        /** Table columns */
        public interface COLS {

            String PROJECT_TITLE = "view.watches-overview.cols.project-title";

            String VERSION = "view.watches-overview.cols.version";

        }

        /** View title */
        String CAPTION = "view.watches-overview.caption";

    }

    /** View title background color. */
    Color PANEL_TITLE_BG = new Color(115, 164, 209);

    /** General string "Error" */
    String ERROR = "Error";

}
