package li.cryx.curse.rc.views.ide;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.TableAspect;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.PagingDelegate;
import li.cryx.curse.rc.component.PagingDelegate.FilterListener;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.model.db.FileAndVersion;
import li.cryx.curse.rc.model.filter.ModFileSearchFilter;
import li.cryx.curse.rc.views.model.ProjectFileTableModel;
import li.cryx.curse.rc.views.model.ProjectFileTableModel.Col;
import li.cryx.curse.rc.views.renderer.CustomeDateRenderer;
import li.cryx.curse.rc.views.renderer.VersionListTableCellRenderer;

public class ProjectDetailIde extends ViewComponent {

    private final Translation t = Translation.getInstance();

    private JButton butProjectLink;

    private JTextArea txDesc;

    private final ProjectFileTableModel model = new ProjectFileTableModel();

    private JTable table;

    private final PagingDelegate<ModFileSearchFilter> paging;

    private JPopupMenu popup;

    private MenuItem<String> butOpenVersionPage;

    private MenuItem<FileAndVersion> butDeleteVersion;

    private MenuItem<FileAndVersion> butEditVersion;

    protected ProjectDetailIde() {
        paging = new PagingDelegate<>( //
                new ModFileSearchFilter(), //
                new FilterListener<ModFileSearchFilter>() {
                    @Override
                    public void applyFilter(final ModFileSearchFilter filter) {
                        preShow();
                    }
                } //
        );
    }

    @Override
    protected void createView() {
        final JPanel main = new JPanel(new BorderLayout());
        setMainPanel(main);
        main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        main.add(getTxDesc(), BorderLayout.NORTH);

        main.add(new JScrollPane(getTable()), BorderLayout.CENTER);

        main.add(paging.createPanel(), BorderLayout.SOUTH);
    }

    protected MenuItem<FileAndVersion> getButDeleteVersion() {
        if (butDeleteVersion == null) {
            butDeleteVersion = new MenuItem<>(Fa.TRASH, t.get(TConstants.VIEW_PROJECT_DETAIL.POPUP.DELETE_VERSION));
        }
        return butDeleteVersion;
    }

    protected MenuItem<FileAndVersion> getButEditVersion() {
        if (butEditVersion == null) {
            butEditVersion = new MenuItem<>(Fa.EDIT, t.get(TConstants.VIEW_PROJECT_DETAIL.POPUP.EDIT_VERSION));
        }
        return butEditVersion;
    }

    protected MenuItem<String> getButOpenVersionPage() {
        if (butOpenVersionPage == null) {
            butOpenVersionPage = new MenuItem<>(Fa.LINK, t.get(TConstants.VIEW_PROJECT_DETAIL.POPUP.OPEN_VERSION_PAGE));
        }
        return butOpenVersionPage;
    }

    protected JButton getButProjectLink() {
        if (butProjectLink == null) {
            butProjectLink = Fa.LINK.makeToolbarButton();
        }
        return butProjectLink;
    }

    protected ModFileSearchFilter getFilter() {
        return paging.getFilter();
    }

    protected ProjectFileTableModel getModel() {
        return model;
    }

    protected JPopupMenu getPopup() {
        if (popup == null) {
            popup = new JPopupMenu();
            popup.add(getButOpenVersionPage());
            popup.addSeparator();
            popup.add(getButEditVersion());
            popup.add(getButDeleteVersion());
        }
        return popup;
    }

    protected JTable getTable() {
        if (table == null) {
            table = new JTable(model);
            TableAspect.setSingleSelection(table);
            TableAspect.setWidth(ProjectFileTableModel.getColumn(table, Col.RELEASE), 100);
            TableAspect.setWidth(ProjectFileTableModel.getColumn(table, Col.UPLOADED), 150);
            ProjectFileTableModel.getColumn(table, Col.UPLOADED).setCellRenderer(new CustomeDateRenderer());
            ProjectFileTableModel.getColumn(table, Col.VERSIONS).setCellRenderer(new VersionListTableCellRenderer());

            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(final MouseEvent evt) {
                    if (evt.isPopupTrigger()) {
                        int rowAtPoint = getTable().rowAtPoint(evt.getPoint());
                        final FileAndVersion version = model.getRow(rowAtPoint);
                        if (preShowPopupMenu(version)) {
                            getPopup().show(getTable(), evt.getX(), evt.getY());
                        }
                    }
                }
            });
        }
        return table;
    }

    @Override
    public List<JButton> getToolbarButtons() {
        return Arrays.asList(getButProjectLink());
    }

    protected JTextArea getTxDesc() {
        if (txDesc == null) {
            txDesc = new JTextArea();
            txDesc.setEditable(false);
            txDesc.setLineWrap(true);
            txDesc.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
            txDesc.setOpaque(false);
            txDesc.setBackground(new Color(0, 0, 0, 0));
            txDesc.setWrapStyleWord(true);
        }
        return txDesc;
    }

    /**
     * Callback to allow changes to popup menu before showing.
     *
     * @return <code>true</code> to show the popup menu. <code>false</code>, otherwise.
     */
    protected boolean preShowPopupMenu(final FileAndVersion version) {
        return true;
    }

    protected void updatePaging() {
        paging.update();
    }

}
