package li.cryx.curse.rc.component;

import java.awt.Component;
import java.awt.Container;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JComponent;

/**
 * A simple focus traversal policy implementation.
 *
 * @author cryxli
 */
public class FocusTraversalPolicy extends java.awt.FocusTraversalPolicy {

    private final LinkedList<Component> order = new LinkedList<>();

    public FocusTraversalPolicy(final Collection<Component> order) {
        this.order.addAll(order);
    }

    public FocusTraversalPolicy(final Component... components) {
        this(Arrays.asList(components));
    }

    @Override
    public Component getComponentAfter(final Container focusCycleRoot, final Component aComponent) {
        int idx = (order.indexOf(aComponent) + 1) % order.size();

        // look for next enabled component
        final int start = order.indexOf(aComponent);
        while (!order.get(idx).isEnabled() && idx != start) {
            idx = (idx + 1) % order.size();
        }

        return order.get(idx);
    }

    @Override
    public Component getComponentBefore(final Container focusCycleRoot, final Component aComponent) {
        int idx = (order.indexOf(aComponent) + order.size() - 1) % order.size();

        // look for previous enabled component
        final int start = order.indexOf(aComponent);
        while (!order.get(idx).isEnabled() && idx != start) {
            idx = (idx + order.size() - 1) % order.size();
        }

        return order.get(idx);
    }

    @Override
    public Component getDefaultComponent(final Container focusCycleRoot) {
        return order.getFirst();
    }

    @Override
    public Component getLastComponent(final Container focusCycleRoot) {
        return order.getLast();
    }

    @Override
    public Component getFirstComponent(final Container focusCycleRoot) {
        return order.getFirst();
    }

    public void add(final JComponent components) {
        order.add(components);
    }

}
