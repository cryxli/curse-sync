package li.cryx.curse.rc.component.validate;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;

import li.cryx.curse.rc.common.Translation;

public class ValidationTextField extends JTextField implements ValidationComponent {

    private static final long serialVersionUID = 99690793999779060L;

    /** Border shown when component is not valid. */
    private static final Border errorBorder = BorderFactory.createLineBorder(Color.RED, 2);;

    /** Default border shown when component is valid. */
    private static Border okBorder;

    public ValidationTextField() {
        if (okBorder == null) {
            okBorder = getBorder();
        }
    }

    @Override
    public final boolean isValidComponent() {
        return validateComponent().isValid();
    }

    @Override
    public ValidationInfo validateComponent() {
        return new ValidationInfo();
    }

    @Override
    public final void validation() {
        final ValidationInfo vi = validateComponent();

        if (vi.isValid()) {
            setBorder(okBorder);
            setToolTipText(null);

        } else {
            setBorder(errorBorder);
            final Translation t = Translation.getInstance();
            final StringBuffer buf = new StringBuffer();
            boolean first = true;
            for (String key : vi.getErrors()) {
                if (first) {
                    first = false;
                } else {
                    buf.append('\n');
                }
                buf.append(t.get(key));
            }
            setToolTipText(buf.toString());
        }
    }

}
