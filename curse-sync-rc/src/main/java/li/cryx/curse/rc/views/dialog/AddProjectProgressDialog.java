package li.cryx.curse.rc.views.dialog;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JDialog;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.component.dialog.MessageUpdateDialog;
import li.cryx.curse.rc.views.ProjectList;

public class AddProjectProgressDialog extends MessageUpdateDialog {

    private final Component parent;

    public AddProjectProgressDialog(final ProjectList parent) {
        super(TConstants.VIEW_PROJECT_LIST.ADD_DIALOG_TITLE, TConstants.VIEW_PROJECT_LIST.ADD_DIALOG_MSG);
        this.parent = parent.getViewPanel();
    }

    @Override
    protected void preShowDialog() {
        final JDialog dialog = getDialog();
        final Dimension d = dialog.getSize();
        d.width = Math.max(d.width, 400);
        dialog.setSize(d);
        dialog.setLocationRelativeTo(parent);
    }

}
