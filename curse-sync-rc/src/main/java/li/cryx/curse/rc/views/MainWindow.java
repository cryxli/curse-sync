package li.cryx.curse.rc.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.views.ide.MainWindowIde;

public class MainWindow extends MainWindowIde {

    private static final Logger LOG = LoggerFactory.getLogger(MainWindow.class);

    private static MainWindow instance;

    public static MainWindow getInstance() {
        return instance;
    }

    private final AppConfig conf;

    /** Map toolbar buttons to views. */
    private final Map<JButton, ViewComponent> views = new HashMap<>();

    public MainWindow(final AppConfig conf) {
        this.conf = conf;
        instance = this;
    }

    private void addViews() {
        // link toolbar buttons to view instances
        views.put(getButProjectList(), new ProjectList(conf));
        views.put(getButSettings(), new Settings(conf));
        // views.put(getButWatchesOverview(), new WatchesOverview(conf));
        views.put(getButMultiMc(), new MultiMcView(conf));

        // add action listener to all toolbar buttons
        final ActionListener l = new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                // button causing the event
                final JButton but = (JButton) evt.getSource();
                // ... has a view assigned
                final ViewComponent view = views.get(but);
                if (view != null) {
                    // show that view
                    show(view);
                }
            }
        };
        for (JButton but : views.keySet()) {
            but.addActionListener(l);
        }
    }

    @Override
    protected void postInit() {
        getFrame().addWindowListener(new WindowAdapter() {
            // user is closing the main window
            @Override
            public void windowClosing(final WindowEvent evt) {
                // save pending changes to config
                try {
                    conf.save();
                } catch (IOException e) {
                    LOG.error("Failed to save config.", e);
                }
                // destroy the main window
                getFrame().dispose();
            }
        });

        addViews();
        // show(views.get(getButWatchesOverview()));
        show(views.get(getButProjectList()));
    }

}
