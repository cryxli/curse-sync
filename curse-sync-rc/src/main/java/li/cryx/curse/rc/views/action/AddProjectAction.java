package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.mng.ScanJsonMod;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.views.ProjectList;
import li.cryx.curse.rc.views.dialog.AddProjectDialog;
import li.cryx.curse.rc.views.dialog.AddProjectProgressDialog;
import li.cryx.curse.web.ProjectIdScraper;

public class AddProjectAction implements ActionListener {

    private static final Logger LOG = LoggerFactory.getLogger(AddProjectAction.class);

    private final ProjectList parent;

    private final AppConfig conf;

    public AddProjectAction(final ProjectList parent, final AppConfig conf) {
        this.parent = parent;
        this.conf = conf;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        final AddProjectDialog dialog = new AddProjectDialog();
        int result = dialog.showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isOkCode(result)) {
            final AddProjectProgressDialog progress = new AddProjectProgressDialog(parent);

            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    final ProjectIdScraper scraper = conf.getBean(ProjectIdScraper.class);
                    final ScanJsonMod scan = conf.getBean(ScanJsonMod.class);
                    try {
                        final Long projectId = scraper.process(dialog.getUrl());
                        if (projectId != null) {
                            scan.process(projectId);
                        }
                    } catch (IOException e) {
                        LOG.error("Error adding project: " + dialog.getUrl(), e);
                    }
                    return null;
                }

                @Override
                protected void done() {
                    progress.hideDialog();
                    parent.preShow();
                }
            }.execute();

            result = progress.showDialog(parent.getViewPanel());
            if (ButtonBasedDialog.isCancelCode(result)) {
                conf.getBean(ScanJsonMod.class).interrupt();
            }
        }
    }

}
