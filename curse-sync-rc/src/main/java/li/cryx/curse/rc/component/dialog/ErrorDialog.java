package li.cryx.curse.rc.component.dialog;

public class ErrorDialog extends MessageDialog {

    public ErrorDialog(final String titleKey, final String msgKey) {
        super(DIALOG_TYPE_ERROR, titleKey, msgKey);
    }

}
