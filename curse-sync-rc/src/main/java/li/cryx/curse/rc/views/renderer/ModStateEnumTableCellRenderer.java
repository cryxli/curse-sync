package li.cryx.curse.rc.views.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.model.ModStateEnum;

/**
 * A table cell renderer that knows how to represent {@link ModStateEnum}s.
 *
 * @author cryxli
 */
public class ModStateEnumTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -5911287986499208861L;

    public static String getStateToolTip(final ModStateEnum state) {
        Translation t = Translation.getInstance();
        switch (state) {
        case UP_TO_DATE:
            return t.get(TConstants.ENUM.MOD_STATE.UP_TO_DATE);
        case OUTDATED:
            return t.get(TConstants.ENUM.MOD_STATE.OUTDATED);
        case UNKNOWN:
        default:
            return t.get(TConstants.ENUM.MOD_STATE.UNKNOWN);
        }
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);
        label.setOpaque(true);

        final ModStateEnum state;
        if (value instanceof ModStateEnum) {
            state = (ModStateEnum) value;
        } else {
            state = ModStateEnum.UNKNOWN;
        }
        label.setText(state.icon().toString());
        label.setForeground(state.color());
        label.setToolTipText(getStateToolTip(state));

        label.setFont(state.icon().getFont());
        label.setHorizontalAlignment(SwingConstants.CENTER);

        return this;
    }

}
