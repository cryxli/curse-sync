package li.cryx.curse.rc.views.dialog;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.commons.lang3.StringUtils;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;

public class AddProjectDialog extends ButtonBasedDialog {

    private final Translation t = Translation.getInstance();

    private JTextArea txUrl;

    public AddProjectDialog() {
        super(DIALOG_TYPE_INFO, TConstants.VIEW_PROJECT_LIST.ADD_DIALOG_TITLE);
        removeAllButtons();
        setButton(ACTION_CODE_OK, Fa.CHECK.decorateButton(createButton(ACTION_CODE_OK), t.get("OK")));
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
        setSize(400, 170);
    }

    @Override
    protected JPanel getMainPanel() {
        final JPanel main = new JPanel(new BorderLayout());
        main.add(new JLabel(t.get(TConstants.VIEW_PROJECT_LIST.ADD_DIALOG_HINT)), BorderLayout.NORTH);
        main.add(getTxUrl(), BorderLayout.CENTER);
        return main;
    }

    protected JTextArea getTxUrl() {
        if (txUrl == null) {
            txUrl = new JTextArea();
            txUrl.setLineWrap(true);
        }
        return txUrl;
    }

    public String getUrl() {
        return StringUtils.stripToEmpty(getTxUrl().getText());
    }

    @Override
    public int showDialog() {
        try {
            final String s = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            getTxUrl().setText(StringUtils.stripToEmpty(s));
        } catch (IOException | HeadlessException | UnsupportedFlavorException e) {
            Logger.getGlobal().finest(e.getMessage());
        }

        return super.showDialog();
    }

}
