package li.cryx.curse.rc.views.renderer;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import li.cryx.curse.rc.model.MultiMcInstance;

/**
 * Renders {@link MultiMcInstance}s in a <code>JList</code> to select one of the presented instances.
 *
 * @author cryxli
 */
public class MultiMcInstanceListRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1657441990616160666L;

    @Override
    public JPanel getListCellRendererComponent(final JList<?> list, final Object val, final int index,
            final boolean isSelected, final boolean cellHasFocus) {
        // default renderer is not typed
        final MultiMcInstance value = (MultiMcInstance) val;

        final JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(true);

        final JLabel lbText = new JLabel();
        panel.add(lbText, BorderLayout.NORTH);
        lbText.setOpaque(false);
        lbText.setText(value.getName());
        lbText.setFont(lbText.getFont().deriveFont(Font.BOLD));

        final JLabel lbVersion = new JLabel();
        panel.add(lbVersion, BorderLayout.SOUTH);
        lbVersion.setOpaque(false);
        lbVersion.setText(value.getVersion());
        lbVersion.setFont(lbVersion.getFont().deriveFont(10f));
        lbVersion.setHorizontalAlignment(SwingConstants.RIGHT);

        // apply super properties
        panel.setEnabled(list.isEnabled());
        panel.setFont(list.getFont());

        // have default renderer calculate colours
        final JLabel c = (JLabel) super.getListCellRendererComponent(list, val, index, isSelected, cellHasFocus);
        panel.setBackground(c.getBackground());
        lbText.setForeground(c.getForeground());
        lbVersion.setForeground(c.getForeground());
        panel.setBorder(c.getBorder());

        return panel;
    }

}
