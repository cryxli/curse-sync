package li.cryx.curse.rc.worker;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.SwingWorker;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.mng.ScanJsonMod;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.model.filter.ProjectSearchFilter;
import li.cryx.curse.rc.views.dialog.ScanForUpdateProjectDialog;

/**
 * Swing worker to scan Curse.com for project file updates.
 *
 * <p>
 * This worker can be used in two ways. With the {@link #addProject(Project) method the project to check can be set
 * manually. By default the worker will scan the database for projects that are marked for update and inspect all of
 * them.</p>
 *
 * @author cryxli
 */
// TODO prototype bean ?
public class ScanForUpdateProjectsWorker extends SwingWorker<Void, Pair<String, Integer>> {

    private static final Logger LOG = LoggerFactory.getLogger(ScanForUpdateProjectsWorker.class);

    /** Dialog to show the progress. */
    private final ScanForUpdateProjectDialog progressDialog;

    /** Access current projects in database. */
    private ProjectDao projectDao;

    /** The scanner. */
    // private ScanNewMod scan;
    private ScanJsonMod scan;

    /** List of pending projects to scan. */
    private final Queue<Project> fifo = new LinkedList<>();

    /** Total number of projects to scan. */
    private int total;

    /** Indicator that the worker has been started. */
    private boolean running;

    public ScanForUpdateProjectsWorker(final ScanForUpdateProjectDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    /**
     * Manually configure the worker by adding a project to check. Adding projects that way disables the database scan
     * to initialise the worker.
     *
     * @param prj
     *            A project to check against Curse.com for updated files. Must not be <code>null</code>, but will not
     *            result in an error.
     * @throws IllegalStateException
     *             This method can only be called before the worker is started. Later an exception is thrown.
     */
    public void addProject(final Project prj) throws IllegalStateException {
        if (!running) {
            if (prj != null) {
                fifo.add(prj);
                total = fifo.size();
            }
        } else {
            throw new IllegalStateException("Worker can only be configured before it is started.");
        }
    }

    public void cancel() {
        fifo.clear();
    }

    @Override
    protected Void doInBackground() throws Exception {
        running = true;
        if (fifo.isEmpty()) {
            final ProjectSearchFilter filter = new ProjectSearchFilter();
            filter.setUpdate(true);
            filter.noPaging();
            fifo.addAll(projectDao.search(filter));
            total = fifo.size();
        }

        int counter = 0;
        while (!fifo.isEmpty()) {
            Project p = fifo.poll();
            publish(new ImmutablePair<>(p.getTitle(), counter++));
            try {
                // scan.process(p.getUrl(), false);
                scan.process(p.getId());
            } catch (IOException e) {
                LOG.error("Error updating project " + p.getTitle(), e);
            }
            publish(new ImmutablePair<>("...", counter));
        }

        return null;
    }

    @Override
    protected void done() {
        running = false;
        progressDialog.hideDialog();
    }

    @Override
    protected void process(final List<Pair<String, Integer>> chunks) {
        // only latest
        final Pair<String, Integer> update = chunks.get(chunks.size() - 1);
        progressDialog.updateSteps(update.getRight(), total);
        progressDialog.updateMsg(update.getLeft());
    }

    public void setProjectDao(final ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    public void setScanJsonMod(final ScanJsonMod scan) {
        this.scan = scan;
    }

}
