package li.cryx.curse.rc.views.model;

import java.util.List;

import javax.swing.JTable;
import javax.swing.table.TableColumn;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.model.db.FileAndVersion;

public class ProjectFileTableModel extends ObjectRowTableModel<FileAndVersion> {

    /** Name table columns */
    public static enum Col {
        NAME, RELEASE, UPLOADED, VERSIONS,

        UNKNOWN;

        public static Col of(final int index) {
            if (index < 0 || index > values().length - 1) {
                return UNKNOWN;
            } else {
                return values()[index];
            }
        }
    }

    private static final long serialVersionUID = -7338708311177372031L;

    public static TableColumn getColumn(final JTable table, final Col column) {
        return table.getColumnModel().getColumn(column.ordinal());
    }

    private final Translation t = Translation.getInstance();

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case NAME:
        case RELEASE:
        default:
            return String.class;
        case UPLOADED:
            return Long.class;
        case VERSIONS:
            return List.class;
        }
    }

    @Override
    public int getColumnCount() {
        return Col.values().length - 1;
    }

    @Override
    public String getColumnName(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case NAME:
            return t.get(TConstants.VIEW_PROJECT_DETAIL.COLS.FILENAME);
        case RELEASE:
            return t.get(TConstants.VIEW_PROJECT_DETAIL.COLS.RELEASE);
        case UPLOADED:
            return t.get(TConstants.VIEW_PROJECT_DETAIL.COLS.UPLOADED);
        case VERSIONS:
            return t.get(TConstants.VIEW_PROJECT_DETAIL.COLS.VERSIONS);
        default:
            return super.getColumnName(columnIndex);
        }
    }

    @Override
    protected Object getValueAt(final FileAndVersion row, final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case NAME:
            return row.getName();
        case RELEASE:
            return row.getRelease();
        case UPLOADED:
            return row.getUploaded();
        case VERSIONS:
            return row.getVersions();
        default:
            return "?";
        }
    }

    @Override
    public void setValueAt(final Object aValue, final FileAndVersion row, final int columnIndex) {
    }

}
