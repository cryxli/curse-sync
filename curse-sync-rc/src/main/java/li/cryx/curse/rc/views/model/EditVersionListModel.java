package li.cryx.curse.rc.views.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultListModel;

import li.cryx.curse.rc.model.McVersion;
import li.cryx.curse.rc.model.db.FileVersion;
import li.cryx.curse.rc.views.model.EditVersionListModel.SelectableMcVersion;

/**
 * A <code>JList</code> model to display minecraft versions applicable to a project file.
 *
 * @author cryxli
 */
public class EditVersionListModel extends DefaultListModel<SelectableMcVersion> {

    /**
     * Decorator class to keep track of selected {@link McVersion}s.
     *
     * @author cryxli
     */
    public static class SelectableMcVersion extends McVersion {

        /** Indicator that the minecraft version is selected and therefore the file works with that version. */
        private boolean selected;

        /** Copy constructor */
        public SelectableMcVersion(final McVersion version) {
            super(version.getId(), version.key(), version.getValue());
        }

        /**
         * Indicator that the version is selected.
         *
         * @return <code>true</code> version is selected, <code>false</code> otherwise.
         */
        public boolean isSelected() {
            return selected;
        }

        /**
         * Set indicator that the vesion is selected.
         *
         * @param selected
         *            <code>true</code> version is selected, <code>false</code> otherwise.
         */
        public void setSelected(final boolean selected) {
            this.selected = selected;
        }

    }

    private static final long serialVersionUID = -4742659295769498192L;

    /**
     * Get all selected minecraft versions.
     *
     * @return List of selected versions.
     */
    public List<McVersion> getSelectedEntries() {
        final List<McVersion> list = new LinkedList<>();
        for (int index = 0; index < getSize(); index++) {
            final SelectableMcVersion v = getElementAt(index);
            if (v.isSelected()) {
                list.add(v);
            }
        }
        return list;
    }

    /** De-select all versions. */
    public void reset() {
        for (int index = 0; index < getSize(); index++) {
            final SelectableMcVersion v = getElementAt(index);
            v.setSelected(false);
        }
        if (getSize() > 0) {
            fireContentsChanged(this, 0, getSize());
        }
    }

    /**
     * Mark given version as selected.
     *
     * @param versions
     *            Collection of versions to mark as selected.
     */
    public void select(final Collection<FileVersion> versions) {
        for (FileVersion v : versions) {
            select(v);
        }

    }

    /**
     * Mark a version as selected
     *
     * @param version
     *            A {@link FileVersion} corresponding to a {@link McVersion} that should be shown as selected.
     */
    public void select(final FileVersion version) {
        // find matching version entry
        for (int index = 0; index < getSize(); index++) {
            final SelectableMcVersion v = getElementAt(index);
            if (v.getValue().equals(version.getVersion())) {
                // mark as selected
                v.setSelected(true);
                fireContentsChanged(this, index, index);
                break;
            }
        }
    }

    /**
     * Fill the model with all known minecraft versions.
     *
     * @param versions
     *            Collection of configured minecraft versions.
     */
    public void setData(final Collection<McVersion> versions) {
        clear();
        final List<McVersion> list = new LinkedList<>(versions);
        // reverse sort by technical key, assuming that a newer version has a higher key
        Collections.sort(list, new Comparator<McVersion>() {
            @Override
            public int compare(final McVersion v1, final McVersion v2) {
                return Integer.valueOf(v2.key()).compareTo(v1.key());
            }
        });
        for (McVersion v : list) {
            addElement(new SelectableMcVersion(v));
        }
    }

    /**
     * Toggle the <code>selected</code> property of the indicated entry.
     *
     * @param entry
     *            An entry of the model.
     */
    public void toggleSelect(final SelectableMcVersion entry) {
        int index = indexOf(entry);
        if (index > -1) {
            final SelectableMcVersion v = getElementAt(index);
            v.setSelected(!v.isSelected());
            fireContentsChanged(this, index, index);
        }
    }

}
