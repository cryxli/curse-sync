package li.cryx.curse.rc.component.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.common.font.Icon;
import li.cryx.curse.rc.component.ValueButton;

public abstract class ButtonBasedDialog {

    public static final int ACTION_CODE_OK = 1;

    public static final int ACTION_CODE_CANCEL = 2;

    public static final int ACTION_CODE_YES = 3;

    public static final int ACTION_CODE_NO = 4;

    public static final int DIALOG_TYPE_INFO = 1;

    public static final int DIALOG_TYPE_QUESTION = 2;

    public static final int DIALOG_TYPE_WARNING = 3;

    public static final int DIALOG_TYPE_ERROR = 4;

    public static boolean isCancelCode(final int actionCode) {
        return ACTION_CODE_CANCEL == actionCode;
    }

    public static boolean isOkCode(final int actionCode) {
        return ACTION_CODE_OK == actionCode;
    }

    public static boolean isYesCode(final int actionCode) {
        return ACTION_CODE_YES == actionCode;
    }

    private JDialog dialog;

    private JPanel main;

    private int type;

    private int result = -1;

    private Dimension size;

    private JLabel dialogIcon;

    private final Map<Integer, ValueButton<Integer>> buttonMap = new LinkedHashMap<>();

    private String title;

    public ButtonBasedDialog() {
        this(DIALOG_TYPE_INFO);
    }

    public ButtonBasedDialog(final int type) {
        changeType(type);
    }

    public ButtonBasedDialog(final int type, final String titleKey) {
        changeType(type);
        setTitle(titleKey);
    }

    public void changeType(final int type) {
        int oldType = this.type;
        switch (type) {
        case DIALOG_TYPE_ERROR:
        case DIALOG_TYPE_INFO:
        case DIALOG_TYPE_QUESTION:
        case DIALOG_TYPE_WARNING:
            this.type = type;
            break;
        default:
            this.type = DIALOG_TYPE_INFO;
            break;
        }

        if (oldType != this.type && dialog != null) {
            // change icon on dialog
            createDialogIcon();
        }
    }

    protected ValueButton<Integer> createButton(final int actionCode) {
        return new ValueButton<>(actionCode);
    }

    private void createDialogIcon() {
        final Icon text;
        final Color color;

        switch (type) {
        case DIALOG_TYPE_ERROR:
            text = Fa.TIMES_CIRCLE;
            color = li.cryx.curse.rc.common.Color.ERROR;
            break;
        default:
        case DIALOG_TYPE_INFO:
            text = Fa.INFO_CIRCLE;
            color = li.cryx.curse.rc.common.Color.INFO;
            break;
        case DIALOG_TYPE_QUESTION:
            text = Fa.QUESTION_CIRCLE;
            color = li.cryx.curse.rc.common.Color.SUCCESS;
            break;
        case DIALOG_TYPE_WARNING:
            text = Fa.EXCLAMATION_CIRCLE;
            color = li.cryx.curse.rc.common.Color.WARNING;
            break;
        }

        if (dialogIcon != null) {
            main.remove(dialogIcon);
        }
        dialogIcon = new JLabel(text.toString());
        dialogIcon.setFont(text.getFont().deriveFont(Font.PLAIN, 28));
        dialogIcon.setForeground(color);
        dialogIcon.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        dialogIcon.setVerticalAlignment(SwingConstants.TOP);
        main.add(dialogIcon, BorderLayout.WEST);
    }

    public int getAction() {
        return result;
    }

    protected JButton getButton(final int actionCode) {
        return buttonMap.get(actionCode);
    }

    protected JDialog getDialog() {
        if (dialog == null) {
            dialog = new JDialog();
            dialog.setModal(true);
            dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(final WindowEvent evt) {
                    result = ACTION_CODE_CANCEL;
                    dialog.dispose();
                }
            });
            dialog.setTitle(title);

            main = new JPanel(new BorderLayout());
            dialog.setContentPane(main);
            main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            createDialogIcon();

            final ActionListener butListener = new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    @SuppressWarnings("unchecked")
                    final ValueButton<Integer> but = (ValueButton<Integer>) evt.getSource();
                    if (preDoAction(but.getValue())) {
                        result = but.getValue();
                        getDialog().dispose();
                    }
                }
            };

            if (buttonMap.isEmpty()) {
                buttonMap.put(ACTION_CODE_OK,
                        Fa.CHECK.decorateButton(createButton(ACTION_CODE_OK), Translation.getInstance().get("OK")));
            }
            final JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
            main.add(buttonPanel, BorderLayout.SOUTH);
            for (ValueButton<Integer> but : buttonMap.values()) {
                buttonPanel.add(but);
                but.addActionListener(butListener);
            }

            final JPanel panel = getMainPanel();
            if (panel == null) {
                throw new IllegalStateException("Main panel is not provided!");
            }
            main.add(panel, BorderLayout.CENTER);

            if (size == null) {
                dialog.pack();
            } else {
                dialog.setSize(size);
            }
        }
        return dialog;
    }

    protected abstract JPanel getMainPanel();

    protected void hideDialog(final int returnCode) {
        result = returnCode;
        getDialog().dispose();
    }

    protected boolean preDoAction(final int actionCode) {
        return true;
    }

    protected void preShowDialog() {
    }

    protected void removeAllButtons() {
        if (dialog == null) {
            buttonMap.clear();
        }
    };

    protected void setButton(final int actionCode, final ValueButton<Integer> button) {
        if (dialog == null) {
            button.setValue(actionCode);
            buttonMap.put(actionCode, button);
        }
    }

    protected void setSize(final Dimension size) {
        this.size = size;
    }

    protected void setSize(final int width, final int height) {
        setSize(new Dimension(width, height));
    }

    public void setTitle(final String titleKey) {
        setTranslatedTitle(Translation.getInstance().get(titleKey));
    }

    public void setTranslatedTitle(final String title) {
        this.title = title;
    }

    public int showDialog() {
        preShowDialog();
        getDialog().setVisible(true);
        return result;
    }

    public int showDialog(final Component parent) {
        final Container c = SwingUtilities.getUnwrappedParent(parent);
        getDialog().setLocationRelativeTo(c != null ? c : parent);
        return showDialog();
    }

}
