package li.cryx.curse.rc.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import li.cryx.curse.db.actor.DeleteModVersionActor;
import li.cryx.curse.db.actor.FileAndVersionReadActor;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.component.dialog.YesNoConfirmDialog;
import li.cryx.curse.rc.jdbc.FileMapper;
import li.cryx.curse.rc.model.db.FileAndVersion;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.model.filter.ModFileSearchFilter;
import li.cryx.curse.rc.views.action.AbstractOpenUrlAction;
import li.cryx.curse.rc.views.ide.ProjectDetailIde;

public class ProjectDetail extends ProjectDetailIde {

    private final AppConfig conf;

    private Project project;

    private final FileAndVersionReadActor searchActor;

    private final DeleteModVersionActor deleteActor;

    public ProjectDetail(final AppConfig conf) {
        this.conf = conf;
        searchActor = conf.getBean(FileAndVersionReadActor.class);
        deleteActor = conf.getBean(DeleteModVersionActor.class);
    }

    protected void onDeleteModVersion(final FileAndVersion version) {
        deleteActor.delete(version.getId());

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                getModel().setData(searchActor.search(getFilter()));
                updatePaging();
            }
        });
    }

    protected void onEditFileVersion(final FileAndVersion fileAndVersion) {
        final FileVersionView view = new FileVersionView(conf);
        view.setFileVersion(fileAndVersion);
        MainWindow.getInstance().show(view);
    }

    @Override
    protected void postInit() {
        getButProjectLink().addActionListener(new AbstractOpenUrlAction() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                openUrl(project.getUrl(), TConstants.VIEW_PROJECT_DETAIL.ERROR.OPEN_PAGE);
            }
        });

        getButOpenVersionPage().addActionListener(new AbstractOpenUrlAction() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                openUrl(getButOpenVersionPage().getPayload(), TConstants.VIEW_PROJECT_DETAIL.ERROR.OPEN_VERSION_PAGE);
            }
        });
        getButDeleteVersion().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                final int result = new YesNoConfirmDialog(TConstants.VIEW_PROJECT_DETAIL.DELETE_VERSION_TITLE,
                        TConstants.VIEW_PROJECT_DETAIL.DELETE_VERSION_QUESTION).showDialog(getTable());
                if (result == ButtonBasedDialog.ACTION_CODE_YES) {
                    onDeleteModVersion(getButDeleteVersion().getPayload());
                }
            }
        });
        getButEditVersion().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                onEditFileVersion(getButEditVersion().getPayload());
            }
        });
    }

    @Override
    protected void preShow() {
        setTitle(project.getTitle());
        getTxDesc().setText(project.getDesc());

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                getModel().setData(searchActor.search(getFilter()));
                updatePaging();
            }
        });
    }

    @Override
    protected boolean preShowPopupMenu(final FileAndVersion version) {
        getButOpenVersionPage().setPayload(version.getDetail());
        getButDeleteVersion().setPayload(version);
        getButEditVersion().setPayload(version);
        return true;
    }

    public void setProject(final Project project) {
        this.project = project;

        final ModFileSearchFilter filter = getFilter();
        filter.setProjectId(project.getId());
        filter.setOrderBy(FileMapper.ATTR_UPLOADED);
        filter.setOrderDesc();
        filter.setOffset(0);
    }

}
