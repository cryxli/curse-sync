package li.cryx.curse.rc.views.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.TableAspect;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.model.MultiMcInstance;
import li.cryx.curse.rc.views.renderer.MultiMcInstanceListRenderer;

public class InstanceSelectDialog extends ButtonBasedDialog {

    private JList<MultiMcInstance> list;

    protected DefaultListModel<MultiMcInstance> model = new DefaultListModel<>();

    public InstanceSelectDialog(final List<MultiMcInstance> list) {
        super(DIALOG_TYPE_QUESTION, TConstants.VIEW_MULTIMC.SELECT_INSTANCE.TITLE);
        final Translation t = Translation.getInstance();
        removeAllButtons();
        setButton(ACTION_CODE_OK, Fa.CHECK.decorateButton(createButton(ACTION_CODE_OK), t.get("OK")));
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));

        for (MultiMcInstance i : list) {
            model.addElement(i);
        }
    }

    protected JList<MultiMcInstance> getList() {
        if (list == null) {
            list = new JList<>(model);
            list.setCellRenderer(new MultiMcInstanceListRenderer());
            TableAspect.setSingleSelection(list);
            list.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(final ListSelectionEvent evt) {
                    getButton(ACTION_CODE_OK).setEnabled(list.getSelectedIndex() > -1);
                }
            });
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if (e.getClickCount() > 1) {
                        getButton(ACTION_CODE_OK).doClick();
                    }
                }
            });
        }
        return list;
    }

    @Override
    protected JPanel getMainPanel() {
        final JPanel main = new JPanel(new BorderLayout());
        main.add(new JScrollPane(getList()), BorderLayout.CENTER);
        return main;
    }

    public MultiMcInstance getSelectedInstance() {
        if (getAction() == ACTION_CODE_OK) {
            return model.getElementAt(list.getSelectedIndex());
        } else {
            return null;
        }
    }

    @Override
    protected void preShowDialog() {
        final Dimension dim = getDialog().getSize();
        dim.width = Math.max(400, dim.width);
        getDialog().setSize(dim);

        getButton(ACTION_CODE_OK).setEnabled(false);
    }

}
