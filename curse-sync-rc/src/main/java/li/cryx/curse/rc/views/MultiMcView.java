package li.cryx.curse.rc.views;

import java.io.File;
import java.util.List;

import javax.swing.SwingUtilities;

import li.cryx.curse.mng.AnalyseMultiMcInstances;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.ModStateEnum;
import li.cryx.curse.rc.model.MultiMcInstance;
import li.cryx.curse.rc.views.action.AnalyseInstanceDependenciesAction;
import li.cryx.curse.rc.views.action.OpenMcInstanceAction;
import li.cryx.curse.rc.views.action.OpenPageAction;
import li.cryx.curse.rc.views.action.UpdateAllModsAction;
import li.cryx.curse.rc.views.action.UpdateSingleModAction;
import li.cryx.curse.rc.views.dialog.InstanceSelectDialog;
import li.cryx.curse.rc.views.ide.MultiMcViewIde;

public class MultiMcView extends MultiMcViewIde {

    private boolean wasWarned = false;

    private List<MultiMcInstance> list;

    public MultiMcView(final AppConfig conf) {
        super(conf);
    }

    public MultiMcInstance getInstance() {
        return instance;
    }

    public List<ModState> getModsForInstance() {
        return getModel().getAllRows();
    }

    @Override
    protected void onSelectInstance() {
        final InstanceSelectDialog dialog = new InstanceSelectDialog(list);
        dialog.showDialog(getViewPanel());
        showInstance(dialog.getSelectedInstance());
    }

    @Override
    protected void postInit() {
        getButUpdateAll().addActionListener(new UpdateAllModsAction(conf, this));
        getButOpenDirectory().addActionListener(new OpenMcInstanceAction(this));
        getButAnalyseDependencies().addActionListener(new AnalyseInstanceDependenciesAction(conf, this));

        getButOpenProjectPage().addActionListener(new OpenPageAction(TConstants.VIEW_PROJECT_LIST.ERROR_PROJECT_URL));
        getButOpenFilePage().addActionListener(new OpenPageAction(TConstants.VIEW_MULTIMC.ERROR_FILE_URL));
        getButOpenDownloadPage().addActionListener(new OpenPageAction(TConstants.VIEW_MULTIMC.ERROR_DOWNLOAD_URL));
        getButUpdateMod().addActionListener(new UpdateSingleModAction(conf, this));
    }

    @Override
    protected void preShow() {
        if (conf.getMultimcDir() == null) {
            wasWarned = true;
            return;
        } else if (wasWarned) {
            createView();
        }

        // decide which instance to display
        list = conf.getBean(AnalyseMultiMcInstances.class).process(conf.getMultimcDir());

        MultiMcInstance instance = null;
        getButSelectInstance().setEnabled(!list.isEmpty());
        if (!list.isEmpty()) {
            getButSelectInstance().setToolTipText(Translation.getInstance().get(TConstants.VIEW_MULTIMC.BUT_INSTANCE));
            if (conf.getMultimcInstance() != null) {
                final File f = new File(conf.getMultimcInstance());
                for (MultiMcInstance i : list) {
                    if (i.getDir().equals(f)) {
                        instance = i;
                        break;
                    }
                }
            }
            if (instance == null) {
                instance = list.get(0);
            }
        } else {
            getButSelectInstance()
                    .setToolTipText(Translation.getInstance().get(TConstants.VIEW_MULTIMC.BUT_INSTANCE_NODATA));
        }
        showInstance(instance);
    }

    @Override
    protected boolean preShowPopupMenu(final ModState state) {
        final boolean outdated = state.getState() == ModStateEnum.OUTDATED;

        getButOpenProjectPage().setPayload(state.getProjectUrl());

        getButOpenFilePage().setPayload(state.getFilePage());

        if (outdated) {
            getTxNewVersionInfo().setText(
                    Translation.getInstance().get(TConstants.VIEW_MULTIMC.INFO_NEW_VERSION, state.getLatestName()));
        }
        getTxNewVersionInfo().setVisible(outdated);

        getButOpenDownloadPage().setEnabled(outdated);
        getButOpenDownloadPage().setPayload(state.getDownloadPage());

        getButUpdateMod().setEnabled(outdated);
        getButUpdateMod().setPayload(state);

        return state.getProjectUrl() != null;
    }

    public void refreshModList() {
        showInstance(instance);
    }

    @Override
    protected void showInstance(final MultiMcInstance instance) {
        super.showInstance(instance);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                AnalyseMultiMcInstances ana = conf.getBean(AnalyseMultiMcInstances.class);
                getModel().clear();
                int count = 0;
                for (ModState mod : ana.analiseModFiles(instance)) {
                    getModel().addRow(mod);
                    count += mod.getState() == ModStateEnum.OUTDATED ? 1 : 0;
                }
                getButUpdateAll().setEnabled(count > 0);
            }
        });
    }

}
