package li.cryx.curse.rc.views.renderer;

import java.awt.Component;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import li.cryx.curse.rc.model.db.FileVersion;

public class VersionListTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 8322108238469112L;

    @SuppressWarnings("unchecked")
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);

        if (value != null && value instanceof List) {
            StringBuffer buf = new StringBuffer();
            boolean first = true;
            for (FileVersion v : (List<FileVersion>) value) {
                if (first) {
                    first = false;
                } else {
                    buf.append("<br>");
                }
                buf.append(v.getVersion());
            }
            if (buf.length() > 0) {
                buf.insert(0, "<html>");
                buf.append("</html>");
            }
            label.setText(buf.toString());
            table.setRowHeight(row, label.getPreferredSize().height);

        } else {
            label.setText("");
        }

        return label;
    }

}
