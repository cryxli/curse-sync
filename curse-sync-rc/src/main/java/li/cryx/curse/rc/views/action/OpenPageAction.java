package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;

import li.cryx.curse.rc.component.MenuItem;

public class OpenPageAction extends AbstractOpenUrlAction {

    private final String errorMessageKey;

    public OpenPageAction(final String errorMessageKey) {
        this.errorMessageKey = errorMessageKey;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        @SuppressWarnings("unchecked")
        final MenuItem<String> item = (MenuItem<String>) evt.getSource();
        openUrl(item.getPayload(), errorMessageKey);
    }

}
