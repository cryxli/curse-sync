package li.cryx.curse.rc.views.model;

import javax.swing.JTable;
import javax.swing.table.TableColumn;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.ModStateEnum;
import li.cryx.curse.rc.views.MultiMcView;

/**
 * Model behind table of {@link MultiMcView}.
 *
 * @author cryxli
 */
public class ModStateTableModel extends ObjectRowTableModel<ModState> {

    /** Name table columns */
    public static enum Col {
        TITLE, STATE, JAR, RELEASE,

        UNKNOWN;

        public static Col of(final int index) {
            if (index < 0 || index > values().length - 1) {
                return UNKNOWN;
            } else {
                return values()[index];
            }
        }
    }

    private static final long serialVersionUID = 3040542534201888791L;

    public static TableColumn getColumn(final JTable table, final Col column) {
        return table.getColumnModel().getColumn(column.ordinal());
    }

    public static int getColumnIndex(final Col column) {
        return column.ordinal();
    }

    private final Translation t = Translation.getInstance();

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case STATE:
            return ModStateEnum.class;
        default:
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return Col.values().length - 1;
    }

    @Override
    public String getColumnName(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case TITLE:
            return t.get(TConstants.VIEW_MULTIMC.COLS.PROJECT_TITLE);
        case STATE:
            return t.get(TConstants.VIEW_MULTIMC.COLS.STATE);
        case JAR:
            return t.get(TConstants.VIEW_MULTIMC.COLS.FILENAME);
        case RELEASE:
            return t.get(TConstants.VIEW_MULTIMC.COLS.RELEASE);
        default:
            return super.getColumnName(columnIndex);
        }
    }

    @Override
    protected Object getValueAt(final ModState row, final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case TITLE:
            return row.getTitle();
        case STATE:
            return row.getState();
        case JAR:
            return row.getFilename();
        case RELEASE:
            return row.getRelease();
        default:
            return "?";
        }
    }

    @Override
    public void setValueAt(final Object aValue, final ModState row, final int columnIndex) {
        // nothing to do
    }

}
