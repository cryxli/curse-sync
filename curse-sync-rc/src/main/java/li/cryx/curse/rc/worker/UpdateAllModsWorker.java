package li.cryx.curse.rc.worker;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.SwingWorker;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.views.dialog.UpdateAllModsDialog;

/**
 * Swing worker to update all mods of a MultiMC instance that have known newer versions.
 *
 * <p>
 * This worker will delegate processing of each mod to a {@link UpdateSingleModWorker}.
 * </p>
 *
 * @author cryxli
 */
public class UpdateAllModsWorker extends SwingWorker<Void, Pair<String, Integer>> {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateAllModsWorker.class);

    /** Config for mod directory */
    private final AppConfig conf;

    /** Progress dialog */
    private final UpdateAllModsDialog dialog;

    /** List of mods to update */
    private Queue<ModState> fifo;

    /** Last finished step */
    private int step;

    /** Total number of steps to finish */
    private final int total;

    /** Indicator that the worker is running */
    private boolean running;

    /** Current instance of delegated worker */
    private UpdateSingleModWorker singleWorker;

    public UpdateAllModsWorker(final AppConfig conf, final UpdateAllModsDialog dialog,
            final Collection<ModState> mods) {
        this.conf = conf;
        this.dialog = dialog;
        if (mods instanceof Queue) {
            fifo = (Queue<ModState>) mods;
        } else {
            fifo = new LinkedList<>(mods);
        }
        total = fifo.size();
    }

    public void cancel() {
        fifo.clear();
        running = false;
        if (singleWorker != null) {
            singleWorker.cancel();
        }
    }

    @Override
    protected Void doInBackground() throws Exception {
        running = true;
        LOG.info("Updating {} mods", total);

        while (running && !fifo.isEmpty()) {
            // next mod
            final ModState state = fifo.poll();
            publish(new ImmutablePair<>(state.getTitle(), step));
            LOG.info("Updating {}", state.getTitle());

            // delegate work
            singleWorker = new UpdateSingleModWorker(conf, null, state);
            singleWorker.setRestTemplate(conf.getBean(RestTemplate.class));
            // do not start a new thread, the worker does not need to update anything on the AWT thread
            singleWorker.doInBackground();

            step++;
            publish(new ImmutablePair<>(null, step));
        }
        return null;
    }

    @Override
    protected void done() {
        running = false;
        if (dialog != null) {
            dialog.hideDialog();
        }
    }

    @Override
    protected void process(final List<Pair<String, Integer>> chunks) {
        if (dialog != null) {
            // show progress in dialog
            final Pair<String, Integer> u = chunks.get(chunks.size() - 1);
            dialog.updateProgressMsg(u.getKey());
            dialog.updateProgressSteps(u.getValue(), total);
        }
    }

}
