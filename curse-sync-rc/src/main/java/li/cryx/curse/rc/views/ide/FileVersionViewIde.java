package li.cryx.curse.rc.views.ide;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import li.cryx.curse.db.DescriptionDao;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.views.model.EditVersionListModel;
import li.cryx.curse.rc.views.model.EditVersionListModel.SelectableMcVersion;
import li.cryx.curse.rc.views.renderer.SelectableMcVersionListRenderer;

public class FileVersionViewIde extends ViewComponent {

    private final Translation t = Translation.getInstance();

    private JTextField txFileName;

    private JList<SelectableMcVersion> list;

    private EditVersionListModel model;

    private JButton butSave;

    protected AppConfig conf;

    protected FileVersionViewIde(final AppConfig conf) {
        this.conf = conf;
    }

    @Override
    protected void createView() {
        final JPanel main = new JPanel(new FormLayout("p,4dlu,f:p:g", "p,2dlu,f:p:g,6dlu,p"));
        final CellConstraints cc = new CellConstraints();

        setMainPanel(main);
        main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        main.add(new JLabel(t.get(TConstants.VIEW_FILE_VERSION.LABEL_FILE_NAME)), cc.xy(1, 1));
        main.add(getTxFileName(), cc.xy(3, 1));

        main.add(new JLabel(t.get(TConstants.VIEW_FILE_VERSION.LABEL_FILE_VERSIONS)), cc.xy(1, 3));
        main.add(new JScrollPane(getList()), cc.xy(3, 3));

        final JPanel butPanel = new JPanel(new BorderLayout());
        main.add(butPanel, cc.xyw(1, 5, 3));
        butPanel.add(getButSave(), BorderLayout.EAST);
    }

    protected JButton getButSave() {
        if (butSave == null) {
            butSave = Fa.SAVE.makeButton(t.get(TConstants.VIEW_FILE_VERSION.BUT_SAVE));
        }
        return butSave;
    }

    protected JList<SelectableMcVersion> getList() {
        if (list == null) {
            list = new JList<>(getModel());
            list.setCellRenderer(new SelectableMcVersionListRenderer());
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent evt) {
                    final int index = getList().locationToIndex(evt.getPoint());
                    if (index > -1) {
                        final SelectableMcVersion entry = getModel().getElementAt(index);
                        getModel().toggleSelect(entry);
                    }
                }
            });
        }
        return list;
    }

    protected EditVersionListModel getModel() {
        if (model == null) {
            model = new EditVersionListModel();
            model.setData(conf.getBean(DescriptionDao.class).loadMcVersions());
        }
        return model;
    }

    @Override
    public List<JButton> getToolbarButtons() {
        // don't show any buttons
        return null;
    }

    protected JTextField getTxFileName() {
        if (txFileName == null) {
            txFileName = new JTextField();
            txFileName.setEnabled(true);
            txFileName.setEditable(false);
        }
        return txFileName;
    }

}
