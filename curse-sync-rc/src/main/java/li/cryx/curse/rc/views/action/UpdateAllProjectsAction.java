package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.mng.ScanJsonMod;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.views.ProjectList;
import li.cryx.curse.rc.views.dialog.ScanForUpdateProjectDialog;
import li.cryx.curse.rc.worker.ScanForUpdateProjectsWorker;

public class UpdateAllProjectsAction implements ActionListener {

    private final AppConfig conf;

    private final ProjectList parent;

    public UpdateAllProjectsAction(final ProjectList parent, final AppConfig conf) {
        this.parent = parent;
        this.conf = conf;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        final ScanForUpdateProjectDialog dialog = new ScanForUpdateProjectDialog();
        final ScanForUpdateProjectsWorker worker = new ScanForUpdateProjectsWorker(dialog);
        worker.setProjectDao(conf.getBean(ProjectDao.class));
        worker.setScanJsonMod(conf.getBean(ScanJsonMod.class));
        worker.execute();
        final int result = dialog.showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isCancelCode(result)) {
            worker.cancel();
        }
    }

}
