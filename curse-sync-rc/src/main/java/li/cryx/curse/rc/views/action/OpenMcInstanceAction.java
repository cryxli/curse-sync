package li.cryx.curse.rc.views.action;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.views.MultiMcView;

public class OpenMcInstanceAction implements ActionListener {

    private static final Logger LOG = LoggerFactory.getLogger(OpenMcInstanceAction.class);

    private final MultiMcView parent;

    public OpenMcInstanceAction(final MultiMcView parent) {
        this.parent = parent;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        File dir = parent.getInstance().getDir();
        try {
            Desktop.getDesktop().open(dir);
        } catch (IOException e) {
            LOG.error("Error opening file browser", e);
        }
    }

}
