package li.cryx.curse.rc.views.action;

import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.component.dialog.ErrorDialog;

/**
 * Abstract <code>ActionListener</code> that provides a facility to open URLs in the system's default browser.
 * Implementing classes can call {@link #openUrl(String, String)} to open the URL.
 *
 * @author cryxli
 */
public abstract class AbstractOpenUrlAction implements ActionListener {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractOpenUrlAction.class);

    protected void openUrl(final String url, final String errorMsgKey) {
        try {
            openUrl(new URL(url), errorMsgKey);
        } catch (MalformedURLException e) {
            LOG.error("Not a valid URL: " + url, e);
            new ErrorDialog(TConstants.ERROR, errorMsgKey).showDialog();
        }
    }

    protected void openUrl(final URI url, final String errorMsgKey) {
        try {
            Desktop.getDesktop().browse(url);
        } catch (IOException e) {
            LOG.error("Error opening URI: " + url, e);
            new ErrorDialog(TConstants.ERROR, errorMsgKey).showDialog();
        }
    }

    protected void openUrl(final URL url, final String errorMsgKey) {
        try {
            openUrl(url.toURI(), errorMsgKey);
        } catch (URISyntaxException e) {
            LOG.error("Not a valid URI: " + url, e);
            new ErrorDialog(TConstants.ERROR, errorMsgKey).showDialog();
        }
    }

}
