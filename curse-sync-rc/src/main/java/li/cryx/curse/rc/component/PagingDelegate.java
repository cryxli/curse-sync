package li.cryx.curse.rc.component;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.model.filter.AbstractDatabaseFilter;

/**
 * This class manages a {@link AbstractDatabaseFilter} and links it to GUI buttons to scroll through pages. It does not
 * actively listen to changes on the filter, but it alters it and notifies listeners to execute the changed filter.
 * Whenever a search was executed the {@link #update()} method should be called to synchronise the buttons again.
 *
 * @author cryxli
 *
 * @param <F>
 *            An implementation of a {@link AbstractDatabaseFilter}.
 */
public class PagingDelegate<F extends AbstractDatabaseFilter> {

    /**
     * Callback that a filter has changed only in aspect of paging.
     *
     * @author cryxli
     *
     * @param <F>
     *            An implementation of a {@link AbstractDatabaseFilter}.
     */
    public interface FilterListener<F> {
        /**
         * Paging offset of the filter has changes.
         *
         * @param filter
         *            Current filter instance.
         */
        void applyFilter(F filter);
    }

    /** An implementation of a {@link AbstractDatabaseFilter}. */
    private final F filter;

    /** A listener that will apply paging changes to the selection. */
    private final FilterListener<F> listener;

    /** Button to go to first page. */
    private JButton butFirst;

    /** Button to go to previous page. */
    private JButton butPrev;

    /** Button to go to next page. */
    private JButton butNext;

    /** Button to go to last page. */
    private JButton butLast;

    public PagingDelegate(final F filter, final FilterListener<F> listener) {
        this.filter = filter;
        this.listener = listener;
    }

    public JPanel createPanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        panel.add(getButFirst());
        panel.add(getButPrev());
        panel.add(getButNext());
        panel.add(getButLast());
        return panel;
    }

    public JButton getButFirst() {
        if (butFirst == null) {
            butFirst = Fa.STEP_BACKWARD.makeButton();
            butFirst.setEnabled(false);
            butFirst.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    getFilter().setOffset(0);
                    listener.applyFilter(getFilter());
                }
            });
        }
        return butFirst;
    }

    public JButton getButLast() {
        if (butLast == null) {
            butLast = Fa.STEP_FORWARD.makeButton();
            butLast.setEnabled(false);
            butLast.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    getFilter().setOffset(
                            getFilter().getRowCount() - getFilter().getRowCount() % getFilter().getPageSize());
                    listener.applyFilter(getFilter());
                }
            });
        }
        return butLast;
    }

    public JButton getButNext() {
        if (butNext == null) {
            butNext = Fa.FORWARD.makeButton();
            butNext.setEnabled(false);
            butNext.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    getFilter().setOffset(getFilter().getOffset() + getFilter().getPageSize());
                    listener.applyFilter(getFilter());
                }
            });
        }
        return butNext;
    }

    public JButton getButPrev() {
        if (butPrev == null) {
            butPrev = Fa.BACKWARD.makeButton();
            butPrev.setEnabled(false);
            butPrev.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    getFilter().setOffset(getFilter().getOffset() - getFilter().getPageSize());
                    listener.applyFilter(getFilter());
                }
            });

        }
        return butPrev;
    }

    public F getFilter() {
        return filter;
    }

    public void update() {
        getButFirst().setEnabled(filter.getOffset() > 0);
        getButPrev().setEnabled(filter.getOffset() >= filter.getPageSize());
        getButNext().setEnabled(filter.getOffset() + filter.getPageSize() < filter.getRowCount());
        getButLast().setEnabled(filter.getRowCount() - filter.getOffset() > filter.getPageSize());
    }

}
