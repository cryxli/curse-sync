package li.cryx.curse.rc.component;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import li.cryx.curse.rc.TConstants;

public abstract class ViewComponent {

    private JPanel main;

    private JPanel mainPanel;

    private JLabel title;

    /**
     * Callback to have the panel created. During this call the {@link #setMainPanel(JPanel)} method has to be called.
     */
    protected abstract void createView();

    /**
     * Callback to return the tool bar buttons needed for this view.
     *
     * @return Return a list of buttons. This can return an empty list or <code>null</code>, if no buttons should be
     *         displayed.
     */
    public abstract List<JButton> getToolbarButtons();

    /** Return the Swing <code>JPanel</code> representing this component. */
    public JPanel getViewPanel() {
        prepare();
        return main;
    }

    /** Notify the panel that it will be hidden in the next step. */
    public final void hide() {
        preHide();
    }

    private void init() {
        main = new JPanel(new BorderLayout());

        title = new JLabel("No title set");
        title.setFont(title.getFont().deriveFont(Font.BOLD));
        title.setBackground(TConstants.PANEL_TITLE_BG);
        title.setOpaque(true);
        title.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        main.add(title, BorderLayout.NORTH);

        if (mainPanel != null) {
            main.add(mainPanel, BorderLayout.CENTER);
        }
    }

    /** Optional callback method that is called after the {@link #createView()} has been executed. */
    protected void postInit() {
    }

    /** Optional callback method that is called before the panel is hidden. */
    protected void preHide() {
    }

    /**
     * Ensure that the main component has been initialised.
     *
     * @see #getViewPanel()
     */
    private void prepare() {
        if (main == null) {
            init();
            createView();
            postInit();
        }
    }

    /** Optional callback method that is called immediately before the panel becomes visible. */
    protected void preShow() {
    }

    /**
     * Set the view's GUI components.
     * <p>
     * This method should only be called from within the {@link #createView()} method.
     * </p>
     *
     * @param mainPanel
     *            The new main panel.
     */
    protected void setMainPanel(final JPanel mainPanel) {
        // replace existing panel
        if (this.mainPanel != null && main != null) {
            main.remove(this.mainPanel);
        }
        // add new panel
        this.mainPanel = mainPanel;
        if (main != null) {
            main.add(mainPanel, BorderLayout.CENTER);
        }
    }

    /**
     * Set the view's title.
     *
     * @param title
     *            Translated view title.
     */
    protected void setTitle(final String title) {
        prepare();
        this.title.setText(title);
    }

    /** Prepare the panel to be shown in the next step. */
    public final void show() {
        prepare();
        preShow();
    }

}
