package li.cryx.curse.rc.views;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import li.cryx.curse.db.actor.FileAndVersionWriteActor;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.model.McVersion;
import li.cryx.curse.rc.model.db.FileAndVersion;
import li.cryx.curse.rc.model.db.FileVersion;
import li.cryx.curse.rc.views.ide.FileVersionViewIde;

public class FileVersionView extends FileVersionViewIde {

    private FileAndVersion fileAndVersion;

    private final FileAndVersionWriteActor actor;

    public FileVersionView(final AppConfig conf) {
        super(conf);
        actor = conf.getBean(FileAndVersionWriteActor.class);
    }

    public void onSave(final List<McVersion> versions) {
        final List<FileVersion> list = new LinkedList<>();
        for (McVersion v : versions) {
            final FileVersion f = new FileVersion();
            f.setFileId(fileAndVersion.getId());
            f.setVersion(v.getValue());
            list.add(f);
        }

        final FileAndVersion fv = new FileAndVersion(fileAndVersion);
        fv.setVersions(list);
        fv.setName(getTxFileName().getText());

        actor.saveChanges(fv);
    }

    @Override
    protected void postInit() {
        getButSave().addActionListener((evt) -> {
            getButSave().setEnabled(false);
            SwingUtilities.invokeLater(() -> {
                try {
                    onSave(getModel().getSelectedEntries());
                } finally {
                    getButSave().setEnabled(true);
                }
            });
        });
    }

    @Override
    protected void preShow() {
        setTitle(fileAndVersion.getProjectName());
        getTxFileName().setText(fileAndVersion.getName());

        getModel().reset();
        getModel().select(fileAndVersion.getVersions());

        // TODO apply data
    }

    public void setFileVersion(final FileAndVersion fileAndVersion) {
        this.fileAndVersion = fileAndVersion;
    }

}
