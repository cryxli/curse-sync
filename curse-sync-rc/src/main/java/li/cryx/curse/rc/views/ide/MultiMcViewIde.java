package li.cryx.curse.rc.views.ide;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableColumn;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.MultiMcInstance;
import li.cryx.curse.rc.views.model.ModStateTableModel;
import li.cryx.curse.rc.views.model.ModStateTableModel.Col;
import li.cryx.curse.rc.views.renderer.ModStateEnumTableCellRenderer;
import li.cryx.curse.rc.views.renderer.StateChoiceRenderer;
import net.coderazzi.filters.gui.TableFilterHeader;

public abstract class MultiMcViewIde extends ViewComponent {

    private final Translation t = Translation.getInstance();

    protected AppConfig conf;

    private JPanel main;

    private JTextArea lbWarning;

    private JButton butSelectInstance;

    private JButton butUpdateAll;

    private JButton butOpenDirectory;

    protected MultiMcInstance instance;

    private final ModStateTableModel model = new ModStateTableModel();

    private JTable table;

    private JPopupMenu popup;

    private MenuItem<String> butOpenProjectPage;

    private MenuItem<String> butOpenFilePage;

    private JMenuItem txNewVersionInfo;

    private MenuItem<String> butOpenDownloadPage;

    private MenuItem<ModState> butUpdateMod;

    private JButton butAnalyseDependencies;

    protected MultiMcViewIde(final AppConfig conf) {
        this.conf = conf;
    }

    @Override
    protected void createView() {
        setTitle(t.get(TConstants.VIEW_MULTIMC.CAPTION));

        if (main == null) {
            main = new JPanel(new BorderLayout());
            main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            setMainPanel(main);
        } else if (lbWarning != null) {
            main.remove(getLbWarning());
        }

        if (conf.getMultimcDir() == null) {
            main.add(getLbWarning(), BorderLayout.CENTER);

        } else {
            main.add(new JScrollPane(getTable()), BorderLayout.CENTER);
            lbWarning = null;
            // TODO Auto-generated method stub
        }
    }

    public JButton getButAnalyseDependencies() {
        if (butAnalyseDependencies == null) {
            butAnalyseDependencies = Fa.COGS.makeToolbarButton();
            butAnalyseDependencies.setToolTipText(t.get(TConstants.VIEW_MULTIMC.BUT_DEPENDENCIES));
        }
        return butAnalyseDependencies;
    }

    protected JButton getButOpenDirectory() {
        if (butOpenDirectory == null) {
            butOpenDirectory = Fa.MAP_MARKER.makeToolbarButton();
            butOpenDirectory.setToolTipText(t.get(TConstants.VIEW_MULTIMC.BUT_INSTANCE_DIR));
        }
        return butOpenDirectory;
    }

    protected MenuItem<String> getButOpenDownloadPage() {
        if (butOpenDownloadPage == null) {
            butOpenDownloadPage = new MenuItem<>(Fa.LINK, t.get(TConstants.VIEW_MULTIMC.BUT_OPEN_DOWNLOAD_PAGE));
        }
        return butOpenDownloadPage;
    }

    protected MenuItem<String> getButOpenFilePage() {
        if (butOpenFilePage == null) {
            butOpenFilePage = new MenuItem<>(Fa.LINK, t.get(TConstants.VIEW_MULTIMC.BUT_OPEN_FILE_PAGE));
        }
        return butOpenFilePage;
    }

    protected MenuItem<String> getButOpenProjectPage() {
        if (butOpenProjectPage == null) {
            butOpenProjectPage = new MenuItem<>(Fa.LINK, t.get(TConstants.VIEW_MULTIMC.BUT_OPEN_PROJECT_PAGE));
        }
        return butOpenProjectPage;
    }

    protected JButton getButSelectInstance() {
        if (butSelectInstance == null) {
            butSelectInstance = Fa.FOLDER_OPEN.makeToolbarButton();
            butSelectInstance.setToolTipText(t.get(TConstants.VIEW_MULTIMC.BUT_INSTANCE));
            butSelectInstance.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    onSelectInstance();
                }
            });
        }
        return butSelectInstance;
    }

    protected JButton getButUpdateAll() {
        if (butUpdateAll == null) {
            butUpdateAll = Fa.DOWNLOAD.makeToolbarButton();
            butUpdateAll.setToolTipText(t.get(TConstants.VIEW_MULTIMC.BUT_UPDATE_ALL));
        }
        return butUpdateAll;
    }

    protected MenuItem<ModState> getButUpdateMod() {
        if (butUpdateMod == null) {
            butUpdateMod = new MenuItem<>(Fa.DOWNLOAD, t.get(TConstants.VIEW_MULTIMC.BUT_UPDATE_MOD));
        }
        return butUpdateMod;
    }

    private JTextArea getLbWarning() {
        if (lbWarning == null) {
            lbWarning = new JTextArea(t.get(TConstants.VIEW_MULTIMC.WARNING));
            lbWarning.setEditable(false);
            lbWarning.setLineWrap(true);
            lbWarning.setBorder(BorderFactory.createEmptyBorder());
            lbWarning.setOpaque(false);
            lbWarning.setBackground(new Color(0, 0, 0, 0));
            lbWarning.setLineWrap(true);
            lbWarning.setFont(lbWarning.getFont().deriveFont(Font.ITALIC));
        }
        return lbWarning;
    }

    protected ModStateTableModel getModel() {
        return model;
    }

    protected JPopupMenu getPopup() {
        if (popup == null) {
            popup = new JPopupMenu();
            popup.add(getButOpenProjectPage());
            popup.add(getButOpenFilePage());
            popup.addSeparator();
            popup.add(getTxNewVersionInfo());
            popup.add(getButOpenDownloadPage());
            popup.add(getButUpdateMod());
        }
        return popup;
    }

    protected JTable getTable() {
        if (table == null) {
            table = new JTable(getModel());
            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(final MouseEvent evt) {
                    if (evt.isPopupTrigger()) {
                        int rowAtPoint = getTable().rowAtPoint(evt.getPoint());
                        final ModState state = model.getRow(rowAtPoint);
                        if (preShowPopupMenu(state)) {
                            getPopup().show(getTable(), evt.getX(), evt.getY());
                        }
                    }
                }
            });
            TableFilterHeader header = new TableFilterHeader(table);

            TableColumn col = ModStateTableModel.getColumn(table, Col.STATE);
            col.setCellRenderer(new ModStateEnumTableCellRenderer());
            col.setMaxWidth(35);
            header.getFilterEditor(ModStateTableModel.getColumnIndex(Col.STATE)).setRenderer(new StateChoiceRenderer());

            col = ModStateTableModel.getColumn(table, Col.RELEASE);
            col.setMaxWidth(150);
        }
        return table;
    }

    @Override
    public List<JButton> getToolbarButtons() {
        if (conf.getMultimcDir() != null) {
            return Arrays.asList( //
                    getButSelectInstance(), //
                    getButUpdateAll(), //
                    getButOpenDirectory() //
            // null, //
            // getButAnalyseDependencies() //
            );
        } else {
            return null;
        }
    }

    protected JMenuItem getTxNewVersionInfo() {
        if (txNewVersionInfo == null) {
            txNewVersionInfo = new JMenuItem();
            txNewVersionInfo.setEnabled(false);
        }
        return txNewVersionInfo;
    }

    protected abstract void onSelectInstance();

    protected abstract boolean preShowPopupMenu(ModState state);

    protected void showInstance(final MultiMcInstance instance) {
        if (instance != null) {
            this.instance = instance;
            conf.setMultimcInstance(instance.getDir());
            setTitle(new StringBuffer() //
                    .append(t.get(TConstants.VIEW_MULTIMC.CAPTION)) //
                    .append(" - ") //
                    .append(instance.getName()) //
                    .append(" (") //
                    .append(instance.getVersion()) //
                    .append(")") //
                    .toString());
        }
    }

}
