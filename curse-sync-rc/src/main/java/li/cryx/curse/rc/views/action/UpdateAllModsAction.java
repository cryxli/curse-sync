package li.cryx.curse.rc.views.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.stream.Collectors;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.ModStateEnum;
import li.cryx.curse.rc.views.MultiMcView;
import li.cryx.curse.rc.views.dialog.UpdateAllModsDialog;
import li.cryx.curse.rc.worker.UpdateAllModsWorker;

public class UpdateAllModsAction implements ActionListener {

    private final AppConfig conf;

    private final MultiMcView parent;

    public UpdateAllModsAction(final AppConfig conf, final MultiMcView parent) {
        this.conf = conf;
        this.parent = parent;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        // filter list of all mods of the instance, only return the once that need updating
        final List<ModState> toUpdate = parent.getModsForInstance().stream()
                .filter(state -> state.getState() == ModStateEnum.OUTDATED).collect(Collectors.toList());
        if (toUpdate.isEmpty()) {
            // nothing to update
            final Component src = (Component) evt.getSource();
            src.setEnabled(false);
            return;
        }

        final UpdateAllModsDialog dialog = new UpdateAllModsDialog();

        final UpdateAllModsWorker worker = new UpdateAllModsWorker(conf, dialog, toUpdate);
        worker.execute();

        final int result = dialog.showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isCancelCode(result)) {
            worker.cancel();
        }

        parent.refreshModList();
    }

}
