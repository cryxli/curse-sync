package li.cryx.curse.rc.component.validate;

import java.util.LinkedList;
import java.util.List;

/**
 * Data bean containing the result of a {@link ValidationComponent}'s validation process.
 *
 * @author cryxli
 */
public class ValidationInfo {

    /** Indicator that the component's content is valid. */
    private boolean valid = true;

    /** Error messages collected with this bean. */
    private final List<String> textKeys = new LinkedList<>();

    public void add(final ValidationInfo vi) {
        textKeys.addAll(vi.textKeys);
        valid = valid && vi.valid;
    }

    /**
     * Add an error message.
     *
     * @param textKey
     *            Translation key of the error message.
     */
    public void addError(final String textKey) {
        valid = false;
        textKeys.add(textKey);
    }

    public List<String> getErrors() {
        return textKeys;
    }

    /**
     * Indicator that this validation was successful.
     *
     * @return <code>true</code>: Validation succeeded. <code>false</code>: Validation failed.
     */
    public boolean isValid() {
        return valid;
    }

}
