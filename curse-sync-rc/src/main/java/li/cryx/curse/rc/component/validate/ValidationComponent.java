package li.cryx.curse.rc.component.validate;

/**
 * Swing component implementing this interface have business logic attached to validate their contents.
 *
 * @author cryxli
 */
public interface ValidationComponent {

    /**
     * Indicator whether the content of the component is valid.
     *
     * @return <code>true</code>: Content is valid. <code>false</code>: Content is invalid.
     */
    boolean isValidComponent();

    /**
     * Implementing components should put their validation logic into this method.
     *
     * @return Created {@link ValidationInfo} indicating the state if the component's content.
     */
    ValidationInfo validateComponent();

    /**
     * Have the component validate its content and adjust its visual representation accordingly.
     *
     * <p>
     * You should call this method instead of {@link #validateComponent()} directly.
     * </p>
     */
    void validation();

}
