package li.cryx.curse.rc.views.renderer;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CustomeDateRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -8201478055196991283L;

    public static final String FORMAT = "yyyy-MM-dd";

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);
        final Date date = toDate(value);
        if (date != null) {
            label.setText(new SimpleDateFormat(FORMAT).format(date));
        } else {
            label.setText("?");
        }
        return label;
    }

    private Date toDate(final Object value) {
        if (value == null) {
            return null;
        } else if (value instanceof Date) {
            return (Date) value;
        } else if (value instanceof Long) {
            // expecting epoch
            return new Date(1000 * (Long) value);
        } else {
            return null;
        }
    }

}
