package li.cryx.curse.rc.views.dialog;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.worker.UpdateSingleModWorker;

/**
 * A dialog to show progress while {@link UpdateSingleModWorker} is downloading an copying the latest version of a mod.
 *
 * @author cryxli
 */
public class UpdateSingleModDialog extends ButtonBasedDialog {

    private final Translation t = Translation.getInstance();

    private final JLabel lbMsg = new JLabel(" ");

    public UpdateSingleModDialog() {
        super(ButtonBasedDialog.DIALOG_TYPE_INFO, TConstants.VIEW_MULTIMC.UPDATE_MOD.CAPTION);
        removeAllButtons();
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
        setSize(400, 170);
    }

    @Override
    protected JPanel getMainPanel() {
        final JPanel main = new JPanel(new BorderLayout());

        main.add(new JLabel(t.get(TConstants.VIEW_MULTIMC.UPDATE_MOD.INFO)), BorderLayout.NORTH);

        final JPanel panel = new JPanel(new BorderLayout());
        main.add(panel, BorderLayout.SOUTH);
        panel.add(lbMsg, BorderLayout.NORTH);
        final JProgressBar progress = new JProgressBar();
        panel.add(progress, BorderLayout.SOUTH);
        progress.setIndeterminate(true);

        return main;
    }

    /** Hide the dialog terminating is as if the user hit the OK button. */
    public void hideDialog() {
        hideDialog(ACTION_CODE_OK);
    }

    /**
     * Update the progress with a new step description message.
     *
     * @param msgKey
     *            Translation key for the step.
     */
    public void updateProgressMsg(final String msgKey) {
        lbMsg.setText(t.get(msgKey));
    }

}
