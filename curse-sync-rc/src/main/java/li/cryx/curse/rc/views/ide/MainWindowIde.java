package li.cryx.curse.rc.views.ide;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.views.MultiMcView;
import li.cryx.curse.rc.views.ProjectList;

public class MainWindowIde {

    private final Translation t = Translation.getInstance();

    /** The main application window. */
    private JFrame frame;

    /** Top toolbar */
    private JToolBar toolbar;

    /** Toolbar button: Switch to {@link Settings} view. */
    private JButton butSettings;

    /** Toolbar button: Switch to {@link ProjectList} view. */
    private JButton butProjectList;

    /** Toolbar button: Switch to {@link MultiMcView}. */
    private JButton butMultiMc;

    /** Current {@link ViewComponent} displayed. */
    private ViewComponent latestView;

    /** Toolbar button: Switch to {@link MultiMcView}. */
    protected JButton getButMultiMc() {
        if (butMultiMc == null) {
            butMultiMc = Fa.BOOKMARK.makeToolbarButton();
            butMultiMc.setToolTipText(t.get(TConstants.MAIN_WINDOW.BUT_MULTIMC));
        }
        return butMultiMc;
    }

    /** Toolbar button: Switch to {@link ProjectList} view. */
    protected JButton getButProjectList() {
        if (butProjectList == null) {
            butProjectList = Fa.DATABASE.makeToolbarButton();
            butProjectList.setToolTipText(t.get(TConstants.MAIN_WINDOW.BUT_PROJECT_LIST));
        }
        return butProjectList;
    }

    /** Toolbar button: Switch to {@link Settings} view. */
    protected JButton getButSettings() {
        if (butSettings == null) {
            butSettings = Fa.COG.makeToolbarButton();
            butSettings.setToolTipText(t.get(TConstants.MAIN_WINDOW.BUT_SETTINGS));
        }
        return butSettings;
    }

    protected JFrame getFrame() {
        return frame;
    }

    private void init() {
        frame = new JFrame(t.get(TConstants.MAIN_WINDOW.TITLE));
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        toolbar = new JToolBar();
        toolbar.setFloatable(false);
        frame.add(toolbar, BorderLayout.NORTH);
        toolbar.add(getButSettings());
        toolbar.add(getButProjectList());
        toolbar.add(getButMultiMc());
    }

    protected void postInit() {
    }

    public void show() {
        if (frame == null) {
            init();
            postInit();
            frame.pack();
            final Dimension dim = frame.getSize();
            frame.setSize(Math.max(640, dim.width), Math.max(480, dim.height));
        }
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public <V extends ViewComponent> void show(final V view) {
        if (latestView != null) {
            if (latestView.getClass().equals(view.getClass())) {
                // same view is already visible
                return;
            }

            // hide current view
            latestView.hide();
            frame.remove(latestView.getViewPanel());
            // remove view's buttons from toolbar
            final List<JButton> toolButs = latestView.getToolbarButtons();
            if (toolButs != null && !toolButs.isEmpty()) {
                for (JButton but : toolButs) {
                    toolbar.remove(but);
                }
                if (toolbar.getComponent(toolbar.getComponentCount() - 1) instanceof JToolBar.Separator) {
                    toolbar.remove(toolbar.getComponentCount() - 1);
                }
            }
        }

        // add new view's buttons to toolbar
        latestView = view;
        final List<JButton> toolButs = latestView.getToolbarButtons();
        if (toolButs != null && !toolButs.isEmpty()) {
            toolbar.addSeparator();
            for (JButton but : toolButs) {
                if (but != null) {
                    toolbar.add(but);
                } else {
                    toolbar.addSeparator();
                }
            }
        }
        // show new view
        frame.add(latestView.getViewPanel(), BorderLayout.CENTER);
        latestView.show();

        // repaint GUI
        frame.revalidate();
        frame.repaint();
    }

}
