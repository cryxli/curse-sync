package li.cryx.curse.rc.views;

import java.util.List;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.db.actor.DeleteProjectActor;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.views.action.AddProjectAction;
import li.cryx.curse.rc.views.action.DeleteProjectAction;
import li.cryx.curse.rc.views.action.OpenPageAction;
import li.cryx.curse.rc.views.action.ScanForUpdatesAction;
import li.cryx.curse.rc.views.action.UpdateAllProjectsAction;
import li.cryx.curse.rc.views.ide.ProjectListIde;
import li.cryx.curse.rc.views.model.ProjectListTableModel.Col;

public class ProjectList extends ProjectListIde {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectList.class);

    private final AppConfig conf;

    private final ProjectDao projectDao;

    public ProjectList(final AppConfig conf) {
        this.conf = conf;
        projectDao = conf.getBean(ProjectDao.class);
    }

    public void onDeleteProject(final long projectId) {
        conf.getBean(DeleteProjectActor.class).delete(projectId);
        // reload project list
        preShow();
    }

    @Override
    protected void onShowProjectDetail(final Project project) {
        final ProjectDetail detail = new ProjectDetail(conf);
        detail.setProject(project);
        MainWindow.getInstance().show(detail);
    }

    @Override
    protected void postInit() {
        getButProjectUrl().addActionListener(new OpenPageAction(TConstants.VIEW_PROJECT_LIST.ERROR_PROJECT_URL));
        getButScanForUpdates().addActionListener(new ScanForUpdatesAction(this, conf));
        getButDeleteProject().addActionListener(new DeleteProjectAction(this));
        getButAdd().addActionListener(new AddProjectAction(this, conf));
        getButUpdate().addActionListener(new UpdateAllProjectsAction(this, conf));

        getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(final TableModelEvent evt) {
                if (evt.getType() == TableModelEvent.UPDATE && evt.getColumn() == Col.UPDATE.ordinal()) {
                    final Project prj = getModel().getRow(evt.getFirstRow());
                    if (prj != null) {
                        conf.getBean(ProjectDao.class).store(prj);
                        LOG.info("Project flag updated: {}", prj.getTitle());
                    }
                }
            }
        });
    }

    @Override
    public void preShow() {
        final List<Project> list = projectDao.search(getFilter());
        getModel().setData(list);
        updatePaging();
    }

    @Override
    protected void preShowPopupMenu(final Project project) {
        getButProjectUrl().setPayload(project.getUrl());
        getButScanForUpdates().setPayload(project);
        getButDeleteProject().setPayload(project);
    }

}
