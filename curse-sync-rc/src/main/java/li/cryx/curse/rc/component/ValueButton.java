package li.cryx.curse.rc.component;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * This <code>JButton</code> implementation is not just a GUI component, but a container for an arbitrary payload.
 *
 * @author cryxli
 *
 * @param <V>
 *            Type of the payload.
 */
public class ValueButton<V> extends JButton {

    private static final long serialVersionUID = 78290428421666250L;

    private V value;

    public ValueButton() {
        super();
    }

    public ValueButton(final Action a, final V value) {
        super(a);
        this.value = value;
    }

    public ValueButton(final Icon icon, final V value) {
        super(icon);
        this.value = value;
    }

    public ValueButton(final String text, final Icon icon, final V value) {
        super(text, icon);
        this.value = value;
    }

    public ValueButton(final String text, final V value) {
        super(text);
        this.value = value;
    }

    public ValueButton(final V value) {
        super();
        this.value = value;
    }

    public V getValue() {
        return value;
    }

    public void setValue(final V value) {
        this.value = value;
    }

}
