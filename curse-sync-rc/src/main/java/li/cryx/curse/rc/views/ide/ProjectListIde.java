package li.cryx.curse.rc.views.ide;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.TableRowSorter;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.TableAspect;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.AbstractRowSorterListener;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.PagingDelegate;
import li.cryx.curse.rc.component.PagingDelegate.FilterListener;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.model.filter.ProjectSearchFilter;
import li.cryx.curse.rc.views.model.ProjectListTableModel;
import li.cryx.curse.rc.views.model.ProjectListTableModel.Col;

public abstract class ProjectListIde extends ViewComponent {

    private final Translation t = Translation.getInstance();

    private JButton butAdd;

    private JButton butUpdate;

    private MenuItem<String> butProjectUrl;

    private MenuItem<Project> butScanForUpdates;

    private MenuItem<Project> butDeleteProject;

    private final ProjectListTableModel model = new ProjectListTableModel();

    private JTable table;

    private JPopupMenu menu;

    private final PagingDelegate<ProjectSearchFilter> paging;

    protected ProjectListIde() {
        paging = new PagingDelegate<>( //
                new ProjectSearchFilter(), //
                new FilterListener<ProjectSearchFilter>() {
                    @Override
                    public void applyFilter(final ProjectSearchFilter filter) {
                        preShow();
                    }
                } //
        );
    }

    @Override
    protected void createView() {
        setTitle(t.get(TConstants.VIEW_PROJECT_LIST.CAPTION));

        final JPanel main = new JPanel(new BorderLayout());
        setMainPanel(main);
        main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        main.add(new JScrollPane(getTable()), BorderLayout.CENTER);

        main.add(paging.createPanel(), BorderLayout.SOUTH);
    }

    protected JButton getButAdd() {
        if (butAdd == null) {
            butAdd = Fa.PLUS.makeToolbarButton();
            butAdd.setToolTipText(t.get(TConstants.VIEW_PROJECT_LIST.BUT_ADD));
        }
        return butAdd;
    }

    protected MenuItem<Project> getButDeleteProject() {
        if (butDeleteProject == null) {
            butDeleteProject = new MenuItem<>(Fa.TRASH, t.get(TConstants.VIEW_PROJECT_LIST.BUT_DELETE_PROJECT));
        }
        return butDeleteProject;
    }

    public MenuItem<String> getButProjectUrl() {
        if (butProjectUrl == null) {
            butProjectUrl = new MenuItem<>(Fa.LINK, t.get(TConstants.VIEW_PROJECT_LIST.BUT_PROJECT_URL));
        }
        return butProjectUrl;
    }

    protected MenuItem<Project> getButScanForUpdates() {
        if (butScanForUpdates == null) {
            butScanForUpdates = new MenuItem<>(Fa.SYNC, t.get(TConstants.VIEW_PROJECT_LIST.BUT_SCAN_UPDATE));
        }
        return butScanForUpdates;
    }

    protected JButton getButUpdate() {
        if (butUpdate == null) {
            butUpdate = Fa.SYNC.makeToolbarButton();
            butUpdate.setToolTipText(t.get(TConstants.VIEW_PROJECT_LIST.BUT_UPDATE));
        }
        return butUpdate;
    }

    protected ProjectSearchFilter getFilter() {
        return paging.getFilter();
    }

    protected ProjectListTableModel getModel() {
        return model;
    }

    private JPopupMenu getPopup() {
        if (menu == null) {
            menu = new JPopupMenu();

            menu.add(getButProjectUrl());
            menu.add(getButScanForUpdates());

            // TODO

            menu.addSeparator();
            menu.add(getButDeleteProject());
        }
        return menu;

    }

    protected JTable getTable() {
        if (table == null) {
            table = new JTable(getModel());
            TableAspect.setSingleSelection(table);
            TableAspect.setWidth(ProjectListTableModel.getColumn(table, Col.UPDATE), 25);

            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent evt) {
                    if (evt.getClickCount() > 1) {
                        final int rowIndex = getTable().getSelectedRow();
                        final Project project = getModel().getRow(rowIndex);
                        onShowProjectDetail(project);
                    }
                }

                @Override
                public void mouseReleased(final MouseEvent evt) {
                    if (evt.isPopupTrigger()) {
                        int rowAtPoint = getTable().rowAtPoint(evt.getPoint());
                        final Project project = model.getRow(rowAtPoint);
                        preShowPopupMenu(project);
                        getPopup().show(getTable(), evt.getX(), evt.getY());
                    }
                }
            });

            installTableSorter();
        }
        return table;
    }

    @Override
    public List<JButton> getToolbarButtons() {
        return Arrays.asList(getButAdd(), getButUpdate());
    }

    private void installTableSorter() {
        final TableRowSorter<ProjectListTableModel> trs = new TableRowSorter<>(getModel());
        trs.setSortKeys(Arrays.asList(Col.TITLE.sortAsc()));
        trs.setSortable(Col.UPDATE.ordinal(), false);
        table.setRowSorter(trs);

        trs.addRowSorterListener(new AbstractRowSorterListener() {
            @Override
            protected void updatedSortKeys(final List<SortKey> sortKeys) {
                for (SortKey sk : sortKeys) {
                    // only handle title sort order
                    if (sk.getColumn() == Col.TITLE.ordinal()) {
                        final boolean oldAsc = getFilter().isOrderAsc();
                        final boolean newAsc = sk.getSortOrder() == SortOrder.ASCENDING;
                        // only reset filter when sort order changed.
                        if (oldAsc != newAsc) {
                            getFilter().setOffset(0);
                            getFilter().setOrderAsc(newAsc);
                            // trigger new database search
                            preShow();
                        }
                    }
                }
            }
        });
    }

    protected abstract void onShowProjectDetail(Project project);

    protected abstract void preShowPopupMenu(Project project);

    protected void updatePaging() {
        paging.update();
    }

}
