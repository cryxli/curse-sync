package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.springframework.web.client.RestTemplate;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.views.MultiMcView;
import li.cryx.curse.rc.views.dialog.UpdateSingleModDialog;
import li.cryx.curse.rc.worker.UpdateSingleModWorker;

public class UpdateSingleModAction implements ActionListener {

    private final AppConfig conf;

    private final MultiMcView parent;

    public UpdateSingleModAction(final AppConfig conf, final MultiMcView parent) {
        this.conf = conf;
        this.parent = parent;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        @SuppressWarnings("unchecked")
        MenuItem<ModState> item = (MenuItem<ModState>) evt.getSource();

        final UpdateSingleModDialog dialog = new UpdateSingleModDialog();

        // start background task
        final UpdateSingleModWorker worker = new UpdateSingleModWorker(conf, dialog, item.getPayload());
        worker.setRestTemplate(conf.getBean(RestTemplate.class));
        worker.execute();

        // show progress dialog
        final int result = dialog.showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isCancelCode(result)) {
            worker.cancel();
        } else {
            parent.refreshModList();
        }
    }

}
