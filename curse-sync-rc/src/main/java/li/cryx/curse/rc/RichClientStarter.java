package li.cryx.curse.rc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import li.cryx.curse.config.DatabaseConfig;
import li.cryx.curse.rc.common.SwingAspect;
import li.cryx.curse.rc.views.MainWindow;

@SpringBootApplication
@Import(DatabaseConfig.class)
public class RichClientStarter {

    public static void main(final String[] args) {
        final ApplicationContext ctx = new SpringApplicationBuilder(RichClientStarter.class).headless(false).run(args);
        ctx.getBean(RichClientStarter.class).start();
    }

    private MainWindow window;

    @Bean
    public MainWindow getMainWindow(final AppConfig config) {
        window = new MainWindow(config);
        return window;
    }

    public void start() {
        SwingAspect.installAcrylStyle();
        SwingAspect.installFa();

        // TODO

        window.show();
    }

}
