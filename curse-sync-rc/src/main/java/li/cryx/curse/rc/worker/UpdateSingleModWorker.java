package li.cryx.curse.rc.worker;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.swing.SwingWorker;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.MD5;
import li.cryx.curse.rc.model.ModState;
import li.cryx.curse.rc.model.ModStateDecorator;
import li.cryx.curse.rc.views.dialog.UpdateSingleModDialog;

/**
 * Swing worker to download latest version of a known mod and replace it in the current MultiMC instance.
 *
 * @author cryxli
 */
public class UpdateSingleModWorker extends SwingWorker<Void, String> {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateSingleModWorker.class);

    /** Config for download directory */
    private final AppConfig conf;

    /** Progress bar to show current step */
    private final UpdateSingleModDialog dialog;

    /** Mod version current and latest */
    private final ModStateDecorator state;

    /** Indicator that the worker should progress. */
    private boolean running = false;

    private RestTemplate restTempalte;

    public UpdateSingleModWorker(final AppConfig conf, final UpdateSingleModDialog dialog, final ModState state) {
        this.conf = conf;
        this.state = new ModStateDecorator(state);
        this.dialog = dialog;
    }

    /** Have the worker abort before starting the next step. */
    public void cancel() {
        running = false;
    }

    @Override
    protected Void doInBackground() throws Exception {
        running = true;

        // check download dir for a matching file
        if (!running) {
            return null;
        }
        publish(TConstants.VIEW_MULTIMC.UPDATE_MOD.STEP_CHECK_EXISTING);
        final File latestJar = new File(conf.getModDir(), state.getLatestFile().getName());
        // if the file already exists, no need to download
        boolean download = !latestJar.isFile();
        // ... but only, if the MD5 hashes match
        download = download || !state.getLatestFile().getHash().equals(MD5.hash(latestJar));

        // download mod to download dir
        if (!running) {
            return null;
        }
        if (download) {
            LOG.info("Downloading file {}", latestJar.getName());
            publish(TConstants.VIEW_MULTIMC.UPDATE_MOD.STEP_DOWNLOAD_JAR);
            download(latestJar);
        } else {
            LOG.info("File {} found locally.", latestJar.getName());
        }

        // copy old mod to backup dir (on instance)
        if (!running) {
            return null;
        }
        publish(TConstants.VIEW_MULTIMC.UPDATE_MOD.STEP_BACKUP_MOD);
        final File modFile = state.getJar();
        final File backupDir = new File(modFile.getParentFile(), "disabled");
        final File backupFile = new File(backupDir, modFile.getName());
        if (backupFile.isFile()) {
            LOG.debug("Deleting old backup {}", backupFile.getName());
            backupFile.delete();
        }
        LOG.debug("Creating backup of {}", backupFile.getName());
        FileUtils.moveToDirectory(modFile, backupDir, true);

        // copy mod to instance
        if (!running) {
            return null;
        }
        LOG.debug("Copying new file {}", latestJar.getName());
        publish(TConstants.VIEW_MULTIMC.UPDATE_MOD.STEP_COPY_MOD);
        FileUtils.copyFile(latestJar, new File(modFile.getParentFile(), latestJar.getName()));

        // done
        return null;
    }

    @Override
    protected void done() {
        running = false;
        if (dialog != null) {
            dialog.hideDialog();
        }
    }

    protected void download(final File latestJar) throws MalformedURLException {
        final byte[] bytes = restTempalte.getForObject(state.getLatestFile().getDownload(), byte[].class);
        try (FileOutputStream fos = new FileOutputStream(latestJar);) {
            latestJar.getParentFile().mkdirs();
            IOUtils.copy(new ByteArrayInputStream(bytes), fos);
        } catch (IOException e) {
            LOG.error("Error downloading mod", e);
            running = false;
            // TODO throw error?
        }
    }

    @Override
    protected void process(final List<String> chunks) {
        if (dialog != null) {
            final String key = chunks.get(chunks.size() - 1);
            // show next step in progress dialog
            dialog.updateProgressMsg(key);
        }
    }

    public void setRestTemplate(final RestTemplate restTempalte) {
        this.restTempalte = restTempalte;
    }

}
