package li.cryx.curse.rc.views.renderer;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import li.cryx.curse.rc.views.model.EditVersionListModel.SelectableMcVersion;

/**
 * A <code>ListCellRenderer</code> to display minecraft versions with checkboxes. It only works with
 * {@link SelectableMcVersion}s.
 *
 * @author cryxli
 */
public class SelectableMcVersionListRenderer extends JCheckBox implements ListCellRenderer<SelectableMcVersion> {

    private static final long serialVersionUID = -382465760018012698L;

    @Override
    public JCheckBox getListCellRendererComponent(final JList<? extends SelectableMcVersion> list,
            final SelectableMcVersion value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        setSelected(value.isSelected());
        setText(value.getValue());
        return this;
    }

}
