package li.cryx.curse.rc.component.dialog;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import li.cryx.curse.rc.common.Translation;

public class MessageDialog extends ButtonBasedDialog {

    private String msg;

    public MessageDialog() {
        super(DIALOG_TYPE_INFO);
    }

    public MessageDialog(final int type) {
        super(type);
    }

    public MessageDialog(final int type, final String titleKey) {
        super(type, titleKey);
    }

    public MessageDialog(final int type, final String titleKey, final String msgKey) {
        super(type, titleKey);
        setMessage(msgKey);
    }

    public MessageDialog(final int type, final String titleKey, final String msgKey, final Object... arguments) {
        super(type, titleKey);
        setMessage(msgKey, arguments);
    }

    public MessageDialog(final String titleKey) {
        super(DIALOG_TYPE_INFO, titleKey);
    }

    public MessageDialog(final String titleKey, final String msgKey) {
        super(DIALOG_TYPE_INFO, titleKey);
        setMessage(msgKey);
    }

    public MessageDialog(final String titleKey, final String msgKey, final Object... arguments) {
        super(DIALOG_TYPE_INFO, titleKey);
        setMessage(msgKey, arguments);
    }

    @Override
    protected JPanel getMainPanel() {
        if (msg != null) {
            final JLabel label = new JLabel("<html>" + msg.replaceAll("\\[nl\\]", "<br>") + "</html>");
            label.setVerticalAlignment(SwingConstants.TOP);
            final JPanel panel = new JPanel(new BorderLayout());
            panel.add(label, BorderLayout.CENTER);
            return panel;
        } else {
            return null;
        }
    }

    public void setMessage(final String msgKey) {
        setTranslatedMessage(Translation.getInstance().get(msgKey));
    }

    public void setMessage(final String msgKey, final Object... arguments) {
        setTranslatedMessage(Translation.getInstance().get(msgKey, arguments));
    }

    public void setTranslatedMessage(final String msg) {
        this.msg = msg;
    }

}
