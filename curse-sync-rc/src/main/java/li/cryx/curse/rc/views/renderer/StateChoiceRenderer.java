package li.cryx.curse.rc.views.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.UIManager;

import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.model.ModStateEnum;
import net.coderazzi.filters.gui.ChoiceRenderer;
import net.coderazzi.filters.gui.IFilterEditor;

public class StateChoiceRenderer extends JLabel implements ChoiceRenderer {

    private static final long serialVersionUID = 5785128157295319767L;

    @Override
    public Component getRendererComponent(final IFilterEditor editor, final Object value, final boolean isSelected) {

        setFont(Fa.TIMES_CIRCLE.getFont());
        if (value instanceof ModStateEnum) {
            ModStateEnum state = (ModStateEnum) value;
            switch (state) {
            case OUTDATED:
            case UNKNOWN:
            case UP_TO_DATE:
                setForeground(state.color());
                setText(state.icon().toString());
                break;
            default:
                setText("");
                break;
            }
        } else {
            setText("");
        }

        if (isSelected) {
            setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            setBackground(UIManager.getColor("Table.background"));
        }

        return this;
    }

}
