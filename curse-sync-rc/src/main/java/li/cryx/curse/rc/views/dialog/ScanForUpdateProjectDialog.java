package li.cryx.curse.rc.views.dialog;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.worker.ScanForUpdateProjectsWorker;

/**
 * This dialog shows the progress while {@link ScanForUpdateProjectsWorker} scans Curse.com for project file updates.
 *
 * @author cryxli
 */
public class ScanForUpdateProjectDialog extends ButtonBasedDialog {

    private final Translation t = Translation.getInstance();

    private JProgressBar progress;

    private JLabel lbStepMsg;

    private JLabel lbStepCounter;

    public ScanForUpdateProjectDialog() {
        super(ButtonBasedDialog.DIALOG_TYPE_INFO, TConstants.VIEW_PROJECT_LIST.BUT_UPDATE);
        removeAllButtons();
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
    }

    @Override
    protected JPanel getMainPanel() {
        // show steps above progress bar
        lbStepMsg = new JLabel();
        lbStepCounter = new JLabel();
        final JPanel stepPanel = new JPanel(new BorderLayout());
        stepPanel.add(lbStepMsg, BorderLayout.CENTER);
        stepPanel.add(lbStepCounter, BorderLayout.EAST);

        // the progress bar
        progress = new JProgressBar();
        final JPanel progressPanel = new JPanel(new BorderLayout());
        progressPanel.add(stepPanel, BorderLayout.NORTH);
        progressPanel.add(progress, BorderLayout.SOUTH);

        // main panel
        final JPanel main = new JPanel(new BorderLayout());
        main.add(progressPanel, BorderLayout.SOUTH);

        // main dialog message
        final JTextArea txt = new JTextArea(t.get(TConstants.VIEW_PROJECT_LIST.UPDATE_DIALOG_TEXT));
        txt.setEditable(false);
        txt.setLineWrap(true);
        txt.setWrapStyleWord(true);
        txt.setBorder(BorderFactory.createEmptyBorder());
        txt.setOpaque(false);
        txt.setBackground(new Color(0, 0, 0, 0));
        main.add(txt, BorderLayout.CENTER);

        setSize(400, 180);
        return main;
    }

    /** Have the dialog closed when worker is done. */
    public void hideDialog() {
        hideDialog(ACTION_CODE_OK);
    }

    /**
     * Update progress message.
     *
     * @param msg
     *            Message on top of the progress bar.
     */
    public void updateMsg(final String msg) {
        lbStepMsg.setText(msg);
    }

    /**
     * Update progress bar and step count.
     *
     * @param current
     *            Current step.
     * @param total
     *            Max expected steps.
     */
    public void updateSteps(final int current, final int total) {
        progress.setMaximum(total);
        progress.setValue(current);
        lbStepCounter.setText(current + "/" + total);
    }

}
