package li.cryx.curse.rc.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import li.cryx.curse.rc.common.font.Icon;

/**
 * This is a <code>JMenuItem</code> that can use {@link Icon}s as icons.
 *
 * @author cryxli
 *
 * @param <E>
 *            Payload. Can be the row of a table, or any other payload the menu trigger decides to place onto the menu
 *            entry before it is shown.
 */
public class MenuItem<E> extends JMenuItem {

    private static final long serialVersionUID = -4160112484364778909L;

    /** Action listener to reset the entry once it is clicked. */
    private static final ActionListener resetListener = new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent evt) {
            final MenuItem<?> item = (MenuItem<?>) evt.getSource();
            item.updateColor(Color.BLACK);
        }
    };

    /** Payload. */
    private E payload;

    /**
     * Not too meaningful, but create an entry only consisting of the given icon.
     */
    public MenuItem(final Icon icon) {
        this(icon, null);
    }

    public MenuItem(final Icon icon, final String text) {
        // inherit properties from super-class
        super(" ");
        removeAll();

        // set glyphicon
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
        if (icon != null) {
            final JLabel iconLabel = new JLabel(icon.toString());
            iconLabel.setFont(icon.getFont());
            iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
            add(iconLabel, BorderLayout.WEST);
        }

        // set text
        final JLabel txt = new JLabel();
        if (text != null) {
            txt.setText(text);
        } else {
            txt.setText(" ");
        }
        add(txt, BorderLayout.CENTER);

        // toggle text color when mouse hovers
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent evt) {
                if (isEnabled()) {
                    updateColor(Color.WHITE);
                } else {
                    updateColor(Color.GRAY);
                }
            }

            @Override
            public void mouseExited(final MouseEvent evt) {
                if (isEnabled()) {
                    updateColor(Color.BLACK);
                } else {
                    updateColor(Color.GRAY);
                }
            }
        });
        addActionListener(resetListener);
    }

    public MenuItem(final String text) {
        this(null, text);
    }

    public E getPayload() {
        return payload;
    }

    @Override
    public Dimension getPreferredSize() {
        final Dimension d = new Dimension(13, 0);
        for (Component c : getComponents()) {
            final Dimension dim = c.getPreferredSize();
            d.width += dim.width;
            d.height = Math.max(d.height, dim.height + 4);
        }
        return d;
    }

    @Override
    public void setEnabled(final boolean b) {
        super.setEnabled(b);
        if (b) {
            updateColor(Color.BLACK);
        } else {
            updateColor(Color.GRAY);
        }
    }

    public void setPayload(final E payload) {
        this.payload = payload;
    }

    private void updateColor(final Color color) {
        for (Component c : getComponents()) {
            c.setForeground(color);
        }
    }

}