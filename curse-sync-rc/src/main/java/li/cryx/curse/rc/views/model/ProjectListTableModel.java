package li.cryx.curse.rc.views.model;

import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.views.ProjectList;

/**
 * Model behind table of {@link ProjectList} view.
 *
 * @author cryxli
 */
public class ProjectListTableModel extends ObjectRowTableModel<Project> {

    /** Name table columns */
    public static enum Col {
        TITLE,

        UPDATE,

        UNKNOWN;

        public static Col of(final int index) {
            if (index < 0 || index > values().length - 1) {
                return UNKNOWN;
            } else {
                return values()[index];
            }
        }

        public SortKey sort(final SortOrder order) {
            return new SortKey(ordinal(), order);
        }

        public SortKey sortAsc() {
            return sort(SortOrder.ASCENDING);
        }

        public SortKey sortDesc() {
            return sort(SortOrder.DESCENDING);
        }

    }

    private static final long serialVersionUID = 5975797574384553971L;

    private static final Logger LOG = LoggerFactory.getLogger(ProjectListTableModel.class);

    public static TableColumn getColumn(final JTable table, final Col column) {
        return table.getColumnModel().getColumn(column.ordinal());
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case TITLE:
        default:
            return String.class;
        case UPDATE:
            return Boolean.class;
        }
    }

    @Override
    public int getColumnCount() {
        return Col.values().length - 1;
    }

    @Override
    public String getColumnName(final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case TITLE:
            return Translation.getInstance().get(TConstants.VIEW_PROJECT_LIST.COL_TITLE);
        case UPDATE:
            return Translation.getInstance().get(TConstants.VIEW_PROJECT_LIST.COL_UPDATE);
        default:
            return super.getColumnName(columnIndex);
        }
    }

    @Override
    protected Object getValueAt(final Project row, final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case TITLE:
            return row.getTitle();
        case UPDATE:
            return row.isUpdated();
        default:
            return "?";
        }
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case UPDATE:
            return true;
        default:
            return false;
        }
    }

    @Override
    public void setValueAt(final Object aValue, final Project row, final int columnIndex) {
        switch (Col.of(columnIndex)) {
        case UPDATE:
            LOG.debug("update={} -> {}", aValue, row);
            // TODO
            row.setUpdated((Boolean) aValue);
            break;
        default:
            // do nothing
            break;
        }
    }

}
