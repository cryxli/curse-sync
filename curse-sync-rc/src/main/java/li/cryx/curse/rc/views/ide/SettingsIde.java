package li.cryx.curse.rc.views.ide;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.FocusTraversalPolicy;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.component.validate.ValidationInfo;
import li.cryx.curse.rc.component.validate.ValidationTextField;

public class SettingsIde extends ViewComponent {

    private final Translation t = Translation.getInstance();

    private final Set<JComponent> proxyAuthComponents = new HashSet<>();

    // -- Download directory

    private JTextField txModDir;

    private JButton butBrowseModDir;

    // -- Proxy settings

    private JCheckBox enableProxy;

    private JTextField txProxyHost;

    private JFormattedTextField txProxyPort;

    private final Set<JComponent> proxyComponents = new HashSet<>();

    private JCheckBox enableProxyAuth;

    private JTextField txProxyUser;

    private JPasswordField txProxyPass;

    // -- MultiMC directory

    private ValidationTextField txMultiMcDir;

    private JButton butBrowseMultiMcDir;

    /** Container to hold tab order. */
    private final FocusTraversalPolicy tabOrder = new FocusTraversalPolicy();

    /** Add components to their parent, and if focusable add them to {@link #tabOrder}, too. */
    private void add(final JComponent parent, final JComponent child, final Object constraints) {
        if (child.isFocusable() && !(child instanceof JLabel)) {
            tabOrder.add(child);
        }
        parent.add(child, constraints);
    }

    private JPanel createModDirPanel() {
        final JPanel panel = new JPanel(new FormLayout("f:p:g,4dlu,p", "p,2dlu,p"));
        final CellConstraints cc = new CellConstraints();
        add(panel, new JLabel(t.get(TConstants.VIEW_SETTINGS.MOD_DIR.LABEL)), cc.xyw(1, 1, 3));
        add(panel, getTxModDir(), cc.xy(1, 3));
        add(panel, getButBrowseModDir(), cc.xy(3, 3));
        return panel;
    }

    private JPanel createMultiMcDirPanel() {
        final JPanel panel = new JPanel(new FormLayout("f:p:g,4dlu,p", "p,2dlu,p"));
        final CellConstraints cc = new CellConstraints();
        add(panel, new JLabel(t.get(TConstants.VIEW_SETTINGS.MULTIMC_DIR.LABEL)), cc.xyw(1, 1, 3));
        add(panel, getTxMultiMcDir(), cc.xy(1, 3));
        add(panel, getButBrowseMultiMcDir(), cc.xy(3, 3));
        return panel;
    }

    private JPanel createProxyPanel() {
        final JPanel panel = new JPanel(new FormLayout("p,4dlu,f:p:g(1),10dlu,p,4dlu,f:p:g(1)", "p,2dlu,p,2dlu,p"));
        final CellConstraints cc = new CellConstraints();

        // left
        add(panel, getEnableProxy(), cc.xyw(1, 1, 3));
        JLabel lb = new JLabel(t.get(TConstants.VIEW_SETTINGS.PROXY_HOST));
        lb.setEnabled(false);
        proxyComponents.add(lb);
        add(panel, lb, cc.xy(1, 3));
        add(panel, getTxProxyHost(), cc.xy(3, 3));
        lb = new JLabel(t.get(TConstants.VIEW_SETTINGS.PROXY_PORT));
        lb.setEnabled(false);
        proxyComponents.add(lb);
        add(panel, lb, cc.xy(1, 5));
        add(panel, getTxProxyPort(), cc.xy(3, 5));

        // right
        add(panel, getEnableProxyAuth(), cc.xyw(5, 1, 3));
        lb = new JLabel(t.get(TConstants.VIEW_SETTINGS.PROXY_USER));
        lb.setEnabled(false);
        proxyAuthComponents.add(lb);
        add(panel, lb, cc.xy(5, 3));
        add(panel, getTxProxyUser(), cc.xy(7, 3));
        lb = new JLabel(t.get(TConstants.VIEW_SETTINGS.PROXY_PASS));
        lb.setEnabled(false);
        proxyAuthComponents.add(lb);
        add(panel, lb, cc.xy(5, 5));
        add(panel, getTxProxyPass(), cc.xy(7, 5));

        return panel;
    }

    @Override
    protected void createView() {
        setTitle(t.get(TConstants.VIEW_SETTINGS.CAPTION));

        final JPanel panel = new JPanel(new FormLayout("f:p:g", "p,4dlu,p,4dlu,p"));
        setMainPanel(panel);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        final CellConstraints cc = new CellConstraints();
        panel.add(createModDirPanel(), cc.xy(1, 1));
        panel.add(createMultiMcDirPanel(), cc.xy(1, 3));
        panel.add(createProxyPanel(), cc.xy(1, 5));

        // TODO add other settings properties

        // install tab order for settings panel
        getViewPanel().setFocusTraversalPolicy(tabOrder);
        getViewPanel().setFocusTraversalPolicyProvider(true);
    }

    protected JButton getButBrowseModDir() {
        if (butBrowseModDir == null) {
            butBrowseModDir = Fa.FOLDER_OPEN.makeButton();
            butBrowseModDir.setToolTipText(t.get(TConstants.VIEW_SETTINGS.MOD_DIR.BUTTON));
            butBrowseModDir.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    final JFileChooser fileSelectBox = new JFileChooser(getTxModDir().getText());
                    fileSelectBox.setDialogTitle(t.get(TConstants.VIEW_SETTINGS.MOD_DIR.BROWSE));
                    fileSelectBox.setMultiSelectionEnabled(false);
                    fileSelectBox.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    fileSelectBox.setCurrentDirectory(new File(getTxModDir().getText()));
                    final int result = fileSelectBox.showOpenDialog(getViewPanel());

                    if (result == JFileChooser.APPROVE_OPTION) {
                        // System.out.println(fileSelectBox.getSelectedFile());
                        getTxModDir().setText(fileSelectBox.getSelectedFile().getAbsolutePath());
                    }
                }
            });
        }
        return butBrowseModDir;
    }

    protected JButton getButBrowseMultiMcDir() {
        if (butBrowseMultiMcDir == null) {
            butBrowseMultiMcDir = Fa.FOLDER_OPEN.makeButton();
            butBrowseMultiMcDir.setToolTipText(t.get(TConstants.VIEW_SETTINGS.MULTIMC_DIR.BUTTON));
            butBrowseMultiMcDir.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    final JFileChooser fileSelectBox = new JFileChooser(getTxModDir().getText());
                    fileSelectBox.setDialogTitle(t.get(TConstants.VIEW_SETTINGS.MULTIMC_DIR.BROWSE));
                    fileSelectBox.setMultiSelectionEnabled(false);
                    fileSelectBox.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    fileSelectBox.setCurrentDirectory(new File(getTxMultiMcDir().getText()));
                    final int result = fileSelectBox.showOpenDialog(getViewPanel());

                    if (result == JFileChooser.APPROVE_OPTION) {
                        // System.out.println(fileSelectBox.getSelectedFile());
                        getTxMultiMcDir().setText(fileSelectBox.getSelectedFile().getAbsolutePath());
                        getTxMultiMcDir().validation();
                    }
                }
            });

            // TODO

        }
        return butBrowseMultiMcDir;
    }

    protected JCheckBox getEnableProxy() {
        if (enableProxy == null) {
            enableProxy = new JCheckBox(t.get(TConstants.VIEW_SETTINGS.PROXY_ENABLE));
            enableProxy.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(final ItemEvent evt) {
                    boolean enable = evt.getStateChange() == ItemEvent.SELECTED;
                    for (JComponent c : proxyComponents) {
                        c.setEnabled(enable);
                    }
                    enable = enable && getEnableProxyAuth().isSelected();
                    for (JComponent c : proxyAuthComponents) {
                        c.setEnabled(enable);
                    }
                }
            });
        }
        return enableProxy;
    }

    protected JCheckBox getEnableProxyAuth() {
        if (enableProxyAuth == null) {
            enableProxyAuth = new JCheckBox(t.get(TConstants.VIEW_SETTINGS.PROXY_AUTH_ENABLE));
            enableProxyAuth.setEnabled(false);
            enableProxyAuth.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(final ItemEvent evt) {
                    boolean enable = evt.getStateChange() == ItemEvent.SELECTED;
                    for (JComponent c : proxyAuthComponents) {
                        c.setEnabled(enable);
                    }
                }
            });
            proxyComponents.add(enableProxyAuth);
        }
        return enableProxyAuth;
    }

    @Override
    public List<JButton> getToolbarButtons() {
        return null;
    }

    protected JTextField getTxModDir() {
        if (txModDir == null) {
            txModDir = new JTextField();
            txModDir.setEnabled(true);
            txModDir.setEditable(false);
        }
        return txModDir;
    }

    protected ValidationTextField getTxMultiMcDir() {
        if (txMultiMcDir == null) {
            txMultiMcDir = new ValidationTextField() {
                private static final long serialVersionUID = 4157732676891184974L;

                @Override
                public ValidationInfo validateComponent() {
                    final ValidationInfo vi = new ValidationInfo();

                    final File mcmConc = new File(getTxMultiMcDir().getText(), "multimc.cfg");
                    if (!mcmConc.isFile()) {
                        vi.addError(TConstants.VIEW_SETTINGS.MULTIMC_DIR.VALIDATION_ERROR);
                    }

                    return vi;
                }
            };
            txMultiMcDir.setEnabled(true);
            txMultiMcDir.setEditable(false);
        }
        return txMultiMcDir;
    }

    protected JTextField getTxProxyHost() {
        if (txProxyHost == null) {
            txProxyHost = new JTextField();
            txProxyHost.setEnabled(false);

            proxyComponents.add(txProxyHost);
        }
        return txProxyHost;
    }

    protected JPasswordField getTxProxyPass() {
        if (txProxyPass == null) {
            txProxyPass = new JPasswordField();
            txProxyPass.setEnabled(false);
            proxyAuthComponents.add(txProxyPass);
        }
        return txProxyPass;
    }

    protected JFormattedTextField getTxProxyPort() {
        if (txProxyPort == null) {
            // restrict to TCP/IP ports
            final NumberFormatter nf = new NumberFormatter(NumberFormat.getIntegerInstance());
            nf.setMinimum(1);
            nf.setMaximum(65535);
            txProxyPort = new JFormattedTextField(nf);
            txProxyPort.setEnabled(false);

            proxyComponents.add(txProxyPort);
        }
        return txProxyPort;
    }

    protected JTextField getTxProxyUser() {
        if (txProxyUser == null) {
            txProxyUser = new JTextField();
            txProxyUser.setEnabled(false);
            proxyAuthComponents.add(txProxyUser);
        }
        return txProxyUser;
    }

}
