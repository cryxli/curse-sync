package li.cryx.curse.rc.component.dialog;

import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;

public class YesNoConfirmDialog extends MessageDialog {

    public YesNoConfirmDialog(final String titleKey, final String msgKey) {
        super(DIALOG_TYPE_QUESTION, titleKey, msgKey);
        init();
    }

    public YesNoConfirmDialog(final String titleKey, final String msgKey, final Object... arguments) {
        super(DIALOG_TYPE_QUESTION, titleKey, msgKey, arguments);
        init();
    }

    private void init() {
        final Translation t = Translation.getInstance();
        removeAllButtons();
        setButton(ACTION_CODE_YES, Fa.CHECK.decorateButton(createButton(ACTION_CODE_YES), t.get("Yes")));
        setButton(ACTION_CODE_NO, Fa.TIMES.decorateButton(createButton(ACTION_CODE_NO), t.get("No")));

    }

}
