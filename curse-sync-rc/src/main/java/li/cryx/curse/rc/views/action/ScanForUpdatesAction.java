package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import li.cryx.curse.db.ProjectDao;
import li.cryx.curse.mng.ScanJsonMod;
import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.ViewComponent;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.views.dialog.ScanForUpdateProjectDialog;
import li.cryx.curse.rc.worker.ScanForUpdateProjectsWorker;

/**
 * Action that allows to scan for updates for a single mod at a time.
 *
 * @author cryxli
 */
public class ScanForUpdatesAction implements ActionListener {

    private final ViewComponent parent;

    private final AppConfig conf;

    public ScanForUpdatesAction(final ViewComponent parent, final AppConfig conf) {
        this.parent = parent;
        this.conf = conf;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        if (evt.getSource() instanceof MenuItem) {
            @SuppressWarnings("unchecked")
            final MenuItem<Project> src = (MenuItem<Project>) evt.getSource();
            scanForUpdates(src.getPayload());
        }
    }

    private void scanForUpdates(final Project project) {
        final ScanForUpdateProjectDialog dialog = new ScanForUpdateProjectDialog();

        final ScanForUpdateProjectsWorker worker = new ScanForUpdateProjectsWorker(dialog);
        worker.addProject(project);
        worker.setProjectDao(conf.getBean(ProjectDao.class));
        worker.setScanJsonMod(conf.getBean(ScanJsonMod.class));
        worker.execute();

        final int result = dialog.showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isCancelCode(result)) {
            worker.cancel();
        }
    }

}
