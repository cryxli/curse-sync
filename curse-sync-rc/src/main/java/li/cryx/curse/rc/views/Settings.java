package li.cryx.curse.rc.views;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.AppConfig;
import li.cryx.curse.rc.views.ide.SettingsIde;

public class Settings extends SettingsIde {

    private static final Logger LOG = LoggerFactory.getLogger(Settings.class);

    private final AppConfig config;

    public Settings(final AppConfig config) {
        this.config = config;
    }

    @Override
    public void preHide() {
        // TODO add other settings properties
        config.setModDir(new File(getTxModDir().getText()));

        boolean enabled = getEnableProxy().isSelected();
        config.setProxyHost(enabled ? getTxProxyHost().getText() : null);
        config.setProxyPort(enabled ? String.valueOf(getTxProxyPort().getValue()) : null);
        enabled = enabled && getEnableProxyAuth().isSelected();
        config.setProxyUser(enabled ? getTxProxyUser().getText() : null);
        config.setProxyPass(enabled ? String.valueOf(getTxProxyPass().getPassword()) : null);
        config.applyProxySettings();

        if (getTxMultiMcDir().isValid()) {
            config.setMultimcDir(getTxMultiMcDir().getText());
        }

        try {
            config.save();
        } catch (IOException e) {
            LOG.error("Cannot save config file.", e);
        }
    }

    @Override
    public void preShow() {
        // TODO add other settings properties
        getTxModDir().setText(config.getModDir().getAbsolutePath());
        setProxy(config.getProxyHost(), config.getProxyPort(), config.getProxyUser(), config.getProxyPass());
        getTxMultiMcDir().setText(config.getMultimcDir());
    }

    public void setProxy(final String host, final String port, final String user, final String password) {
        if (StringUtils.stripToNull(host) == null) {
            getEnableProxy().setSelected(false);
            getTxProxyHost().setText("");
            getTxProxyPort().setText("");
            getEnableProxyAuth().setSelected(false);
            getTxProxyUser().setText("");
            getTxProxyPass().setText("");
        } else {
            getEnableProxy().setSelected(true);
            getTxProxyHost().setText(host);
            getTxProxyPort().setText(port);
            if (StringUtils.stripToNull(user) == null) {
                getEnableProxyAuth().setSelected(false);
                getTxProxyUser().setText("");
                getTxProxyPass().setText("");
            } else {
                getEnableProxyAuth().setSelected(true);
                getTxProxyUser().setText(user);
                getTxProxyPass().setText(password);
            }
        }
    }

}
