package li.cryx.curse.rc.views.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class ObjectRowTableModel<E> extends AbstractTableModel {

    private static final long serialVersionUID = 8485575359921214033L;

    private final List<E> data = new LinkedList<>();

    public void addRow(final E data) {
        this.data.add(data);
        fireTableRowsInserted(this.data.size() - 1, this.data.size() - 1);
    }

    public void clear() {
        this.data.clear();
        fireTableDataChanged();
    }

    public List<E> getAllRows() {
        return Collections.unmodifiableList(data);
    }

    public E getRow(final int rowIndex) {
        return data.get(rowIndex);
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    protected abstract Object getValueAt(E row, int columnIndex);

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        return getValueAt(getRow(rowIndex), columnIndex);
    }

    public void setData(final Collection<E> data) {
        this.data.clear();
        this.data.addAll(data);
        fireTableDataChanged();
    }

    public abstract void setValueAt(Object aValue, E row, int columnIndex);

    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
        setValueAt(aValue, getRow(rowIndex), columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

}
