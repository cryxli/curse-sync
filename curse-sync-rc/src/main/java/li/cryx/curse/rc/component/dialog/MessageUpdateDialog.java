package li.cryx.curse.rc.component.dialog;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;

/**
 * Generic progress bar dialog with message.
 *
 * @author cryxli
 */
public class MessageUpdateDialog extends ButtonBasedDialog {

    private final Translation t = Translation.getInstance();

    private String msg;

    public MessageUpdateDialog(final String titleKey) {
        super(DIALOG_TYPE_INFO, titleKey);
        removeAllButtons();
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
    }

    public MessageUpdateDialog(final String titleKey, final String msgKey) {
        this(titleKey);
        setMsgKey(msgKey);
    }

    @Override
    protected JPanel getMainPanel() {
        final JPanel main = new JPanel(new BorderLayout());

        final JTextArea txt = new JTextArea(msg);
        txt.setEditable(false);
        txt.setLineWrap(true);
        txt.setBorder(BorderFactory.createEmptyBorder());
        txt.setOpaque(false);
        txt.setBackground(new Color(0, 0, 0, 0));
        txt.setWrapStyleWord(true);
        main.add(txt, BorderLayout.CENTER);

        final JProgressBar progress = new JProgressBar();
        progress.setIndeterminate(true);
        main.add(progress, BorderLayout.SOUTH);

        return main;
    }

    /** Have the dialog closed when worker is done. */
    public void hideDialog() {
        hideDialog(ACTION_CODE_OK);
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public void setMsgKey(final String msgKey) {
        setMsg(t.get(msgKey));
    }

}
