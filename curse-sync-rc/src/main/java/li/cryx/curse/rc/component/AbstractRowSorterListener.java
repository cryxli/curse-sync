package li.cryx.curse.rc.component;

import java.util.List;

import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterEvent.Type;
import javax.swing.event.RowSorterListener;

public abstract class AbstractRowSorterListener implements RowSorterListener {

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void sorterChanged(final RowSorterEvent e) {
        if (e.getType() == Type.SORT_ORDER_CHANGED) {
            final RowSorter rowSorter = e.getSource();
            final List<SortKey> sortKeys = rowSorter.getSortKeys();
            updatedSortKeys(sortKeys);
        }
    }

    protected abstract void updatedSortKeys(List<SortKey> sortKeys);

}
