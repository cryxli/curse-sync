package li.cryx.curse.rc.views.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.component.MenuItem;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.component.dialog.YesNoConfirmDialog;
import li.cryx.curse.rc.model.db.Project;
import li.cryx.curse.rc.views.ProjectList;

public class DeleteProjectAction implements ActionListener {

    private final ProjectList parent;

    public DeleteProjectAction(final ProjectList parent) {
        this.parent = parent;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        if (evt.getSource() instanceof MenuItem) {
            @SuppressWarnings("unchecked")
            final MenuItem<Project> src = (MenuItem<Project>) evt.getSource();
            askForDelete(src.getPayload());
        }
    }

    private void askForDelete(final Project project) {
        final int result = new YesNoConfirmDialog( //
                TConstants.VIEW_PROJECT_LIST.DIALOG_DELETE.CAPTION, //
                TConstants.VIEW_PROJECT_LIST.DIALOG_DELETE.MESSAGE, //
                project.getTitle() //
        ).showDialog(parent.getViewPanel());
        if (ButtonBasedDialog.isYesCode(result)) {
            parent.onDeleteProject(project.getId());
        }
    }

}
