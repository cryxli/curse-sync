package li.cryx.curse.rc.views.dialog;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import li.cryx.curse.rc.TConstants;
import li.cryx.curse.rc.common.Translation;
import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.component.dialog.ButtonBasedDialog;
import li.cryx.curse.rc.worker.UpdateAllModsWorker;

/**
 * This dialog shows the progress while {@link UpdateAllModsWorker} scans Curse.com for mod updates.
 *
 * @author cryxli
 */
public class UpdateAllModsDialog extends ButtonBasedDialog {

    private final Translation t = Translation.getInstance();

    private JProgressBar progress;

    private final JLabel lbMsg = new JLabel(" ");

    private final JLabel lbStep = new JLabel();

    public UpdateAllModsDialog() {
        super(ButtonBasedDialog.DIALOG_TYPE_INFO, TConstants.VIEW_MULTIMC.UPDATE_ALL.CAPTION);
        removeAllButtons();
        setButton(ACTION_CODE_CANCEL, Fa.TIMES.decorateButton(createButton(ACTION_CODE_CANCEL), t.get("Cancel")));
        setSize(400, 180);
    }

    @Override
    protected JPanel getMainPanel() {
        // show steps above progress bar
        final JPanel stepPanel = new JPanel(new BorderLayout());
        stepPanel.add(lbMsg, BorderLayout.CENTER);
        stepPanel.add(lbStep, BorderLayout.EAST);

        // the progress bar
        progress = new JProgressBar();
        final JPanel progressPanel = new JPanel(new BorderLayout());
        progressPanel.add(stepPanel, BorderLayout.NORTH);
        progressPanel.add(progress, BorderLayout.SOUTH);

        // main panel
        final JPanel main = new JPanel(new BorderLayout());
        main.add(progressPanel, BorderLayout.SOUTH);

        // main dialog message
        final JTextArea txt = new JTextArea(t.get(TConstants.VIEW_MULTIMC.UPDATE_ALL.INFO));
        txt.setEditable(false);
        txt.setLineWrap(true);
        txt.setWrapStyleWord(true);
        txt.setBorder(BorderFactory.createEmptyBorder());
        txt.setOpaque(false);
        txt.setBackground(new Color(0, 0, 0, 0));
        main.add(txt, BorderLayout.CENTER);

        return main;
    }

    /** Hide the dialog terminating is as if the user hit the OK button. */
    public void hideDialog() {
        hideDialog(ACTION_CODE_OK);
    }

    public void updateProgressMsg(final String msg) {
        if (msg != null) {
            lbMsg.setText(msg);
        }
    }

    public void updateProgressSteps(final int current, final int total) {
        progress.setMaximum(total);
        progress.setValue(current);
        lbStep.setText(current + " / " + total);
    }

}
