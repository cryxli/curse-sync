package li.cryx.curse.rc.model.filter;

import li.cryx.curse.rc.jdbc.ProjectMapper;
import li.cryx.curse.rc.model.db.Project;

public class ProjectSearchFilter extends AbstractDatabaseFilter {

    /**
     * State of the update flag {@link Project#isUpdated()}:
     * <ul>
     * <li><code>true</code>: Only show project that are updated.</li>
     * <li><code>false</code>: Only show project that are not updated.</li>
     * <li><code>null</code>: Show all projects.</li>
     * </ul>
     */
    private Boolean update;

    public ProjectSearchFilter() {
        setOrderBy(ProjectMapper.ATTR_TITLE);
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(final Boolean update) {
        this.update = update;
    }

}
