package li.cryx.curse.rc.common;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MD5 {

    private static final Logger LOG = LoggerFactory.getLogger(MD5.class);

    public static String hash(final File file) {
        try {
            final MessageDigest md = MessageDigest.getInstance("md5");
            byte[] hash = md.digest(FileUtils.readFileToByteArray(file));
            return DatatypeConverter.printHexBinary(hash).toLowerCase();
        } catch (IOException e) {
            LOG.error("Error reading file: " + file, e);
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Java does not know MD5", e);
        }
        return null;
    }

}
