package li.cryx.curse.rc.model;

import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

/**
 * Model to show the state of a mod project.
 *
 * @author cryxli
 */
public class ModState {

    public static final String UNKNOWN = "?";

    protected Project project;

    protected File file;

    protected File latest;

    protected java.io.File jar;

    protected String hash;

    private ModStateEnum state = ModStateEnum.UNKNOWN;

    private void checkVersions() {
        if (file != null && latest != null) {
            if (file.getUploaded() == latest.getUploaded()) {
                setState(ModStateEnum.UP_TO_DATE);

            } else if (file.getUploaded() < latest.getUploaded()) {
                setState(ModStateEnum.OUTDATED);

            } else {
                setState(ModStateEnum.UNKNOWN);
            }
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ModState other = (ModState) obj;
        if (getHash() == null) {
            if (other.getHash() != null) {
                return false;
            }
        } else if (!getHash().equals(other.getHash())) {
            return false;
        }
        return true;
    }

    public String getDownloadPage() {
        if (state == ModStateEnum.OUTDATED) {
            return latest.getDetail();
        } else {
            return null;
        }
    }

    public String getFilename() {
        if (file != null) {
            return file.getName();
        } else if (jar != null) {
            return jar.getName();
        } else {
            return UNKNOWN;
        }
    }

    public String getFilePage() {
        if (file != null) {
            return file.getDetail();
        } else {
            return null;
        }
    }

    public String getHash() {
        if (file != null) {
            return file.getHash();
        } else {
            return hash;
        }
    }

    public String getLatestName() {
        if (latest != null) {
            return latest.getName();
        } else {
            return null;
        }
    }

    protected Project getProject() {
        return project;
    }

    public String getProjectUrl() {
        if (project != null) {
            return project.getUrl();
        } else {
            return null;
        }
    }

    public String getRelease() {
        if (file != null) {
            return file.getRelease();
        } else {
            return UNKNOWN;
        }
    }

    public ModStateEnum getState() {
        return state;
    }

    public String getTitle() {
        if (project != null) {
            return project.getTitle();
        } else {
            return UNKNOWN;
        }
    }

    @Override
    public int hashCode() {
        if (getHash() == null) {
            return 0;
        } else {
            return getHash().hashCode();
        }
    }

    public void setFile(final File file) {
        this.file = file;
        checkVersions();
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public void setJar(final java.io.File jar) {
        this.jar = jar;
    }

    public void setLatest(final File latest) {
        this.latest = latest;
        checkVersions();
    }

    public void setProject(final Project project) {
        this.project = project;
    }

    public void setState(final ModStateEnum state) {
        if (state != null) {
            this.state = state;
        }
    }

    @Override
    public String toString() {
        return new StringBuffer() //
                .append("projectId=").append(project == null ? null : project.getId()) //
                .append(',') //
                .append("fileId=").append(file == null ? null : file.getId()) //
                .toString();
    }

    // TODO

}
