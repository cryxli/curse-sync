package li.cryx.curse.rc.common;

import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

public class TableAspect {

    public static void setSingleSelection(final JList<?> list) {
        list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public static void setSingleSelection(final JTable table) {
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    /**
     * Sets min, max and preferred width of a table column and therefore sets it to unresizable.
     *
     * @param column
     *            A table column.
     * @param width
     *            Absolute size in pixel.
     */
    public static void setWidth(final TableColumn column, final int width) {
        column.setMinWidth(width);
        column.setPreferredWidth(width);
        column.setMaxWidth(width);
    }

    private TableAspect() {
    }

}
