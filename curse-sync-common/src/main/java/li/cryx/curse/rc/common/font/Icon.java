package li.cryx.curse.rc.common.font;

import java.awt.Font;

import javax.swing.JButton;

public interface Icon {

    public <B extends JButton> B decorateButton(final B but);

    public <B extends JButton> B decorateButton(final B but, final String text);

    public <B extends JButton> B decorateButton(final B but, final String text, String iconLocation);

    public Font getFont();

    /**
     * Create a button containing only an icon.
     *
     * @return Created button.
     */
    public JButton makeButton();

    /**
     * Create a button with text and an icon to the left of it.
     *
     * @param text
     *            Text to be displayed in the button.
     * @return Created button.
     */
    public JButton makeButton(final String text);

    /**
     * Create a button with text and an icon.
     *
     * @param text
     *            Text to be displayed in the button.
     * @param iconLocation
     *            One of the <code>BorderLayout</code> locations.
     * @return Created button.
     */
    public JButton makeButton(final String text, final String iconLocation);

    public JButton makeToolbarButton();

}