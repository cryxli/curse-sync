package li.cryx.curse.rc.model;

import java.io.IOException;
import java.io.InputStream;

public class NamedInputStream extends InputStream {

    private final String name;

    private final InputStream stream;

    public NamedInputStream(final String name, final InputStream stream) {
        this.name = name;
        this.stream = stream;
    }

    @Override
    public int available() throws IOException {
        return stream.available();
    }

    @Override
    public void close() throws IOException {
        stream.close();
    }

    public String getName() {
        return name;
    }

    @Override
    public synchronized void mark(final int readlimit) {
        stream.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
        return stream.markSupported();
    }

    @Override
    public int read() throws IOException {
        return stream.read();
    }

    @Override
    public int read(final byte[] b) throws IOException {
        return stream.read(b);
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        return stream.read(b, off, len);
    }

    @Override
    public synchronized void reset() throws IOException {
        stream.reset();
    }

    @Override
    public long skip(final long n) throws IOException {
        return stream.skip(n);
    }

}
