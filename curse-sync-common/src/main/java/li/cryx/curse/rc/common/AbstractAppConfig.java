package li.cryx.curse.rc.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public abstract class AbstractAppConfig {

    private final String userHome;

    private File configFile;

    /**
     * Create a new config POJO with all default values.
     */
    protected AbstractAppConfig(final String appDir) {
        userHome = System.getProperty("user.home");
        if (!appDir.startsWith(".")) {
            configFile = new File(userHome, "." + appDir + "/config.xml");
        } else {
            configFile = new File(userHome, appDir + "/config.xml");
        }
        configFile.getParentFile().mkdirs();
    }

    public File getConfigFile() {
        return configFile;
    }

    public String getUserHome() {
        return userHome;
    }

    public void load(final File file) throws InvalidPropertiesFormatException, IOException {
        if (file != null && file.isFile()) {
            try (final FileInputStream fis = new FileInputStream(file)) {
                load(fis);
            }
        } else {
            // use defaults
            loadProperties(new Properties());
        }
    }

    public void load(final InputStream stream) throws InvalidPropertiesFormatException, IOException {
        final Properties prop = new Properties();
        if (stream != null) {
            prop.loadFromXML(stream);
        }
        loadProperties(prop);
    }

    protected abstract void loadProperties(Properties prop);

    public void save() throws IOException {
        final Properties prop = new Properties();
        saveProperties(prop);

        try (FileOutputStream fos = new FileOutputStream(getConfigFile())) {
            prop.storeToXML(fos, "created at " + new Date(), StandardCharsets.UTF_8.displayName());
        }
    }

    protected abstract void saveProperties(Properties prop);

    public void setConfigFile(final File configFile) {
        this.configFile = configFile;
    }

}
