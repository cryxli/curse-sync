package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public abstract class AbstractJdbcSetMapper<T> implements ResultSetExtractor<T>, RowMapper<T> {

    public ResultSetExtractor<T> asExtractor() {
        return this;
    }

    public RowMapper<T> asMapper() {
        return this;
    }

    @Override
    public T extractData(final ResultSet rs) throws SQLException, DataAccessException {
        if (rs.next()) {
            return map(rs);
        } else {
            return null;
        }
    }

    @Override
    public T mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        return map(rs);
    }

    protected abstract T map(ResultSet rs) throws SQLException, DataAccessException;

    protected Integer toInt(final ResultSet rs, final String attr) throws SQLException {
        return toInt(rs, attr, null);
    }

    protected Integer toInt(final ResultSet rs, final String attr, final Integer defaultValue) throws SQLException {
        final Integer value = rs.getInt(attr);
        if (rs.wasNull()) {
            return defaultValue;
        } else {
            return value;
        }
    }

    protected Long toLong(final ResultSet rs, final String attr) throws SQLException {
        return toLong(rs, attr, null);
    }

    protected Long toLong(final ResultSet rs, final String attr, final Long defaultValue) throws SQLException {
        final Long value = rs.getLong(attr);
        if (rs.wasNull()) {
            return defaultValue;
        } else {
            return value;
        }
    }

    protected String toStr(final ResultSet rs, final String attr) throws SQLException {
        return toStr(rs, attr, null);
    }

    protected String toStr(final ResultSet rs, final String attr, final String defaultValue) throws SQLException {
        final String value = rs.getString(attr);
        if (rs.wasNull()) {
            return defaultValue;
        } else {
            return value;
        }
    }

}
