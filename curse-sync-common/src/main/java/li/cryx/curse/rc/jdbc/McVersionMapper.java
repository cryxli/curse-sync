package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;

import li.cryx.curse.rc.model.McVersion;

public class McVersionMapper extends AbstractJdbcSetMapper<McVersion> {

    public static final String TABLE = "PVD_DESCRIPTION";

    public static final String ATTR_ID = "ID";

    public static final String ATTR_KEY = "DESC_KEY";

    public static final String ATTR_VALUE = "DESC_VALUE";

    public static final String ATTR_TYPE = "DESC_TYPE";

    public static final int TYPE = 1001;

    @Override
    protected McVersion map(final ResultSet rs) throws SQLException, DataAccessException {
        return new McVersion( //
                toInt(rs, ATTR_ID), //
                toInt(rs, ATTR_KEY), //
                toStr(rs, ATTR_VALUE) //
        );
    }

}
