package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;

import li.cryx.curse.rc.model.db.File;

public class FileMapper extends AbstractJdbcSetMapper<File> {

    public static final String TABLE = "CUR_FILE";

    public static final String ATTR_ID = "FIL_ID";

    public static final String ATTR_PRJ_ID = "FIL_PRJ_ID";

    public static final String ATTR_HASH = "FIL_HASH";

    public static final String ATTR_NAME = "FIL_NAME";

    public static final String ATTR_DOWNLOAD = "FIL_DOWNLOAD";

    public static final String ATTR_RELEASE = "FIL_RELEASE";

    public static final String ATTR_UPLOADED = "FIL_UPLOADED";

    public static final String ATTR_DETAIL = "FIL_DETAIL";

    @Override
    protected File map(final ResultSet rs) throws SQLException, DataAccessException {
        final File f = new File();
        f.setId(toLong(rs, ATTR_ID));
        f.setProjectId(toLong(rs, ATTR_PRJ_ID));
        f.setDetail(toStr(rs, ATTR_DETAIL));
        f.setDownload(toStr(rs, ATTR_DOWNLOAD));
        f.setHash(toStr(rs, ATTR_HASH));
        f.setName(toStr(rs, ATTR_NAME));
        f.setRelease(toStr(rs, ATTR_RELEASE));
        f.setUploaded(toLong(rs, ATTR_UPLOADED));
        return f;
    }

}
