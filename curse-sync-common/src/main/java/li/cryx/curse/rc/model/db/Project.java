package li.cryx.curse.rc.model.db;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Project {

    /** Description */
    private String desc;

    /** URL to first files page. */
    private String files;

    /** Project ID as given by Curse */
    private long id;

    private String key;

    /** URL to logo/icon */
    private String logo;

    /** (Display) name */
    private String title;

    /** Indicator to look for updates for the project when scanning Curse.com */
    private boolean updated = true;

    /** URL to project page */
    private String url;

    public String getDesc() {
        return desc;
    }

    public String getFiles() {
        return files;
    }

    public long getId() {
        return id;
    }

    public String getLogo() {
        return logo;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }

    public void setFiles(final String files) {
        this.files = files;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setLogo(final String logo) {
        this.logo = logo;
    }

    public void setTitle(final String title) {
        this.title = StringUtils.stripToNull(StringUtils.stripToEmpty(title).trim());
    }

    public void setUpdated(final boolean updated) {
        this.updated = updated;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
