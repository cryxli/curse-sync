package li.cryx.curse.rc.model.db;

import java.util.List;

/**
 * This is a special data bean used to display {@link File}s and their associated {@link FileVersion}s in the GUI.
 *
 * @author cryxli
 */
public class FileAndVersion extends File {

    private String projectName;

    private List<FileVersion> versions;

    public FileAndVersion(final File file) {
        super(file);
    }

    public List<FileVersion> getVersions() {
        return versions;
    }

    public void setVersions(final List<FileVersion> versions) {
        this.versions = versions;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

}
