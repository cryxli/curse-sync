package li.cryx.curse.rc.model.db;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class File {

    /** File ID as given by Curse */
    private long id;

    /** Project ID, see {@link Project#id} */
    private long projectId;

    /** Release type, usually "Release", "Beta", "Alpha". */
    private String release;

    /** File name */
    private String name;

    /** MD5 hash */
    private String hash;

    /** Epoch timestamp when the file was uploaded to Curse */
    private long uploaded;

    /** URL to detail page */
    private String detail;

    /** Download URL */
    private String download;

    public File() {
    }

    /** Clone constructor */
    public File(final File file) {
        setDetail(file.getDetail());
        setDownload(file.getDownload());
        setHash(file.getHash());
        setId(file.getId());
        setName(file.getName());
        setProjectId(file.getProjectId());
        setRelease(file.getRelease());
        setUploaded(file.getUploaded());
    }

    public String getDetail() {
        return detail;
    }

    public String getDownload() {
        return download;
    }

    public String getHash() {
        return hash;
    }

    public long getId() {
        return id;
    }

    /** Get filename with extension */
    public String getName() {
        return name;
    }

    public long getProjectId() {
        return projectId;
    }

    public String getRelease() {
        return release;
    }

    public long getUploaded() {
        return uploaded;
    }

    public void setDetail(final String detail) {
        this.detail = detail;
    }

    public void setDownload(final String download) {
        this.download = download;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = StringEscapeUtils.unescapeHtml4(name);
    }

    public void setProjectId(final long projectId) {
        this.projectId = projectId;
    }

    public void setRelease(final String release) {
        this.release = release;
    }

    public void setUploaded(final long uploaded) {
        this.uploaded = uploaded;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
