package li.cryx.curse.rc.model.db;

import org.apache.commons.lang3.StringUtils;

public class Property {

    private final String key;

    private String value;

    public Property(final String key) {
        this.key = key;
    }

    public Property(final String key, final String value) {
        this(key);
        set(value);
    }

    public Boolean get(final Boolean defaultValue) {
        if (value != null) {
            return "1".equals(value) || "true".equalsIgnoreCase(value);
        } else {
            return defaultValue;
        }
    }

    public Byte get(final Byte defaultValue) {
        if (value != null) {
            try {
                return Byte.parseByte(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public <V> V get(final Class<V> clazz) {
        if (Integer.class == clazz) {
            return clazz.cast(get((Integer) null));
        } else if (Long.class == clazz) {
            return clazz.cast(get((Long) null));
        } else if (Short.class == clazz) {
            return clazz.cast(get((Short) null));
        } else if (Byte.class == clazz) {
            return clazz.cast(get((Byte) null));
        } else if (Float.class == clazz) {
            return clazz.cast(get((Float) null));
        } else if (Double.class == clazz) {
            return clazz.cast(get((Double) null));

        } else if (Boolean.class == clazz) {
            return clazz.cast(get((Boolean) null));
        } else {
            return clazz.cast(get((String) null));
        }
    }

    public Double get(final Double defaultValue) {
        if (value != null) {
            try {
                return Double.parseDouble(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public Float get(final Float defaultValue) {
        if (value != null) {
            try {
                return Float.parseFloat(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public Integer get(final Integer defaultValue) {
        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public Long get(final Long defaultValue) {
        if (value != null) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public Short get(final Short defaultValue) {
        if (value != null) {
            try {
                return Short.parseShort(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public String get(final String defaultValue) {
        if (value != null) {
            return value;
        } else {
            return defaultValue;
        }
    }

    public String getKey() {
        return key;
    }

    public void set(final Boolean value) {
        if (value == null) {
            set((String) null);
        } else if (value) {
            set("true");
        } else {
            set("false");
        }
    }

    public void set(final Number value) {
        if (value != null) {
            set(value.toString());
        } else {
            set((String) null);
        }
    }

    public void set(final String value) {
        this.value = value;
    }

    public boolean isNull() {
        return value == null;
    }

    public boolean isNullOrEmpty() {
        return StringUtils.trimToNull(value) == null;
    }

    @Override
    public String toString() {
        return value;
    }

}
