package li.cryx.curse.rc.common;

/**
 * A collection of Bootstrap text colors.
 *
 * @author cryxli
 */
public class Color {

    /** Black */
    public static final java.awt.Color BLACK = java.awt.Color.BLACK;

    /** Gray */
    public static final java.awt.Color MUTED = new java.awt.Color(119, 119, 119);

    /** Dark blue */
    public static final java.awt.Color PRIMARY = new java.awt.Color(51, 122, 183);

    /** Dark green */
    public static final java.awt.Color SUCCESS = new java.awt.Color(60, 118, 61);

    /** Blue */
    public static final java.awt.Color INFO = new java.awt.Color(49, 112, 143);

    /** Dark yellow */
    public static final java.awt.Color WARNING = new java.awt.Color(138, 109, 59);

    /** Dark red */
    public static final java.awt.Color ERROR = new java.awt.Color(169, 68, 66);

    private Color() {
	// static singleton
    }

}
