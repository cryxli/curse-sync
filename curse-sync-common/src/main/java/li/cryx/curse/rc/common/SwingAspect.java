package li.cryx.curse.rc.common;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import li.cryx.curse.rc.common.font.Fa;

public class SwingAspect {

    private static final Logger LOG = LoggerFactory.getLogger(SwingAspect.class);

    /**
     * Activate the AcrylLookAndFeel. For this to work the artifact <code>com.jtattoo:JTattoo:1.6.11</code> has to be
     * present.
     */
    public static void installAcrylStyle() {
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
        } catch (Exception e) {
            LOG.warn("Could not change to Acryl style.", e);
        }
    }

    public static void installFa() {
        try {
            final InputStream stream = SwingAspect.class.getResourceAsStream("/font/fontawesome-webfont.ttf");
            final Font font = Font.createFont(Font.TRUETYPE_FONT, stream);
            Fa.setFont(font);
        } catch (FontFormatException e) {
            LOG.error("Unsupported font format", e);
        } catch (IOException e) {
            LOG.error("Unable to load icon font", e);
        }
    }

    public static void installNimbusStyle() {
        try {
            for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(laf.getName())) {
                    UIManager.setLookAndFeel(laf.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            LOG.warn("Could not change to Nimbus style.", e);
        }
    }

    public static JButton removeAllActionListeners(final JButton but) {
        final ActionListener[] listeners = but.getActionListeners();
        for (ActionListener l : listeners) {
            but.removeActionListener(l);
        }
        return but;
    }

    private SwingAspect() {
        // static singleton
    }

}
