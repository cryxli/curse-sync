package li.cryx.curse.rc.model;

import li.cryx.curse.rc.model.db.File;
import li.cryx.curse.rc.model.db.Project;

public class ModStateDecorator extends ModState {

    public ModStateDecorator(final ModState state) {
        setProject(state.project);
        setFile(state.file);
        setLatest(state.latest);
        setJar(state.jar);
        setHash(state.hash);
    }

    public File getCurrentFile() {
        return file;
    }

    public java.io.File getJar() {
        return jar;
    }

    public File getLatestFile() {
        return latest;
    }

    public Project getProject() {
        return project;
    }

}
