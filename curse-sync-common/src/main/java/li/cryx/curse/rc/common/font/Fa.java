package li.cryx.curse.rc.common.font;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JButton;

public enum Fa implements Icon {
    PX500("\uf26e"),

    ACCESSIBLE_ICON("\uf368"),

    ACCUSOFT("\uf369"),

    ADDRESS_BOOK("\uf2b9"),

    ADDRESS_CARD("\uf2bb"),

    ADJUST("\uf042"),

    ADN("\uf170"),

    ADVERSAL("\uf36a"),

    AFFILIATETHEME("\uf36b"),

    ALGOLIA("\uf36c"),

    ALIGN_CENTER("\uf037"),

    ALIGN_JUSTIFY("\uf039"),

    ALIGN_LEFT("\uf036"),

    ALIGN_RIGHT("\uf038"),

    AMAZON("\uf270"),

    AMAZON_PAY("\uf42c"),

    AMBULANCE("\uf0f9"),

    AMERICAN_SIGN_LANGUAGE_INTERPRETING("\uf2a3"),

    AMILIA("\uf36d"),

    ANCHOR("\uf13d"),

    ANDROID("\uf17b"),

    ANGELLIST("\uf209"),

    ANGLE_DOUBLE_DOWN("\uf103"),

    ANGLE_DOUBLE_LEFT("\uf100"),

    ANGLE_DOUBLE_RIGHT("\uf101"),

    ANGLE_DOUBLE_UP("\uf102"),

    ANGLE_DOWN("\uf107"),

    ANGLE_LEFT("\uf104"),

    ANGLE_RIGHT("\uf105"),

    ANGLE_UP("\uf106"),

    ANGRYCREATIVE("\uf36e"),

    ANGULAR("\uf420"),

    APP_STORE("\uf36f"),

    APP_STORE_IOS("\uf370"),

    APPER("\uf371"),

    APPLE("\uf179"),

    APPLE_PAY("\uf415"),

    ARCHIVE("\uf187"),

    ARROW_ALT_CIRCLE_DOWN("\uf358"),

    ARROW_ALT_CIRCLE_LEFT("\uf359"),

    ARROW_ALT_CIRCLE_RIGHT("\uf35a"),

    ARROW_ALT_CIRCLE_UP("\uf35b"),

    ARROW_CIRCLE_DOWN("\uf0ab"),

    ARROW_CIRCLE_LEFT("\uf0a8"),

    ARROW_CIRCLE_RIGHT("\uf0a9"),

    ARROW_CIRCLE_UP("\uf0aa"),

    ARROW_DOWN("\uf063"),

    ARROW_LEFT("\uf060"),

    ARROW_RIGHT("\uf061"),

    ARROW_UP("\uf062"),

    ARROWS_ALT("\uf0b2"),

    ARROWS_ALT_H("\uf337"),

    ARROWS_ALT_V("\uf338"),

    ASSISTIVE_LISTENING_SYSTEMS("\uf2a2"),

    ASTERISK("\uf069"),

    ASYMMETRIK("\uf372"),

    AT("\uf1fa"),

    AUDIBLE("\uf373"),

    AUDIO_DESCRIPTION("\uf29e"),

    AUTOPREFIXER("\uf41c"),

    AVIANEX("\uf374"),

    AVIATO("\uf421"),

    AWS("\uf375"),

    BACKWARD("\uf04a"),

    BALANCE_SCALE("\uf24e"),

    BAN("\uf05e"),

    BANDCAMP("\uf2d5"),

    BARCODE("\uf02a"),

    BARS("\uf0c9"),

    BASEBALL_BALL("\uf433"),

    BASKETBALL_BALL("\uf434"),

    BATH("\uf2cd"),

    BATTERY_EMPTY("\uf244"),

    BATTERY_FULL("\uf240"),

    BATTERY_HALF("\uf242"),

    BATTERY_QUARTER("\uf243"),

    BATTERY_THREE_QUARTERS("\uf241"),

    BED("\uf236"),

    BEER("\uf0fc"),

    BEHANCE("\uf1b4"),

    BEHANCE_SQUARE("\uf1b5"),

    BELL("\uf0f3"),

    BELL_SLASH("\uf1f6"),

    BICYCLE("\uf206"),

    BIMOBJECT("\uf378"),

    BINOCULARS("\uf1e5"),

    BIRTHDAY_CAKE("\uf1fd"),

    BITBUCKET("\uf171"),

    BITCOIN("\uf379"),

    BITY("\uf37a"),

    BLACK_TIE("\uf27e"),

    BLACKBERRY("\uf37b"),

    BLIND("\uf29d"),

    BLOGGER("\uf37c"),

    BLOGGER_B("\uf37d"),

    BLUETOOTH("\uf293"),

    BLUETOOTH_B("\uf294"),

    BOLD("\uf032"),

    BOLT("\uf0e7"),

    BOMB("\uf1e2"),

    BOOK("\uf02d"),

    BOOKMARK("\uf02e"),

    BOWLING_BALL("\uf436"),

    BRAILLE("\uf2a1"),

    BRIEFCASE("\uf0b1"),

    BTC("\uf15a"),

    BUG("\uf188"),

    BUILDING("\uf1ad"),

    BULLHORN("\uf0a1"),

    BULLSEYE("\uf140"),

    BUROMOBELEXPERTE("\uf37f"),

    BUS("\uf207"),

    BUYSELLADS("\uf20d"),

    CALCulator("\uf1ec"),

    CALENDAR("\uf133"),

    CALENDAR_ALT("\uf073"),

    CALENDAR_CHECK("\uf274"),

    CALENDAR_MINUS("\uf272"),

    CALENDAR_PLUS("\uf271"),

    CALENDAR_TIMES("\uf273"),

    CAMERA("\uf030"),

    CAMERA_RETRO("\uf083"),

    CAR("\uf1b9"),

    CARET_DOWN("\uf0d7"),

    CARET_LEFT("\uf0d9"),

    CARET_RIGHT("\uf0da"),

    CARET_SQUARE_DOWN("\uf150"),

    CARET_SQUARE_LEFT("\uf191"),

    CARET_SQUARE_RIGHT("\uf152"),

    CARET_SQUARE_UP("\uf151"),

    CARET_UP("\uf0d8"),

    CART_ARROW_DOWN("\uf218"),

    CART_PLUS("\uf217"),

    CC_AMAZON_PAY("\uf42d"),

    CC_AMEX("\uf1f3"),

    CC_APPLE_PAY("\uf416"),

    CC_DINERS_CLUB("\uf24c"),

    CC_DISCOVER("\uf1f2"),

    CC_JCB("\uf24b"),

    CC_MASTERCARD("\uf1f1"),

    CC_PAYPAL("\uf1f4"),

    CC_STRIPE("\uf1f5"),

    CC_VISA("\uf1f0"),

    CENTERCODE("\uf380"),

    CERTIFICATE("\uf0a3"),

    CHART_AREA("\uf1fe"),

    CHART_BAR("\uf080"),

    CHART_LINE("\uf201"),

    CHART_PIE("\uf200"),

    CHECK("\uf00c"),

    CHECK_CIRCLE("\uf058"),

    CHECK_SQUARE("\uf14a"),

    CHESS("\uf439"),

    CHESS_BISHOP("\uf43a"),

    CHESS_BOARD("\uf43c"),

    CHESS_KING("\uf43f"),

    CHESS_KNIGHT("\uf441"),

    CHESS_PAWN("\uf443"),

    CHESS_QUEEN("\uf445"),

    CHESS_ROOK("\uf447"),

    CHEVRON_CIRCLE_DOWN("\uf13a"),

    CHEVRON_CIRCLE_LEFT("\uf137"),

    CHEVRON_CIRCLE_RIGHT("\uf138"),

    CHEVRON_CIRCLE_UP("\uf139"),

    CHEVRON_DOWN("\uf078"),

    CHEVRON_LEFT("\uf053"),

    CHEVRON_RIGHT("\uf054"),

    CHEVRON_UP("\uf077"),

    CHILD("\uf1ae"),

    CHROME("\uf268"),

    CIRCLE("\uf111"),

    CIRCLE_NOTCH("\uf1ce"),

    CLIPBOARD("\uf328"),

    CLOCK("\uf017"),

    CLONE("\uf24d"),

    CLOSED_CAPTIONING("\uf20a"),

    CLOUD("\uf0c2"),

    CLOUD_DOWNLOAD_ALT("\uf381"),

    CLOUD_UPLOAD_ALT("\uf382"),

    CLOUDSCALE("\uf383"),

    CLOUDSMITH("\uf384"),

    CLOUDVERSIFY("\uf385"),

    CODE("\uf121"),

    CODE_BRANCH("\uf126"),

    CODEPEN("\uf1cb"),

    CODIEPIE("\uf284"),

    COFFEE("\uf0f4"),

    COG("\uf013"),

    COGS("\uf085"),

    COLUMNS("\uf0db"),

    COMMENT("\uf075"),

    COMMENT_ALT("\uf27a"),

    COMMENTS("\uf086"),

    COMPASS("\uf14e"),

    COMPRESS("\uf066"),

    CONNECTDEVELOP("\uf20e"),

    CONTAO("\uf26d"),

    COPY("\uf0c5"),

    COPYRIGHT("\uf1f9"),

    CPANEL("\uf388"),

    CREATIVE_COMMONS("\uf25e"),

    CREDIT_CARD("\uf09d"),

    CROP("\uf125"),

    CROSSHAIRS("\uf05b"),

    CSS3("\uf13c"),

    CSS3_ALT("\uf38b"),

    CUBE("\uf1b2"),

    CUBES("\uf1b3"),

    CUT("\uf0c4"),

    CUTTLEFISH("\uf38c"),

    D_AND_D("\uf38d"),

    DASHCUBE("\uf210"),

    DATABASE("\uf1c0"),

    DEAF("\uf2a4"),

    DELICIOUS("\uf1a5"),

    DEPLOYDOG("\uf38e"),

    DESKPRO("\uf38f"),

    DESKTOP("\uf108"),

    DEVIANTART("\uf1bd"),

    DIGG("\uf1a6"),

    DIGITAL_OCEAN("\uf391"),

    DISCORD("\uf392"),

    DISCOURSE("\uf393"),

    DOCHUB("\uf394"),

    DOCKER("\uf395"),

    DOLLAR_SIGN("\uf155"),

    DOT_CIRCLE("\uf192"),

    DOWNLOAD("\uf019"),

    DRAFT2DIGITAL("\uf396"),

    DRIBBBLE("\uf17d"),

    DRIBBBLE_SQUARE("\uf397"),

    DROPBOX("\uf16b"),

    DRUPAL("\uf1a9"),

    DYALOG("\uf399"),

    EARLYBIRDS("\uf39a"),

    EDGE("\uf282"),

    EDIT("\uf044"),

    EJECT("\uf052"),

    ELEMENTOR("\uf430"),

    ELLIPSIS_H("\uf141"),

    ELLIPSIS_V("\uf142"),

    EMBER("\uf423"),

    EMPIRE("\uf1d1"),

    ENVELOPE("\uf0e0"),

    ENVELOPE_OPEN("\uf2b6"),

    ENVELOPE_SQUARE("\uf199"),

    ENVIRA("\uf299"),

    ERASER("\uf12d"),

    ERLANG("\uf39d"),

    ETHEREUM("\uf42e"),

    ETSY("\uf2d7"),

    EURO_SIGN("\uf153"),

    EXCHANGE_ALT("\uf362"),

    EXCLAMATION("\uf12a"),

    EXCLAMATION_CIRCLE("\uf06a"),

    EXCLAMATION_TRIANGLE("\uf071"),

    EXPAND("\uf065"),

    EXPAND_ARROWS_ALT("\uf31e"),

    EXPEDITEDSSL("\uf23e"),

    EXTERNAL_LINK_ALT("\uf35d"),

    EXTERNAL_LINK_SQUARE_ALT("\uf360"),

    EYE("\uf06e"),

    EYE_DROPPER("\uf1fb"),

    EYE_SLASH("\uf070"),

    FACEBOOK("\uf09a"),

    FACEBOOK_F("\uf39e"),

    FACEBOOK_MESSENGER("\uf39f"),

    FACEBOOK_SQUARE("\uf082"),

    FAST_BACKWARD("\uf049"),

    FAST_FORWARD("\uf050"),

    FAX("\uf1ac"),

    FEMALE("\uf182"),

    FIGHTER_JET("\uf0fb"),

    FILE("\uf15b"),

    FILE_ALT("\uf15c"),

    FILE_ARCHIVE("\uf1c6"),

    FILE_AUDIO("\uf1c7"),

    FILE_CODE("\uf1c9"),

    FILE_EXCEL("\uf1c3"),

    FILE_IMAGE("\uf1c5"),

    FILE_PDF("\uf1c1"),

    FILE_POWERPOINT("\uf1c4"),

    FILE_VIDEO("\uf1c8"),

    FILE_WORD("\uf1c2"),

    FILM("\uf008"),

    FILTER("\uf0b0"),

    FIRE("\uf06d"),

    FIRE_EXTINGUISHER("\uf134"),

    FIREFOX("\uf269"),

    FIRST_ORDER("\uf2b0"),

    FIRSTDRAFT("\uf3a1"),

    FLAG("\uf024"),

    FLAG_CHECKERED("\uf11e"),

    FLASK("\uf0c3"),

    FLICKR("\uf16e"),

    FLIPBOARD("\uf44d"),

    FLY("\uf417"),

    FOLDER("\uf07b"),

    FOLDER_OPEN("\uf07c"),

    FONTS("\uf031"),

    FONT_AWESOME("\uf2b4"),

    FONT_AWESOME_ALT("\uf35c"),

    FONT_AWESOME_FLAG("\uf425"),

    FONTICONS("\uf280"),

    FONTICONS_FI("\uf3a2"),

    FOOTBALL_BALL("\uf44e"),

    FORT_AWESOME("\uf286"),

    FORT_AWESOME_ALT("\uf3a3"),

    FORUMBEE("\uf211"),

    FORWARD("\uf04e"),

    FOURSQUARE("\uf180"),

    FREE_CODE_CAMP("\uf2c5"),

    FREEBSD("\uf3a4"),

    FROWN("\uf119"),

    FUTBOL("\uf1e3"),

    GAMEPAD("\uf11b"),

    GAVEL("\uf0e3"),

    GEM("\uf3a5"),

    GENDERLESS("\uf22d"),

    GET_POCKET("\uf265"),

    GG("\uf260"),

    GG_CIRCLE("\uf261"),

    GIFT("\uf06b"),

    GIT("\uf1d3"),

    GIT_SQUARE("\uf1d2"),

    GITHUB("\uf09b"),

    GITHUB_ALT("\uf113"),

    GITHUB_SQUARE("\uf092"),

    GITKRAKEN("\uf3a6"),

    GITLAB("\uf296"),

    GITTER("\uf426"),

    GLASS_MARTINI("\uf000"),

    GLIDE("\uf2a5"),

    GLIDE_G("\uf2a6"),

    GLOBE("\uf0ac"),

    GOFORE("\uf3a7"),

    GOLF_BALL("\uf450"),

    GOODREADS("\uf3a8"),

    GOODREADS_G("\uf3a9"),

    GOOGLE("\uf1a0"),

    GOOGLE_DRIVE("\uf3aa"),

    GOOGLE_PLAY("\uf3ab"),

    GOOGLE_PLUS("\uf2b3"),

    GOOGLE_PLUS_G("\uf0d5"),

    GOOGLE_PLUS_SQUARE("\uf0d4"),

    GOOGLE_WALLET("\uf1ee"),

    GRADUATION_CAP("\uf19d"),

    GRATIPAY("\uf184"),

    GRAV("\uf2d6"),

    GRIPFIRE("\uf3ac"),

    GRUNT("\uf3ad"),

    GULP("\uf3ae"),

    H_SQUARE("\uf0fd"),

    HACKER_NEWS("\uf1d4"),

    HACKER_NEWS_SQUARE("\uf3af"),

    HAND_LIZARD("\uf258"),

    HAND_PAPER("\uf256"),

    HAND_PEACE("\uf25b"),

    HAND_POINT_DOWN("\uf0a7"),

    HAND_POINT_LEFT("\uf0a5"),

    HAND_POINT_RIGHT("\uf0a4"),

    HAND_POINT_UP("\uf0a6"),

    HAND_POINTER("\uf25a"),

    HAND_ROCK("\uf255"),

    HAND_SCISSORS("\uf257"),

    HAND_SPOCK("\uf259"),

    HANDSHAKE("\uf2b5"),

    HASHTAG("\uf292"),

    HDD("\uf0a0"),

    HEADING("\uf1dc"),

    HEADPHONES("\uf025"),

    HEART("\uf004"),

    HEARTBEAT("\uf21e"),

    HIPS("\uf452"),

    HIRE_A_HELPER("\uf3b0"),

    HISTORY("\uf1da"),

    HOCKEY_PUCK("\uf453"),

    HOME("\uf015"),

    HOOLI("\uf427"),

    HOSPITAL("\uf0f8"),

    HOTJAR("\uf3b1"),

    HOURGLASS("\uf254"),

    HOURGLASS_END("\uf253"),

    HOURGLASS_HALF("\uf252"),

    HOURGLASS_START("\uf251"),

    HOUZZ("\uf27c"),

    HTML5("\uf13b"),

    HUBSPOT("\uf3b2"),

    I_CURSOR("\uf246"),

    ID_BADGE("\uf2c1"),

    ID_CARD("\uf2c2"),

    IMAGE("\uf03e"),

    IMAGES("\uf302"),

    IMDB("\uf2d8"),

    INBOX("\uf01c"),

    INDENT("\uf03c"),

    INDUSTRY("\uf275"),

    INFO("\uf129"),

    INFO_CIRCLE("\uf05a"),

    INSTAGRAM("\uf16d"),

    INTERNET_EXPLORER("\uf26b"),

    IOXHOST("\uf208"),

    ITALIC("\uf033"),

    ITUNES("\uf3b4"),

    ITUNES_NOTE("\uf3b5"),

    JENKINS("\uf3b6"),

    JOGET("\uf3b7"),

    JOOMLA("\uf1aa"),

    JS("\uf3b8"),

    JS_SQUARE("\uf3b9"),

    JSFIDDLE("\uf1cc"),

    KEYS("\uf084"),

    KEYBOARD("\uf11c"),

    KEYCDN("\uf3ba"),

    KICKSTARTER("\uf3bb"),

    KICKSTARTER_K("\uf3bc"),

    KORVUE("\uf42f"),

    LANGUAGE("\uf1ab"),

    LAPTOP("\uf109"),

    LARAVEL("\uf3bd"),

    LASTFM("\uf202"),

    LASTFM_SQUARE("\uf203"),

    LEAF("\uf06c"),

    LEANPUB("\uf212"),

    LEMON("\uf094"),

    LESS("\uf41d"),

    LEVEL_DOWN_ALT("\uf3be"),

    LEVEL_UP_ALT("\uf3bf"),

    LIFE_RING("\uf1cd"),

    LIGHTBULB("\uf0eb"),

    LINE("\uf3c0"),

    LINK("\uf0c1"),

    LINKEDIN("\uf08c"),

    LINKEDIN_IN("\uf0e1"),

    LINODE("\uf2b8"),

    LINUX("\uf17c"),

    LIRA_SIGN("\uf195"),

    LIST("\uf03a"),

    LIST_ALT("\uf022"),

    LIST_OL("\uf0cb"),

    LIST_UL("\uf0ca"),

    LOCATION_ARROW("\uf124"),

    LOCK("\uf023"),

    LOCK_OPEN("\uf3c1"),

    LONG_ARROW_ALT_DOWN("\uf309"),

    LONG_ARROW_ALT_LEFT("\uf30a"),

    LONG_ARROW_ALT_RIGHT("\uf30b"),

    LONG_ARROW_ALT_UP("\uf30c"),

    LOW_VISION("\uf2a8"),

    LYFT("\uf3c3"),

    MAGENTO("\uf3c4"),

    MAGIC("\uf0d0"),

    MAGNET("\uf076"),

    MALE("\uf183"),

    MAP("\uf279"),

    MAP_MARKER("\uf041"),

    MAP_MARKER_ALT("\uf3c5"),

    MAP_PIN("\uf276"),

    MAP_SIGNS("\uf277"),

    MARS("\uf222"),

    MARS_DOUBLE("\uf227"),

    MARS_STROKE("\uf229"),

    MARS_STROKE_H("\uf22b"),

    MARS_STROKE_V("\uf22a"),

    MAXCDN("\uf136"),

    MEDAPPS("\uf3c6"),

    MEDIUM("\uf23a"),

    MEDIUM_M("\uf3c7"),

    MEDKIT("\uf0fa"),

    MEDRT("\uf3c8"),

    MEETUP("\uf2e0"),

    MEH("\uf11a"),

    MERCURY("\uf223"),

    MICROCHIP("\uf2db"),

    MICROPHONE("\uf130"),

    MICROPHONE_SLASH("\uf131"),

    MICROSOFT("\uf3ca"),

    MINUS("\uf068"),

    MINUS_CIRCLE("\uf056"),

    MINUS_SQUARE("\uf146"),

    MIX("\uf3cb"),

    MIXCLOUD("\uf289"),

    MIZUNI("\uf3cc"),

    MOBILE("\uf10b"),

    MOBILE_ALT("\uf3cd"),

    MODX("\uf285"),

    MONERO("\uf3d0"),

    MONEY_BILL_ALT("\uf3d1"),

    MOON("\uf186"),

    MOTORCYCLE("\uf21c"),

    MOUSE_POINTER("\uf245"),

    MUSIC("\uf001"),

    NAPSTER("\uf3d2"),

    NEUTER("\uf22c"),

    NEWSPAPER("\uf1ea"),

    NINTENDO_SWITCH("\uf418"),

    NODE("\uf419"),

    NODE_JS("\uf3d3"),

    NPM("\uf3d4"),

    NS8("\uf3d5"),

    NUTRITIONIX("\uf3d6"),

    OBJECT_GROUP("\uf247"),

    OBJECT_UNGROUP("\uf248"),

    ODNOKLASSNIKI("\uf263"),

    ODNOKLASSNIKI_SQUARE("\uf264"),

    OPENCART("\uf23d"),

    OPENID("\uf19b"),

    OPERA("\uf26a"),

    OPTIN_MONSTER("\uf23c"),

    OSI("\uf41a"),

    OUTDENT("\uf03b"),

    PAGE4("\uf3d7"),

    PAGELINES("\uf18c"),

    PAINT_BRUSH("\uf1fc"),

    PALFED("\uf3d8"),

    PAPER_PLANE("\uf1d8"),

    PAPERCLIP("\uf0c6"),

    PARAGRAPH("\uf1dd"),

    PASTE("\uf0ea"),

    PATREON("\uf3d9"),

    PAUSE("\uf04c"),

    PAUSE_CIRCLE("\uf28b"),

    PAW("\uf1b0"),

    PAYPAL("\uf1ed"),

    PEN_SQUARE("\uf14b"),

    PENCIL_ALT("\uf303"),

    PERCENT("\uf295"),

    PERISCOPE("\uf3da"),

    PHABRICATOR("\uf3db"),

    PHOENIX_FRAMEWORK("\uf3dc"),

    PHONE("\uf095"),

    PHONE_SQUARE("\uf098"),

    PHONE_VOLUME("\uf2a0"),

    PHP("\uf457"),

    PIED_PIPER("\uf2ae"),

    PIED_PIPER_ALT("\uf1a8"),

    PIED_PIPER_PP("\uf1a7"),

    PINTEREST("\uf0d2"),

    PINTEREST_P("\uf231"),

    PINTEREST_SQUARE("\uf0d3"),

    PLANE("\uf072"),

    PLAY("\uf04b"),

    PLAY_CIRCLE("\uf144"),

    PLAYSTATION("\uf3df"),

    PLUG("\uf1e6"),

    PLUS("\uf067"),

    PLUS_CIRCLE("\uf055"),

    PLUS_SQUARE("\uf0fe"),

    PODCAST("\uf2ce"),

    POUND_SIGN("\uf154"),

    POWER_OFF("\uf011"),

    PRINT("\uf02f"),

    PRODUCT_HUNT("\uf288"),

    PUSHED("\uf3e1"),

    PUZZLE_PIECE("\uf12e"),

    PYTHON("\uf3e2"),

    QQ("\uf1d6"),

    QRCODE("\uf029"),

    QUESTION("\uf128"),

    QUESTION_CIRCLE("\uf059"),

    QUIDDITCH("\uf458"),

    QUINSCAPE("\uf459"),

    QUORA("\uf2c4"),

    QUOTE_LEFT("\uf10d"),

    QUOTE_RIGHT("\uf10e"),

    RANDOM("\uf074"),

    RAVELRY("\uf2d9"),

    REACT("\uf41b"),

    REBEL("\uf1d0"),

    RECYCLE("\uf1b8"),

    RED_RIVER("\uf3e3"),

    REDDIT("\uf1a1"),

    REDDIT_ALIEN("\uf281"),

    REDDIT_SQUARE("\uf1a2"),

    REDO("\uf01e"),

    REDO_ALT("\uf2f9"),

    REGISTERED("\uf25d"),

    RENDACT("\uf3e4"),

    RENREN("\uf18b"),

    REPLY("\uf3e5"),

    REPLY_ALL("\uf122"),

    REPLYD("\uf3e6"),

    RESOLVING("\uf3e7"),

    RETWEET("\uf079"),

    ROAD("\uf018"),

    ROCKET("\uf135"),

    ROCKETCHAT("\uf3e8"),

    ROCKRMS("\uf3e9"),

    RSS("\uf09e"),

    RSS_SQUARE("\uf143"),

    RUBLE_SIGN("\uf158"),

    RUPEE_SIGN("\uf156"),

    SAFARI("\uf267"),

    SASS("\uf41e"),

    SAVE("\uf0c7"),

    SCHLIX("\uf3ea"),

    SCRIBD("\uf28a"),

    SEARCH("\uf002"),

    SEARCH_MINUS("\uf010"),

    SEARCH_PLUS("\uf00e"),

    SEARCHENGIN("\uf3eb"),

    SELLCAST("\uf2da"),

    SELLSY("\uf213"),

    SERVER("\uf233"),

    SERVICESTACK("\uf3ec"),

    SHARE("\uf064"),

    SHARE_ALT("\uf1e0"),

    SHARE_ALT_SQUARE("\uf1e1"),

    SHARE_SQUARE("\uf14d"),

    SHEKEL_SIGN("\uf20b"),

    SHIELD_ALT("\uf3ed"),

    SHIP("\uf21a"),

    SHIRTSINBULK("\uf214"),

    SHOPPING_BAG("\uf290"),

    SHOPPING_BASKET("\uf291"),

    SHOPPING_CART("\uf07a"),

    SHOWER("\uf2cc"),

    SIGN_IN_ALT("\uf2f6"),

    SIGN_LANGUAGE("\uf2a7"),

    SIGN_OUT_ALT("\uf2f5"),

    SIGNAL("\uf012"),

    SIMPLYBUILT("\uf215"),

    SISTRIX("\uf3ee"),

    SITEMAP("\uf0e8"),

    SKYATLAS("\uf216"),

    SKYPE("\uf17e"),

    SLACK("\uf198"),

    SLACK_HASH("\uf3ef"),

    SLIDERS_H("\uf1de"),

    SLIDESHARE("\uf1e7"),

    SMILE("\uf118"),

    SNAPCHAT("\uf2ab"),

    SNAPCHAT_GHOST("\uf2ac"),

    SNAPCHAT_SQUARE("\uf2ad"),

    SNOWFLAKE("\uf2dc"),

    SORT("\uf0dc"),

    SORT_ALPHA_DOWN("\uf15d"),

    SORT_ALPHA_UP("\uf15e"),

    SORT_AMOUNT_DOWN("\uf160"),

    SORT_AMOUNT_UP("\uf161"),

    SORT_DOWN("\uf0dd"),

    SORT_NUMERIC_DOWN("\uf162"),

    SORT_NUMERIC_UP("\uf163"),

    SORT_UP("\uf0de"),

    SOUNDCLOUD("\uf1be"),

    SPACE_SHUTTLE("\uf197"),

    SPEAKAP("\uf3f3"),

    SPINNER("\uf110"),

    SPOTIFY("\uf1bc"),

    SQUARE("\uf0c8"),

    SQUARE_FULL("\uf45c"),

    STACK_EXCHANGE("\uf18d"),

    STACK_OVERFLOW("\uf16c"),

    STAR("\uf005"),

    STAR_HALF("\uf089"),

    STAYLINKED("\uf3f5"),

    STEAM("\uf1b6"),

    STEAM_SQUARE("\uf1b7"),

    STEAM_SYMBOL("\uf3f6"),

    STEP_BACKWARD("\uf048"),

    STEP_FORWARD("\uf051"),

    STETHOSCOPE("\uf0f1"),

    STICKER_MULE("\uf3f7"),

    STICKY_NOTE("\uf249"),

    STOP("\uf04d"),

    STOP_CIRCLE("\uf28d"),

    STOPWATCH("\uf2f2"),

    STRAVA("\uf428"),

    STREET_VIEW("\uf21d"),

    STRIKETHROUGH("\uf0cc"),

    STRIPE("\uf429"),

    STRIPE_S("\uf42a"),

    STUDIOVINARI("\uf3f8"),

    STUMBLEUPON("\uf1a4"),

    STUMBLEUPON_CIRCLE("\uf1a3"),

    SUBSCRIPT("\uf12c"),

    SUBWAY("\uf239"),

    SUITCASE("\uf0f2"),

    SUN("\uf185"),

    SUPERPOWERS("\uf2dd"),

    SUPERSCRIPT("\uf12b"),

    SUPPLE("\uf3f9"),

    SYNC("\uf021"),

    SYNC_ALT("\uf2f1"),

    TABLE("\uf0ce"),

    TABLE_TENNIS("\uf45d"),

    TABLET("\uf10a"),

    TABLET_ALT("\uf3fa"),

    TACHOMETER_ALT("\uf3fd"),

    TAG("\uf02b"),

    TAGS("\uf02c"),

    TASKS("\uf0ae"),

    TAXI("\uf1ba"),

    TELEGRAM("\uf2c6"),

    TELEGRAM_PLANE("\uf3fe"),

    TENCENT_WEIBO("\uf1d5"),

    TERMINAL("\uf120"),

    TEXT_HEIGHT("\uf034"),

    TEXT_WIDTH("\uf035"),

    TH("\uf00a"),

    TH_LARGE("\uf009"),

    TH_LIST("\uf00b"),

    THEMEISLE("\uf2b2"),

    THERMOMETER_EMPTY("\uf2cb"),

    THERMOMETER_FULL("\uf2c7"),

    THERMOMETER_HALF("\uf2c9"),

    THERMOMETER_QUARTER("\uf2ca"),

    THERMOMETER_THREE_QUARTERS("\uf2c8"),

    THUMBS_DOWN("\uf165"),

    THUMBS_UP("\uf164"),

    THUMBTACK("\uf08d"),

    TICKET_ALT("\uf3ff"),

    TIMES("\uf00d"),

    TIMES_CIRCLE("\uf057"),

    TINT("\uf043"),

    TOGGLE_OFF("\uf204"),

    TOGGLE_ON("\uf205"),

    TRADEMARK("\uf25c"),

    TRAIN("\uf238"),

    TRANSGENDER("\uf224"),

    TRANSGENDER_ALT("\uf225"),

    TRASH("\uf1f8"),

    TRASH_ALT("\uf2ed"),

    TREE("\uf1bb"),

    TRELLO("\uf181"),

    TRIPADVISOR("\uf262"),

    TROPHY("\uf091"),

    TRUCK("\uf0d1"),

    TTY("\uf1e4"),

    TUMBLR("\uf173"),

    TUMBLR_SQUARE("\uf174"),

    TV("\uf26c"),

    TWITCH("\uf1e8"),

    TWITTER("\uf099"),

    TWITTER_SQUARE("\uf081"),

    TYPO3("\uf42b"),

    UBER("\uf402"),

    UIKIT("\uf403"),

    UMBRELLA("\uf0e9"),

    UNDERLINE("\uf0cd"),

    UNDO("\uf0e2"),

    UNDO_ALT("\uf2ea"),

    UNIREGISTRY("\uf404"),

    UNIVERSAL_ACCESS("\uf29a"),

    UNIVERSITY("\uf19c"),

    UNLINK("\uf127"),

    UNLOCK("\uf09c"),

    UNLOCK_ALT("\uf13e"),

    UNTAPPD("\uf405"),

    UPLOAD("\uf093"),

    USB("\uf287"),

    USER("\uf007"),

    USER_CIRCLE("\uf2bd"),

    USER_MD("\uf0f0"),

    USER_PLUS("\uf234"),

    USER_SECRET("\uf21b"),

    USER_TIMES("\uf235"),

    USERS("\uf0c0"),

    USSUNNAH("\uf407"),

    UTENSIL_SPOON("\uf2e5"),

    UTENSILS("\uf2e7"),

    VAADIN("\uf408"),

    VENUS("\uf221"),

    VENUS_DOUBLE("\uf226"),

    VENUS_MARS("\uf228"),

    VIACOIN("\uf237"),

    VIADEO("\uf2a9"),

    VIADEO_SQUARE("\uf2aa"),

    VIBER("\uf409"),

    VIDEO("\uf03d"),

    VIMEO("\uf40a"),

    VIMEO_SQUARE("\uf194"),

    VIMEO_V("\uf27d"),

    VINE("\uf1ca"),

    VK("\uf189"),

    VNV("\uf40b"),

    VOLLEYBALL_BALL("\uf45f"),

    VOLUME_DOWN("\uf027"),

    VOLUME_OFF("\uf026"),

    VOLUME_UP("\uf028"),

    VUEJS("\uf41f"),

    WEIBO("\uf18a"),

    WEIXIN("\uf1d7"),

    WHATSAPP("\uf232"),

    WHATSAPP_SQUARE("\uf40c"),

    WHEELCHAIR("\uf193"),

    WHMCS("\uf40d"),

    WIFI("\uf1eb"),

    WIKIPEDIA_W("\uf266"),

    WINDOW_CLOSE("\uf410"),

    WINDOW_MAXIMIZE("\uf2d0"),

    WINDOW_MINIMIZE("\uf2d1"),

    WINDOW_RESTORE("\uf2d2"),

    WINDOWS("\uf17a"),

    WON_SIGN("\uf159"),

    WORDPRESS("\uf19a"),

    WORDPRESS_SIMPLE("\uf411"),

    WPBEGINNER("\uf297"),

    WPEXPLORER("\uf2de"),

    WPFORMS("\uf298"),

    WRENCH("\uf0ad"),

    XBOX("\uf412"),

    XING("\uf168"),

    XING_SQUARE("\uf169"),

    Y_COMBINATOR("\uf23b"),

    YAHOO("\uf19e"),

    YANDEX("\uf413"),

    YANDEX_INTERNATIONAL("\uf414"),

    YELP("\uf1e9"),

    YEN_SIGN("\uf157"),

    YOAST("\uf2b1"),

    YOUTUBE("\uf167"),

    YOUTUBE_SQUARE("\uf431");

    private static Font font;

    public static void setFont(final Font font) {
        Fa.font = font.deriveFont(14.0f);
    }

    private final String key;

    private Fa(final String key) {
        this.key = key;
    }

    @Override
    public <B extends JButton> B decorateButton(final B but) {
        return IconDelegate.decorateButton(but, null, BorderLayout.WEST, this);
    }

    @Override
    public <B extends JButton> B decorateButton(final B but, final String text) {
        return IconDelegate.decorateButton(but, text, BorderLayout.WEST, this);
    }

    @Override
    public <B extends JButton> B decorateButton(final B but, final String text, final String iconLocation) {
        return IconDelegate.decorateButton(but, text, iconLocation, this);
    }

    @Override
    public Font getFont() {
        return font;
    }

    @Override
    public JButton makeButton() {
        return IconDelegate.makeButton(this);
    }

    @Override
    public JButton makeButton(final String text) {
        return IconDelegate.decorateButton(new JButton(), text, BorderLayout.WEST, this);
    }

    @Override
    public JButton makeButton(final String text, final String iconLocation) {
        return IconDelegate.decorateButton(new JButton(), text, iconLocation, this);
    }

    @Override
    public JButton makeToolbarButton() {
        return IconDelegate.makeToolbarButton(this);
    }

    @Override
    public String toString() {
        return key;
    }

}
