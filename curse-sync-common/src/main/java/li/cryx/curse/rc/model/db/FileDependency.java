package li.cryx.curse.rc.model.db;

public class FileDependency {

    private long fileId;

    private long dependencyProjectId;

    public long getDependencyProjectId() {
        return dependencyProjectId;
    }

    public long getFileId() {
        return fileId;
    }

    public void setDependencyProjectId(final long dependencyProjectId) {
        this.dependencyProjectId = dependencyProjectId;
    }

    public void setFileId(final long fileId) {
        this.fileId = fileId;
    }

}
