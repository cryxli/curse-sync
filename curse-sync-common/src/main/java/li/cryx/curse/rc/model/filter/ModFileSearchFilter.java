package li.cryx.curse.rc.model.filter;

import li.cryx.curse.rc.jdbc.FileMapper;

public class ModFileSearchFilter extends AbstractDatabaseFilter {

    private Long projectId;

    private String version;

    public ModFileSearchFilter() {
        setOrderBy(FileMapper.ATTR_ID);
        setOrderAsc(false);
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getVersion() {
        return version;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

}
