package li.cryx.curse.rc.model;

public enum Release implements IDescription {

    ALPHA,

    BETA,

    RELEASE;

    public static int getType() {
        return 1002;
    }

    public int key() {
        return ordinal() + 1;
    }

    public String getValue() {
        return name();
    }

}
