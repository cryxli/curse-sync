package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;

import li.cryx.curse.rc.model.db.Project;

public class ProjectMapper extends AbstractJdbcSetMapper<Project> {

    public static final String TABLE = "CUR_PROJECT";

    public static final String ATTR_ID = "PRJ_ID";

    public static final String ATTR_TITLE = "PRJ_TITLE";

    public static final String ATTR_DESC = "PRJ_DESC";

    public static final String ATTR_LOGO = "PRJ_LOGO";

    public static final String ATTR_FILES = "PRJ_FILES";

    public static final String ATTR_URL = "PRJ_URL";

    public static final String ATTR_UPDATE = "PRJ_UPDATE";

    public static final String ATTR_KEY = "PRJ_KEY";

    @Override
    protected Project map(final ResultSet rs) throws SQLException, DataAccessException {
        final Project p = new Project();
        p.setId(toLong(rs, ATTR_ID));
        p.setKey(toStr(rs, ATTR_KEY));
        p.setTitle(toStr(rs, ATTR_TITLE));
        p.setDesc(toStr(rs, ATTR_DESC));
        p.setLogo(toStr(rs, ATTR_LOGO));
        p.setFiles(toStr(rs, ATTR_FILES));
        p.setUrl(toStr(rs, ATTR_URL));
        p.setUpdated(toInt(rs, ATTR_UPDATE) > 0);
        return p;
    }

}
