package li.cryx.curse.rc.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class MultiMcInstance {

    private String name;

    private String version;

    private File dir;

    private File minecraftDir;

    private final List<ModState> mods = new ArrayList<>();

    public MultiMcInstance() {
    }

    /**
     * Copy constructor.
     *
     * @param instance
     *            Create a new instance with exactly the same property as the given one.
     */
    protected MultiMcInstance(final MultiMcInstance instance) {
        setName(instance.getName());
        setVersion(instance.getVersion());
        setDir(instance.getDir());
        setMinecraftDir(instance.getMinecraftDir());
        setMods(instance.getMods());
    }

    public File getDir() {
        return dir;
    }

    public File getMinecraftDir() {
        return minecraftDir;
    }

    public List<ModState> getMods() {
        return mods;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public void setDir(final File dir) {
        this.dir = dir;
    }

    public void setMinecraftDir(final File minecraftDir) {
        this.minecraftDir = minecraftDir;
    }

    public void setMods(final Collection<ModState> mods) {
        this.mods.clear();
        this.mods.addAll(mods);
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
