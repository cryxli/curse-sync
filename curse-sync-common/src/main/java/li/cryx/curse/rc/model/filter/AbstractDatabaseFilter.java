package li.cryx.curse.rc.model.filter;

public abstract class AbstractDatabaseFilter {

    private String orderBy;

    private boolean orderAsc = true;

    private int offset = 0;

    private int pageSize = 100;

    private int rowCount;

    public int getOffset() {
        return offset;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getRowCount() {
        return rowCount;
    }

    public boolean isOrderAsc() {
        return orderAsc;
    }

    public void noPaging() {
        setPageSize(0);
    }

    public void setOffset(final int offset) {
        this.offset = offset;
    }

    public void setOrderAsc() {
        setOrderAsc(true);
    }

    public void setOrderAsc(final boolean orderAsc) {
        this.orderAsc = orderAsc;
    }

    public void setOrderBy(final String orderBy) {
        this.orderBy = orderBy;
    }

    public void setOrderDesc() {
        setOrderAsc(false);
    }

    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    public void setRowCount(final int rowCount) {
        this.rowCount = rowCount;
    }

}
