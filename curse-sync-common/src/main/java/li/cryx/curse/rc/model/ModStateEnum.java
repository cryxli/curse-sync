package li.cryx.curse.rc.model;

import java.awt.Color;

import li.cryx.curse.rc.common.font.Fa;
import li.cryx.curse.rc.common.font.Icon;

public enum ModStateEnum {

    /** Mod JAR not yet resolved to a project, or, unable to resolve to a project. (<code>-1</code>) */
    UNKNOWN(-1, Fa.QUESTION_CIRCLE, li.cryx.curse.rc.common.Color.MUTED),

    /** Mod JAR is up-to-date for the given version. (<code>1</code>) */
    UP_TO_DATE(1, Fa.CHECK_CIRCLE, li.cryx.curse.rc.common.Color.SUCCESS),

    /** There is a newer version of this mod for the given version. (<code>2</code>) */
    OUTDATED(2, Fa.TIMES_CIRCLE, li.cryx.curse.rc.common.Color.ERROR);

    private final int key;

    private final Icon icon;

    private final Color color;

    private ModStateEnum(final int key, final Icon icon, final Color color) {
        this.key = key;
        this.icon = icon;
        this.color = color;
    }

    public Color color() {
        return color;
    }

    public Icon icon() {
        return icon;
    }

    public int key() {
        return key;
    }

}
