package li.cryx.curse.rc.model.db;

public class FileVersion {

    private long fileId;

    private String version;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileVersion other = (FileVersion) obj;
        if (fileId != other.fileId) {
            return false;
        }
        if (version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!version.equals(other.version)) {
            return false;
        }
        return true;
    }

    public long getFileId() {
        return fileId;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (fileId ^ fileId >>> 32);
        result = prime * result + (version == null ? 0 : version.hashCode());
        return result;
    }

    public void setFileId(final long fileId) {
        this.fileId = fileId;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

}
