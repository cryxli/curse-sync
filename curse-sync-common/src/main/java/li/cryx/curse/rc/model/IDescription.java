package li.cryx.curse.rc.model;

public interface IDescription {

    int key();

    String getValue();

}
