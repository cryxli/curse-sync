package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;

import li.cryx.curse.rc.model.db.FileDependency;

public class DependencyMapper extends AbstractJdbcSetMapper<FileDependency> {

    public static final String TABLE = "CUR_DEPENDENCY";

    public static final String ATTR_FIL_ID = "DEP_FIL_ID";

    public static final String ATTR_PRJ_ID = "DEP_PRJ_ID";

    @Override
    protected FileDependency map(final ResultSet rs) throws SQLException, DataAccessException {
        final FileDependency ver = new FileDependency();
        ver.setFileId(toLong(rs, ATTR_FIL_ID));
        ver.setDependencyProjectId(toLong(rs, ATTR_PRJ_ID));
        return ver;
    }

}
