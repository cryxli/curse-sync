package li.cryx.curse.rc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;

import li.cryx.curse.rc.model.db.FileVersion;

public class VersionMapper extends AbstractJdbcSetMapper<FileVersion> {

    public static final String TABLE = "CUR_VERSION";

    public static final String ATTR_FILE_ID = "VER_FIL_ID";

    public static final String ATTR_VERSION = "VER_VERSION";

    @Override
    protected FileVersion map(final ResultSet rs) throws SQLException, DataAccessException {
        final FileVersion ver = new FileVersion();
        ver.setFileId(toLong(rs, ATTR_FILE_ID));
        ver.setVersion(toStr(rs, ATTR_VERSION));
        return ver;
    }

}
