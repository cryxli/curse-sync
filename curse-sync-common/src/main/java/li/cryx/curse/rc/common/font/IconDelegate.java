package li.cryx.curse.rc.common.font;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

class IconDelegate {

    private static final int TOOLBAR_BUTTON_SIZE = 35;

    public static <B extends JButton> B decorateButton(final B but, final String text, String iconLocation,
            final Icon icon) {
        if (!BorderLayout.WEST.equals(iconLocation) //
                && !BorderLayout.EAST.equals(iconLocation) //
                && !BorderLayout.SOUTH.equals(iconLocation) //
                && !BorderLayout.NORTH.equals(iconLocation)) {
            iconLocation = BorderLayout.WEST;
        }

        but.setLayout(new BorderLayout());
        final JLabel iconLabel = new JLabel(icon.toString());
        iconLabel.setFont(icon.getFont());
        iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
        but.add(iconLabel, iconLocation);
        if (text != null) {
            but.add(new JLabel(text), BorderLayout.CENTER);
        }
        return but;
    }

    public static JButton makeButton(final Icon icon) {
        return makeButton(icon.toString(), icon.getFont());
    }

    public static JButton makeButton(final String key, final Font font) {
        final JButton but = new JButton(key);
        but.setFont(font);
        but.setMargin(new Insets(3, 3, 3, 3));
        return but;
    }

    public static JButton makeToolbarButton(final Icon icon) {
        final JButton but = IconDelegate.makeButton(icon.toString(), icon.getFont().deriveFont(18.0f));
        // make button square
        but.setPreferredSize(new Dimension(TOOLBAR_BUTTON_SIZE, TOOLBAR_BUTTON_SIZE));
        but.setMinimumSize(new Dimension(TOOLBAR_BUTTON_SIZE, TOOLBAR_BUTTON_SIZE));
        but.setMaximumSize(new Dimension(TOOLBAR_BUTTON_SIZE, TOOLBAR_BUTTON_SIZE));
        return but;
    }

}
