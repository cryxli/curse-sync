package li.cryx.curse.rc;

import java.io.File;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import li.cryx.curse.rc.common.AbstractAppConfig;

public class AppConfig extends AbstractAppConfig implements ApplicationContextAware {

    private static final String MOD_DIR = "mod-dir";

    private static final String PROXY_HOST = "http.proxyHost";

    private static final String PROXY_PORT = "http.proxyPort";

    private static final String PROXY_USER = "http.proxyUser";

    private static final String PROXY_PASS = "http.proxyPassword";

    private static final String MULTIMC_DIR = "multimc-dir";

    private static final String LAST_MULTIMC_INSTANCE = "multimc-instance";

    static String clean(final File file) {
        if (file == null) {
            return null;
        }
        String f = file.getAbsolutePath();
        return f.replace("\\.\\", "\\").replace("/./", "/");
    }

    private ApplicationContext ctx;

    /** Download directory. */
    private File modDir;

    /** Proxy host. If <code>null</code> proxy is disabled. */
    private String proxyHost;

    /** Proxy port */
    private String proxyPort;

    /** Proxy user. If <code>null</code> proxy authentification is disabled. */
    private String proxyUser;

    /** Proxy password */
    private String proxyPass;

    /** Directory to MultiMC installation */
    private String multimcDir;

    private String multimcInstance;

    public AppConfig() {
        super(".curse-sync");
    }

    public void applyProxySettings() {
        if (proxyHost != null) {
            System.setProperty("http.proxyHost", proxyHost);
            System.setProperty("http.proxyPort", proxyPort);
            System.setProperty("https.proxyHost", proxyHost);
            System.setProperty("https.proxyPort", proxyPort);
            if (proxyUser != null) {
                Authenticator.setDefault(new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(proxyUser,
                                proxyPass == null ? new char[] {} : proxyPass.toCharArray());
                    }
                });
            }
        }
    }

    /**
     * Get a bean from the Spring context.
     *
     * @param beanClass
     *            Class of expected bean.
     * @return Instance of that bean.
     */
    public <B> B getBean(final Class<B> beanClass) {
        return ctx.getBean(beanClass);
    }

    public File getModDir() {
        return modDir;
    }

    public String getMultimcDir() {
        return multimcDir;
    }

    public String getMultimcInstance() {
        return multimcInstance;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public String getProxyPass() {
        return proxyPass;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    @Override
    protected void loadProperties(final Properties prop) {
        final File defaultModFolder = new File(getConfigFile().getParentFile(), "mods");
        modDir = new File(prop.getProperty(MOD_DIR, defaultModFolder.getAbsolutePath()));

        setProxyHost(prop.getProperty(PROXY_HOST));
        setProxyPort(prop.getProperty(PROXY_PORT));
        setProxyUser(prop.getProperty(PROXY_USER));
        setProxyPass(prop.getProperty(PROXY_PASS));
        applyProxySettings();

        setMultimcDir(prop.getProperty(MULTIMC_DIR));
        if (getMultimcDir() != null) {
            setMultimcInstance(prop.getProperty(LAST_MULTIMC_INSTANCE));
        }

        // TODO
    }

    @Override
    protected void saveProperties(final Properties prop) {
        prop.setProperty(MOD_DIR, clean(modDir));

        if (proxyHost != null) {
            prop.setProperty(PROXY_HOST, proxyHost);
            prop.setProperty(PROXY_PORT, proxyPort);
            if (proxyUser != null) {
                prop.setProperty(PROXY_USER, proxyUser);
                prop.setProperty(PROXY_PASS, proxyPass);
            }
        }

        if (multimcDir != null) {
            prop.setProperty(MULTIMC_DIR, multimcDir);
            if (multimcInstance != null) {
                prop.setProperty(LAST_MULTIMC_INSTANCE, multimcInstance);
            }
        }

        // TODO
    }

    @Override
    public void setApplicationContext(final ApplicationContext ctx) throws BeansException {
        this.ctx = ctx;
    }

    public void setModDir(final File modDir) {
        this.modDir = modDir;
    }

    public void setMultimcDir(final String dir) {
        if (dir == null) {
            multimcDir = null;
            return;
        }
        File d = new File(dir);
        while (d != null && !new File(d, "multimc.cfg").isFile()) {
            d = d.getParentFile();
        }
        if (d != null) {
            multimcDir = clean(d);
        }
    }

    public void setMultimcInstance(final File multimcInstance) {
        setMultimcInstance(clean(multimcInstance));
    }

    public void setMultimcInstance(final String multimcInstance) {
        this.multimcInstance = multimcInstance;
    }

    public void setProxyHost(final String proxyHost) {
        this.proxyHost = StringUtils.stripToNull(proxyHost);
    }

    public void setProxyPass(final String proxyPass) {
        this.proxyPass = proxyPass;
    }

    public void setProxyPort(final String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public void setProxyUser(final String proxyUser) {
        this.proxyUser = StringUtils.stripToNull(proxyUser);
    }

}
