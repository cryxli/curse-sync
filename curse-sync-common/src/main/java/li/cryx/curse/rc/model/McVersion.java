package li.cryx.curse.rc.model;

import li.cryx.curse.rc.jdbc.McVersionMapper;

public class McVersion implements IDescription, Comparable<McVersion> {

    public static int getType() {
        return McVersionMapper.TYPE;
    }

    private final int id;

    private final int key;

    private final String version;

    public McVersion(final int id, final int key, final String version) {
        this.id = id;
        this.key = key;
        this.version = version;
    }

    @Override
    public int compareTo(final McVersion v) {
        return version.compareTo(v.version);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof McVersion) {
            return ((McVersion) obj).key() == key;
        } else {
            return false;
        }
    }

    public int getId() {
        return id;
    }

    @Override
    public String getValue() {
        return version;
    }

    @Override
    public int hashCode() {
        return key;
    }

    @Override
    public int key() {
        return key;
    }

    @Override
    public String toString() {
        return key + "=" + version;
    }

}
