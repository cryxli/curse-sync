package li.cryx.curse.rc.common;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import li.cryx.curse.rc.lang.XMLResourceBundleControl;

public class Translation {

    private static Translation instance;

    private static final String RES_BUNDLE_BASE = "lang/lang";

    public static Translation getInstance() {
        if (instance == null) {
            instance = new Translation();
        }
        return instance;
    }

    private Translation() {
        // static singleton
    }

    public String get(final Locale locale, final String key) {
        try {
            return ResourceBundle
                    .getBundle(RES_BUNDLE_BASE, locale, getClass().getClassLoader(), new XMLResourceBundleControl())
                    .getString(key);
        } catch (MissingResourceException e) {
            return "XXX " + key + " XXX";
        }
    }

    public String get(final Locale locale, final String key, final Object... arguments) {
        final String pattern = get(locale, key);
        return MessageFormat.format(pattern, arguments);
    }

    public String get(final String key) {
        return get(Locale.getDefault(), key);
    }

    public String get(final String key, final Object... arguments) {
        return get(Locale.getDefault(), key, arguments);
    }

}
