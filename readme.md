# curse-sync

A tool to track and update your [MultiMC](https://multimc.org)  instances.

Latest build: [NoFile.io](https://nofile.io/f/yA0p2urWYEf/curse-sync-rc-1.1.0-S-201805271450.jar) [Google](https://drive.google.com/open?id=1iggpIW00GCqPS0Hs2__XzkWmCdHVTakt).
